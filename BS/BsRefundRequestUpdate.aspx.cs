﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
//using PG;

public partial class BS_BsRefundRequestUpdate : System.Web.UI.Page
{
    //Itz_Trans_Dal objItzT = new Itz_Trans_Dal();
    //ITZ_Trans objIzT = new ITZ_Trans();
   
    DataSet DSCancel = new DataSet();
    string UserId = "", UserType = "";
    
    SqlTransactionDom ObjST = new SqlTransactionDom();
    string lbl_Reqid = "";
    private SqlTransaction ST = new SqlTransaction();
    DataSet GridDS = new DataSet();
    private double Refundfare;
    private SqlTransactionDom STDom = new SqlTransactionDom();
   // private PaymentGateway objPG = new PaymentGateway();
    int counter;
    DataSet AgencyDS = new DataSet();
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myCon"].ConnectionString);
    protected void Page_Load(object sender, System.EventArgs e)
    {
        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        if (string.IsNullOrEmpty(Session["UID"].ToString()) || Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        if (!IsPostBack)
        {
            lbluserid.Text = Session["UID"].ToString();
            try
            {
                binddata();
            }
            catch (Exception ex)
            {
                clsErrorLog.LogInfo(ex);
            }
        }
    }
    public void binddata()
    {
        counter = Convert.ToInt32(Request.QueryString["counter"].ToString());      
        AgencyDS = BusInfo(counter);
        DataTable dt1 = new DataTable();
        DataTable dt2 = new DataTable();
        dt1 = AgencyDS.Tables[0];
        dt2 = AgencyDS.Tables[1];
        if (dt1.Rows.Count > 0)
        {
            string addr = dt1.Rows[0]["city"].ToString() + ", " + dt1.Rows[0]["State"].ToString() + ", " + dt1.Rows[0]["country"].ToString() + ", " + dt1.Rows[0]["zipcode"].ToString();
            td_AgentID.InnerText = dt1.Rows[0]["User_id"].ToString();
            td_AgentName.InnerText = dt1.Rows[0]["Agency_Name"].ToString();
            td_AgentAddress.InnerText = dt1.Rows[0]["Address"].ToString();          
            td_AgentMobNo.InnerText = dt1.Rows[0]["Mobile"].ToString();
            td_Email.InnerText = dt1.Rows[0]["Email"].ToString();           
            td_CardLimit.InnerText = dt1.Rows[0]["Crd_Limit"].ToString();

        }
        if(dt2.Rows.Count>0)
        {

            grd_Pax.DataSource = dt2;
            grd_Pax.DataBind();
            txt_charge.Text=dt2.Rows[0]["REFUND_SERVICECHRG"].ToString();
            txt_Service.Text=dt2.Rows[0]["cancelCharge"].ToString();
            td1_AgentID.InnerText = dt2.Rows[0]["AGENTID"].ToString();
            td_orderid.InnerText = dt2.Rows[0]["ORDERID"].ToString();
            td_seat.InnerText = dt2.Rows[0]["SEATNO"].ToString();
            td_CancellationId.InnerText = dt2.Rows[0]["CANCELLATIONID"].ToString();
            td_totalfare.InnerText = dt2.Rows[0]["TA_NET_FARE"].ToString();

        }
    }
   
    public DataSet BusInfo(int count)
    {
        DataSet DS = new DataSet();
        try
        {
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                sqlcmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                sqlcmd.CommandTimeout = 900;
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "USP_BusRefundUpdate";
                sqlcmd.Parameters.Add("@CounterVal", SqlDbType.Int).Value = count;
                sqlcmd.Parameters.Add("@CMD_TYPE", SqlDbType.VarChar).Value = "Select";
                SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
                da.Fill(DS);
                con.Close();
                DS.Dispose();
            }
        }
        catch (Exception ex)
        {
           
        }
        return DS;
    }
    
    public void RefundBusBookingAmount(BS_SHARED.SHARED shared, string remark, string cancellationID, decimal totservicechrg)
    {
        shared.addAmt = Convert.ToDecimal(shared.taNetFare - totservicechrg);
        if (shared.addAmt > 0)
        {           
            string refundOrderID = "RFNDBUS" + shared.orderID + "_" + DateTime.Now.ToString("HHmmss");
            string rfndStatus = "RefundRequested";
            //string easyRefundID = "";
            string easyRefundID = "ERID" + shared.orderID + "_" + DateTime.Now.ToString("HHmmss");        
            try
            {
                if (!string.IsNullOrEmpty(shared.agentID))
                {
                    #region Update Status and update Credit Limit
                    rfndStatus = "Refunded";                  
                    string apicancelstatus = "offlinecancelled";
                    BS_DAL.SharedDAL shareddal = new BS_DAL.SharedDAL();
                    UpdateBuscanceldetailinprocess(rfndStatus, remark, cancellationID, apicancelstatus, easyRefundID, Session["UID"].ToString(), shared.orderID, shared.seat); 
                   
                    //SqlTransaction SqlT = new SqlTransaction();
                    //double ablBalance = 0;
                    //ablBalance = SqlT.UpdateNew_RegsRefund(shared.agentID, Convert.ToDouble(shared.refundAmt));

                    shared.refundAmt = Convert.ToString(shared.addAmt);
                    if (!string.IsNullOrEmpty(shared.agentID) && !string.IsNullOrEmpty(shared.refundAmt))
                    {
                        shared.avalBal = shareddal.deductAndaddfareAmt(shared, "Add");                        
                        shareddal.insertLedgerDetails(shared, "Add");
                        ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Refunded Successfully.');javascript: window.close();window.opener.location=window.opener.location.href;", true);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Booking status updated but refund is not process.');javascript: window.close();window.opener.location=window.opener.location.href;", true);
                    }
                    #endregion
                }
                else
                {
                    ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('try again !');javascript: window.close();window.opener.location=window.opener.location.href;", true);
                }
            }
            catch (Exception ex1)
            {
                clsErrorLog.LogInfo(ex1);
            }
        }

        else
        {
            ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Refund Amount Not Valid');javascript: window.close();window.opener.location=window.opener.location.href;", true);
        }


        ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Please Try After some time.');javascript: window.close();window.opener.location=window.opener.location.href;", true);
    }
    protected void btn_result_Click(object sender, System.EventArgs e)
    {
        //string strresult = "";
        decimal servicecharge = Convert.ToDecimal( txt_charge.Text);
        decimal cancelcharge = Convert.ToDecimal(txt_Service.Text);
        decimal totalrefchrg = servicecharge + cancelcharge;
        BS_SHARED.SHARED shared = new BS_SHARED.SHARED();
        shared.taNetFare = Convert.ToDecimal(td_totalfare.InnerText);
        shared.agentID = td1_AgentID.InnerText;
        shared.orderID = td_orderid.InnerText;
        shared.seat = td_seat.InnerText;
        RefundBusBookingAmount(shared, txtRemark.Text, td_CancellationId.InnerText, totalrefchrg);
        //strresult = UpdateBuscanceldetailinprocess("Refunded", txtRemark.Text, td_CancellationId.InnerText, "", "", shared.agentID, shared.orderID);
        //if (strresult.ToString() == "Y")
        //{
        //    ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Refunded Successfully.');javascript: window.close();window.opener.location=window.opener.location.href;", true);          
        //}
    }
    public string UpdateBuscanceldetailinprocess(string refundstatus, string remark, string cancellationId, string apicancelstatuss, string easyrefundid, string userid, string orderid,string seatno)
    {
        string i = "";
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myCon"].ConnectionString);
        DataSet DS = new DataSet();
        try
        {
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                sqlcmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                sqlcmd.CommandTimeout = 900;
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "SP_BusRefundRequestInprocess";
                sqlcmd.Parameters.AddWithValue("@cmd", "Update");
                sqlcmd.Parameters.AddWithValue("@refundstatus", refundstatus);
                sqlcmd.Parameters.AddWithValue("@remark", remark);
                sqlcmd.Parameters.AddWithValue("@cancellationId", cancellationId);
                sqlcmd.Parameters.AddWithValue("@apicancelstatus", apicancelstatuss);
                sqlcmd.Parameters.AddWithValue("@easyrefundid", easyrefundid);
                sqlcmd.Parameters.AddWithValue("@uid", userid);
                sqlcmd.Parameters.AddWithValue("@orderid", orderid);
                sqlcmd.Parameters.AddWithValue("@seatno", seatno);
                i = sqlcmd.ExecuteScalar().ToString();
                con.Close();

            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        return i;

    }
    protected void btn_Close_Click(object sender, System.EventArgs e)
    {           
            ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "javascript: window.close();", true);       
    }
}
