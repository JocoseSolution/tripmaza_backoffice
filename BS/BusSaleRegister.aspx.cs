﻿
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

public partial class BS_BusSaleRegister : System.Web.UI.Page
{
    private SqlTransaction ST = new SqlTransaction();
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private clsInsertSelectedFlight CllInsSelectFlt = new clsInsertSelectedFlight();
    DataSet AgencyDDLDS = new DataSet();
    DataSet grdds = new DataSet();
    DataSet fltds = new DataSet();
    private Status sttusobj = new Status();
    SqlConnection con = new SqlConnection();
    ClsCorporate clsCorp = new ClsCorporate();
    public void CheckEmptyValue()
    {
        try
        {
            string FromDate = null;
            string ToDate = null;
            int lenth = 0;
           // string PgStatus = drpPaymentStatus.Visible == true ? drpPaymentStatus.SelectedValue.ToLower() != "select" ? drpPaymentStatus.SelectedValue : null : null;
            if (String.IsNullOrEmpty(Request["From"]))
            {
                FromDate = "";
            }
            else
            {
                FromDate = Request["From"].Substring(3, 2) + "/" + Request["From"].Substring(0, 2) + "/" + Request["From"].Substring(6, 4);
                FromDate = FromDate + " " + "12:00:00 AM";
            }
            if (String.IsNullOrEmpty(Request["To"]))
            {
                ToDate = "";
            }
            else
            {
                ToDate = Request["To"].Substring(3, 2) + "/" + Request["To"].Substring(0, 2) + "/" + Request["To"].Substring(6, 4);
                ToDate = ToDate + " " + "11:59:59 PM";
            }
            string AgentID = String.IsNullOrEmpty(Request["txtAgencyName"]) ? "" : Request["txtAgencyName"];
            string OrderID = String.IsNullOrEmpty(txtOrderID.Text) ? "" : txtOrderID.Text.Trim();
            string busopretor = String.IsNullOrEmpty(txtBusOperator.Text) ? "" : txtBusOperator.Text.Trim();
            string source = String.IsNullOrEmpty(TxtSource.Text) ? "" : TxtSource.Text.Trim();
            string destination = String.IsNullOrEmpty(TxtDestination.Text) ? "" : TxtDestination.Text.Trim();
            if (AgentID != "")
            {
                string str = AgentID;
                int pos1 = str.IndexOf("(");
                int pos2 = str.IndexOf(")");
                lenth = pos2 - pos1;
                string AgentID1 = str.Substring(pos1 + 1, lenth - 1);
                grdds.Clear();
                grdds = BUSDetails(Session["UID"].ToString(), Session["User_Type"].ToString(), FromDate, ToDate, OrderID, AgentID1, "", busopretor, source, destination);
                ViewState["grdds"] = grdds;
                GrdBusReport.DataSource = grdds;
                GrdBusReport.DataBind();
            }
            else
            {
                grdds.Clear();
                grdds = BUSDetails(Session["UID"].ToString(), Session["User_Type"].ToString(), FromDate, ToDate, OrderID, AgentID, "", busopretor,source, destination);
                if (grdds.Tables[0].Rows.Count > 0)
                {
                    ViewState["grdds"] = grdds;
                    GrdBusReport.DataSource = grdds;
                    GrdBusReport.DataBind();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "err_msg", "alert('No Record found');", true);                  
                }
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    protected void Page_Load(object sender, System.EventArgs e)
    {

        try
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            if (string.IsNullOrEmpty(Session["UID"].ToString()) | Session["UID"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }
         
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    public DataSet BUSDetails(string loginid, string usertype, string fromdate, string todate, string orderid, string agentid, string paymentStatus, string busoptertor, string source ,string destination)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myCon"].ConnectionString);
        DataSet DS = new DataSet();
        try
        {
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                sqlcmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                sqlcmd.CommandTimeout = 900;
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "SP_BUS_SALEREGISTER";
                sqlcmd.Parameters.Add("@usertype", SqlDbType.VarChar).Value = usertype;
                sqlcmd.Parameters.Add("@LoginID", SqlDbType.VarChar).Value = loginid;
                sqlcmd.Parameters.Add("@FormDate", SqlDbType.VarChar).Value = fromdate;
                sqlcmd.Parameters.Add("@ToDate", SqlDbType.VarChar).Value = todate;
                sqlcmd.Parameters.Add("@OrderID", SqlDbType.VarChar).Value = orderid;
                sqlcmd.Parameters.Add("@AgentId", SqlDbType.VarChar).Value = agentid;
                sqlcmd.Parameters.Add("@PaymentStatus", SqlDbType.VarChar).Value = paymentStatus;
                sqlcmd.Parameters.Add("@busopretor", SqlDbType.VarChar).Value = busoptertor;
                sqlcmd.Parameters.Add("@source", SqlDbType.VarChar).Value = source;
                sqlcmd.Parameters.Add("@destination", SqlDbType.VarChar).Value = destination;

                SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
                da.Fill(DS);
                con.Close();
                DS.Dispose();
                con.Close();
            }
        }
        catch (Exception ex)
        {
        }
        return DS;
    }
    protected void btn_export_Click(object sender, System.EventArgs e)
    {
        try
        {
            string FromDate = null;
            string ToDate = null;
            int lenth = 0;
           // string PgStatus = drpPaymentStatus.Visible == true ? drpPaymentStatus.SelectedValue.ToLower() != "select" ? drpPaymentStatus.SelectedValue : null : null;
            if (String.IsNullOrEmpty(Request["From"]))
            {
                FromDate = "";
            }
            else
            {
                FromDate = Request["From"].Substring(3, 2) + "/" + Request["From"].Substring(0, 2) + "/" + Request["From"].Substring(6, 4);
                FromDate = FromDate + " " + "12:00:00 AM";
            }
            if (String.IsNullOrEmpty(Request["To"]))
            {
                ToDate = "";
            }
            else
            {
                ToDate = Request["To"].Substring(3, 2) + "/" + Request["To"].Substring(0, 2) + "/" + Request["To"].Substring(6, 4);
                ToDate = ToDate + " " + "11:59:59 PM";
            }


            string AgentID = String.IsNullOrEmpty(Request["txtAgencyName"]) ? "" : Request["txtAgencyName"];
            string OrderID = String.IsNullOrEmpty(txtOrderID.Text) ? "" : txtOrderID.Text.Trim();
            string busopretor = String.IsNullOrEmpty(txtOrderID.Text) ? "" : txtBusOperator.Text.Trim();
            string source = String.IsNullOrEmpty(TxtSource.Text) ? "" : TxtSource.Text.Trim();
            string destination = String.IsNullOrEmpty(TxtDestination.Text) ? "" : TxtDestination.Text.Trim();
            if (AgentID != "")
            {
                string str = AgentID;
                int pos1 = str.IndexOf("(");
                int pos2 = str.IndexOf(")");
                lenth = pos2 - pos1;
                string AgentID1 = str.Substring(pos1 + 1, lenth - 1);
                grdds.Clear();
                grdds = BUSDetails(Session["UID"].ToString(), Session["User_Type"].ToString(), FromDate, ToDate, OrderID, AgentID1, "", busopretor, source, destination);
                STDom.ExportData(grdds);
            }
            else
            {
                grdds.Clear();
                grdds = BUSDetails(Session["UID"].ToString(), Session["User_Type"].ToString(), FromDate, ToDate, OrderID, AgentID, "", busopretor, source, destination);             
                STDom.ExportData(grdds);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }

    }
    protected void GridView1_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
        try
        {
            GrdBusReport.PageIndex = e.NewPageIndex;
            GrdBusReport.DataSource = ViewState["grdds"];
            GrdBusReport.DataBind();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);

        }
    }
    protected void btn_result_Click(object sender, EventArgs e)
    {
        CheckEmptyValue();
        ClearInputs(Page.Controls);
    }
    void ClearInputs(ControlCollection ctrls)
    {
        try
        {
            foreach (Control ctrl in ctrls)
            {
                if (ctrl is TextBox)
                    ((TextBox)ctrl).Text = string.Empty;
                ClearInputs(ctrl.Controls);
            }
        }
        catch (Exception ex)
        {
        }

    }

}	