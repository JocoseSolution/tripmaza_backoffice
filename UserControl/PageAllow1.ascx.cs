﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class PageAllow1 : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

        BindRepeater();
    }


    //private static DataTable GetData(string query)
    //{

    //    string constr = ConfigurationManager.ConnectionStrings["reg_connection"].ConnectionString;
    //    SqlConnection con = new SqlConnection(constr);
    //    con.Open();

    //    SqlCommand cmd = new SqlCommand();
    //    cmd.CommandText = query;

    //    SqlDataAdapter sda = new SqlDataAdapter();

    //    cmd.Connection = con;
    //    sda.SelectCommand = cmd;
    //    DataTable dt = new DataTable();

    //    sda.Fill(dt);
    //    con.Close();

    //    return dt;
    //}
    //protected void BindRepeater()
    //{
    //  //  Repeater1.DataSource = GetData("select r1.Role,pd1.Page_name,pd1.Page_url from Page_Authorized p1 inner join Role_tab r1 on p1.Role_id=r1.Role_id inner join Page_Details pd1 on p1.Page_id=pd1.Page_id where r1.Role_id=1");


    //    Repeater1.DataSource = GetData("select p1.Page_name,p1.Page_url from Page_Details p1 inner join Page_Authorized p2 on p1.Page_id=p2.Page_id where p2.Role_id=1 and p1.Is_Parent_Page='Y'");
    //    Repeater1.DataBind();


    //}


    public void BindRepeater()
    {


          try
        {

            int Role_id = Convert.ToInt16( Session["Role_Id"]);

            //int Role_id = Session["xyz"];

            string is_parentpage ="Y";

            string constr = ConfigurationManager.ConnectionStrings["myAmdDB1"].ConnectionString;
            SqlConnection con = new SqlConnection(constr);



            SqlCommand cmd = new SqlCommand("ParentPageSP_PP");
              cmd.Connection = con;
              con.Open();

                   cmd.CommandType = CommandType.StoredProcedure;

                   cmd.Parameters.AddWithValue("@role_id", Role_id);
                   cmd.Parameters.AddWithValue("@is_parentpage", is_parentpage);

                   SqlDataAdapter sda = new SqlDataAdapter();
                   sda.SelectCommand = cmd;
                   DataTable dt = new DataTable();

                   sda.Fill(dt);


                   Repeater2.DataSource = dt;



                   Repeater2.DataBind();
                   con.Close();
                }
             
        catch (Exception ex)
        {
            throw ex;
        }
      
        
       
    }


    }
