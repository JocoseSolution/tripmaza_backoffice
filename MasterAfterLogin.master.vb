﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.Security
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Xml
Imports IPTracker

Partial Class MasterAfterLogin
    Inherits System.Web.UI.MasterPage
    Private id As String
    Private usertype As String
    Private typeid As String
    Private ds As DataSet
    Private dsm As DataSet
    Private con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    Private adap As SqlDataAdapter
    Private det As New Details()
    Private dtm As DataTable
    Private servtype As String
    Public strPubMenu As String = ""


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Session("UID") = "" Or Session("UID") Is Nothing Then
            Response.Redirect("Login.aspx")
        End If
        'int role_id = 1;


        Try
            If (Convert.ToBoolean(Session("DExport")) = False) Then

                Dim contentPage As ContentPlaceHolder = TryCast(FindControl("ContentPlaceHolder1"), ContentPlaceHolder)
                Dim lblExport As Button = CType(contentPage.FindControl("btn_export"), Button)
                Dim lblExport1 As Button = CType(contentPage.FindControl("export"), Button)
                Dim lblExport2 As Button = CType(contentPage.FindControl("BtnExport"), Button)

                If (lblExport IsNot Nothing) Then
                    lblExport.Visible = False
                End If

                If (lblExport1 IsNot Nothing) Then
                    lblExport1.Visible = False
                End If

                If (lblExport2 IsNot Nothing) Then
                    lblExport2.Visible = False
                End If



            End If
        Catch ex As Exception

        End Try
      

        If (Page.IsPostBack = False) Then
            ShowMenu()

        End If
        Dim Page_url As String = Request.Url.AbsolutePath
        Dim result As String = ""
        If Page_url = "/IBEHome.aspx" Then

            result = "valideuser"
        Else
            '' result = checkAuthorization()
        End If

        If result = "Invalideuser" Then
            Response.Redirect("Error.aspx")
        End If
        Try
            If Session("UID") <> "" AndAlso Session("UID") IsNot Nothing Then
                Dim State As New StateCollection()
                Dim objIP As New IPDetails()
                State.SessionID = Session.SessionID
                State.Path = Request.CurrentExecutionFilePath
                State.Username = Session("UID").ToString() 'Page.User.Identity.Name
                State.VISTING_TIME = DateTime.Now.ToString()
                Dim objST As New SessionTrack()
                objST.Add(State, Request.CurrentExecutionFilePath)
            End If

        Catch ex As Exception

        End Try
        Try
            If Not IsPostBack Then
                If Session("UID") <> "" AndAlso Session("UID") IsNot Nothing AndAlso Session("UserType") <> "" AndAlso Session("UserType") IsNot Nothing AndAlso Session("TypeID") <> "" AndAlso Session("TypeID") IsNot Nothing Then
                    id = Session("UID").ToString()
                    usertype = Session("UserType").ToString()
                    servtype = "Flight"
                    'div_Series.Visible = False
                    typeid = Session("TypeID").ToString()
                    If usertype = "AD" Then
                        'lblagency.Text = Session("ADMINLogin")
                        'crdrow.Visible = False
                        'tr_AgencyID.Visible = False
                    ElseIf usertype = "AC" Then
                        'lblagency.Text = "Accounts"
                        'crdrow.Visible = False
                        'tr_AgencyID.Visible = False
                    ElseIf usertype = "EC" Then
                        'lblagency.Text = Session("UID").ToString()
                        'crdrow.Visible = False
                        'tr_AgencyID.Visible = False
                    ElseIf usertype = "SE" Then
                        'lblagency.Text = Session("UID").ToString()
                        'crdrow.Visible = False
                        'tr_AgencyID.Visible = False
                    ElseIf usertype = "TA" Then
                        '' ds = det.AgencyInfo(id)
                        ''If ds.Tables(0).Rows.Count > 0 Then
                        'lblagency.Text = Session("AgencyName")
                        'lblCamt.Visible = True
                        ' ''''lblCamt.Text = " INR " & Convert.ToDouble(ds.Tables(0).Rows(0)("crd_limit").ToString())
                        ' ''td_AgencyID.InnerText = ds.Tables(0).Rows(0)("user_id").ToString()
                        'td_AgencyID.InnerText = Session("UID")

                        'lblagency.Text = Session("AgencyName")
                        'Session("AGTY") = ds.Tables(0).Rows(0)("Agent_Type").ToString()
                        'Session("agent_type") = ds.Tables(0).Rows(0)("Agent_Type").ToString()
                        'Session("MchntKeyITZ") = ds.Tables(0).Rows(0)("MerchantKey_ITZ").ToString().Trim()
                        ' ''''Session("ModeTypeITZ") = ds.Tables(0).Rows(0)("ModeType_ITZ").ToString().Trim()
                        'Session("_DCODE") = ds.Tables(0).Rows(0)("Decode_ITZ").ToString().Trim()
                        'Session("_SvcTypeITZ") = ds.Tables(0).Rows(0)("SvcType_ITZ").ToString().Trim()
                        'If ds.Tables(0).Rows(0)("Distr").ToString() <> "SPRING" Then
                        '    'div_ccpay.Visible = False
                        'End If
                        '' End If
                        'Marquee Message

                        'Try
                        '    dsm = det.GetMarquueemsg(servtype)
                        '    If dsm.Tables(0).Rows.Count > 0 Then
                        '        Dim msg As String = ""
                        '        For Each row As DataRow In dsm.Tables(0).Rows

                        '            msg += row("Message").ToString() & " ."
                        '        Next row

                        '        tdmarquee.InnerText = msg



                        '        'tdmarquee.InnerText = dsm.Tables(0).Rows(0)("Message").ToString()


                        '    End If
                        'Catch ex As Exception

                        'End Try





                        'BEGIN CHANGES FOR DISTR
                    ElseIf usertype = "DI" Then
                        'divflt.Visible = False
                        'divhtl.Visible = False
                        'div_Rail.Visible = False
                        'div_Bus.Visible = False
                        'div_Utility.Visible = False
                        'div_BillPayment.Visible = False
                        'div_Series.Visible = False
                        ds = det.AgencyInfo(id)
                        If ds.Tables(0).Rows.Count > 0 Then
                            'lblagency.Text = ds.Tables(0).Rows(0)("Agency_Name").ToString()
                            'lblCamt.Visible = True
                            'lblCamt.Text = " INR " & Convert.ToDouble(ds.Tables(0).Rows(0)("crd_limit").ToString())
                            'td_AgencyID.InnerText = ds.Tables(0).Rows(0)("user_id").ToString()
                            'lblagency.Text = Session("AgencyName")
                            ''Session("AGTY") = ds.Tables(0).Rows(0)("Agent_Type").ToString()
                        End If
                        'END CHANGES FOR DISTR
                    End If

                    If Session("User_Type") = "ACC" Or Session("User_Type") = "SALES" Then
                        ' div_menu.Visible = False
                        'hypdeal.Visible = False
                    End If
                    If Session("User_Type") = "EXEC" Then
                        'div_Rail.Visible = False
                        'div_Utility.Visible = False
                        'div_Series.Visible = False
                    End If

                    If Session("User_Type") = "ADMIN" Then
                        ' hypdeal.Visible = False
                    End If
                    If typeid = "TA2" Then
                        'div_Rail.Visible = False
                        'divflt.Visible = False
                        'divhtl.Visible = False
                        'div_Series.Visible = False
                        'div_Utility.Visible = False
                        'hypdeal.Visible = False
                    End If

                    'If Session("User_Type") = "A" Or Session("User_Type") = "EXEC" Then
                    '    div_menu.Visible = False
                    'End If



                ElseIf Session("UID") Is Nothing AndAlso Session("UserType") Is Nothing AndAlso Session("TypeID") Is Nothing Then

                    Response.Redirect("~/Login.aspx?reason=Session TimeOut")
                End If
                ''  ShowMenu()
                'RowMenu.Visible = False

                If (Request.UserAgent.IndexOf("AppleWebKit") > 0) Then
                    Request.Browser.Adapters.Clear()
                End If


            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Public Function checkAuthorization() As String
        Try

            ' Dim role_id As Integer = 3

            Dim role_id As Integer = Session("Role_id")


            Dim Page_url As String = "~" + Request.Url.AbsolutePath



            Dim constr As String = ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString
            Dim con As New SqlConnection(constr)
            If con.State = ConnectionState.Closed Then
                con.Open()
            End If

            Dim cmd As New SqlCommand("CheckPageAuthorization_PP", con)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@pageurl", Page_url)
            cmd.Parameters.AddWithValue("@Role", role_id)

            Dim ret As String = ""
            ret = cmd.ExecuteScalar().ToString()
            If con.State = ConnectionState.Open Then
                con.Close()
            End If
            '' Return "valideuser"
            Return ret
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Protected Sub lnklogout_Click1(ByVal sender As Object, ByVal e As EventArgs) Handles lnklogout.Click
    '    Try
    '        FormsAuthentication.SignOut()
    '        Session.Abandon()
    '        Response.Redirect("~/Login.aspx")
    '    Catch ex As Exception
    '        clsErrorLog.LogInfo(ex)
    '    End Try

    'End Sub
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1))
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetNoStore()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Public Sub ShowMenu()
        Try
            'If Session("getMenuItem") = Nothing Then
            Dim dset As New DataSet
            adap = New SqlDataAdapter("getMenu", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure

            adap.SelectCommand.Parameters.AddWithValue("@roleid", Session("Role_Id"))
            adap.Fill(dset)


            'Dim strmenu As String = " <aside class='navigation'><nav><ul class='nav luna-navs'><li class='nav-category'></li>"
            Dim strmenu As String = " <ul>"
            Dim i As Integer = 0
            For i = 0 To dset.Tables(0).Rows.Count - 1
                If dset.Tables(0).Rows(i)("Is_Parent_Page").ToString().Trim() = "Y" Then


                    strmenu = strmenu & "<li id=''" & dset.Tables(0).Rows(i)("Page_name").ToString() & "  class='sidebar-dropdown'><a href='#'><i class='icofont-hand-right'></i> <span>" & dset.Tables(0).Rows(i)("Page_name").ToString() & "</span></a><div class='sidebar-submenu'>"
                    'strmenu = strmenu & "<ul id=''" & dset.Tables(0).Rows(i)("Page_name").ToString() & " class='finner nav nav-second-level ' style='Display: none;'>"
                    For k = 0 To dset.Tables(0).Rows.Count - 1
                        If (dset.Tables(0).Rows(i)("page_id").ToString() = dset.Tables(0).Rows(k)("Root_page_id").ToString()) Then

                            ''     If ("https://seeingo.in/fixedDeparture/FlightSearchResults" = dset.Tables(0).Rows(k)("Page_url").ToString) Then
                            '' If ("http://220.158.167.31/fixedDeparture/FlightSearchResults" = dset.Tables(0).Rows(k)("Page_url").ToString) Then
                            If ("https://www.flywidus.co/FixedDeparture/FlightSearchResults" = dset.Tables(0).Rows(k)("Page_url").ToString) Then
                                Dim R1 As String = ""
                                If Session("UID") = "" Or Session("UID") Is Nothing Then
                                    Response.Redirect("Login.aspx")
                                Else


                                    '' R1 = Encrypt(Session("UID") & "-" & Session("_PASSWORD") & "-" & DateTime.Now.ToString("yyMMddHHmmssff"))
                                End If



                                Dim url As String = ""
                                '' QueryKeyI(R1)
                                ''url = Request.Url.Host & "/fixedDeparture/FlightSearchResults"
                                url = dset.Tables(0).Rows(k)("Page_url").ToString
                                '' strmenu = strmenu & "<li> <a href=" & url & "?R1=" & R1 & "><i class='fa fa-angle-down' aria-hidden='true'></i>&nbsp;&nbsp;&nbsp; " & dset.Tables(0).Rows(k)("Page_name").ToString() & "</a></li>"
                                'strmenu = strmenu & "<li> <a href='#' onclick='Direct()'><i class='fa fa-angle-down' aria-hidden='true'></i>&nbsp;&nbsp;&nbsp; FixedDeparture</a></li>"
                            Else
                                strmenu = strmenu & "<ul><li> <a style='padding: 6px 18px 5px 0px;' href=" & ResolveClientUrl("~" & dset.Tables(0).Rows(k)("Page_url")) & ">" & dset.Tables(0).Rows(k)("Page_name").ToString() & "</a></li></ul>"
                            End If

                        End If

                    Next
                    strmenu = strmenu & "</div></li><hr>"
                End If
            Next
            strmenu = strmenu & "</ul>"
            'Session("getMenuItem") = strmenu
            Ul_Menu.InnerHtml = strmenu
            adap.Dispose()
            'Else
            '    Ul_Menu.InnerHtml = Session("getMenuItem")

            'End If


            'Dim xmld As New XmlDataSource
            'xmld.ID = "XmlDataSource1"
            'xmld.EnableCaching = False
            'dset.DataSetName = "Menus"
            'dset.Tables(0).TableName = "abc"
            'Dim relation As New DataRelation("ParentChild", dset.Tables("abc").Columns("Page_ID"), dset.Tables("abc").Columns("PageParent_ID"), True)
            'relation.Nested = True
            'dset.Relations.Add(relation)
            'xmld.Data = dset.GetXml()
            'xmld.TransformFile = Server.MapPath("~/Transform.xslt")
            'xmld.XPath = "MenuItems/MenuItem"
            'Menu1.DataSource = xmld
            'Menu1.DataBind()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub


    'Public Sub ShowMenu()
    '    Try
    '        Dim dset As New DataSet
    '        adap = New SqlDataAdapter("GetURL", con)
    '        adap.SelectCommand.CommandType = CommandType.StoredProcedure
    '        adap.SelectCommand.Parameters.AddWithValue("@typeid", typeid)
    '        adap.Fill(dset)
    '        adap.Dispose()
    '        Dim xmld As New XmlDataSource
    '        xmld.ID = "XmlDataSource1"
    '        xmld.EnableCaching = False
    '        dset.DataSetName = "Menus"
    '        dset.Tables(0).TableName = "abc"
    '        Dim relation As New DataRelation("ParentChild", dset.Tables("abc").Columns("Page_ID"), dset.Tables("abc").Columns("PageParent_ID"), True)
    '        relation.Nested = True
    '        dset.Relations.Add(relation)
    '        xmld.Data = dset.GetXml()
    '        xmld.TransformFile = Server.MapPath("~/Transform.xslt")
    '        xmld.XPath = "MenuItems/MenuItem"
    '        'Menu1.DataSource = xmld
    '        'Menu1.DataBind()
    '    Catch ex As Exception
    '        clsErrorLog.LogInfo(ex)
    '    End Try
    'End Sub

    'Protected Sub lnkDash_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDash.Click
    '    menu.Visible = True
    'End Sub

    'Protected Sub lnkDash_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDash.Click
    '    RowMenu.Visible = True
    'End Sub

    'Protected Sub lnkflight_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkflight.Click
    '    RowMenu.Visible = False
    '    Response.Redirect("~/IBEHome.aspx")
    'End Sub

    'Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

    '    'int role_id = 1;
    '    Dim Page_url As String = Request.Url.AbsolutePath
    '    Dim result As String = checkAuthorization()


    '    If result = "Invalideuser" Then
    '        Response.Redirect("Error.aspx")
    '    End If

    '    Try
    '        If Session("UID") <> "" AndAlso Session("UID") IsNot Nothing Then
    '            Dim State As New StateCollection()
    '            Dim objIP As New IPDetails()
    '            State.SessionID = Session.SessionID
    '            State.Path = Request.CurrentExecutionFilePath
    '            State.Username = Session("UID").ToString() 'Page.User.Identity.Name
    '            State.VISTING_TIME = DateTime.Now.ToString()
    '            Dim objST As New SessionTrack()
    '            objST.Add(State, Request.CurrentExecutionFilePath)
    '        End If

    '    Catch ex As Exception

    '    End Try
    '    Try
    '        If Not IsPostBack Then
    '            If Session("UID") <> "" AndAlso Session("UID") IsNot Nothing AndAlso Session("UserType") <> "" AndAlso Session("UserType") IsNot Nothing AndAlso Session("TypeID") <> "" AndAlso Session("TypeID") IsNot Nothing Then
    '                id = Session("UID").ToString()
    '                usertype = Session("UserType").ToString()
    '                servtype = "Flight"
    '                'div_Series.Visible = False
    '                typeid = Session("TypeID").ToString()
    '                If usertype = "AD" Then
    '                    lblagency.Text = Session("ADMINLogin")
    '                    crdrow.Visible = False
    '                    tr_AgencyID.Visible = False
    '                ElseIf usertype = "AC" Then
    '                    lblagency.Text = "Accounts"
    '                    crdrow.Visible = False
    '                    tr_AgencyID.Visible = False
    '                ElseIf usertype = "EC" Then
    '                    lblagency.Text = Session("UID").ToString()
    '                    crdrow.Visible = False
    '                    tr_AgencyID.Visible = False
    '                ElseIf usertype = "SE" Then
    '                    lblagency.Text = Session("UID").ToString()
    '                    crdrow.Visible = False
    '                    tr_AgencyID.Visible = False
    '                ElseIf usertype = "TA" Then
    '                    '' ds = det.AgencyInfo(id)
    '                    ''If ds.Tables(0).Rows.Count > 0 Then
    '                    lblagency.Text = Session("AgencyName")
    '                    lblCamt.Visible = True
    '                    ''''lblCamt.Text = " INR " & Convert.ToDouble(ds.Tables(0).Rows(0)("crd_limit").ToString())
    '                    ''td_AgencyID.InnerText = ds.Tables(0).Rows(0)("user_id").ToString()
    '                    td_AgencyID.InnerText = Session("UID")

    '                    lblagency.Text = Session("AgencyName")
    '                    'Session("AGTY") = ds.Tables(0).Rows(0)("Agent_Type").ToString()
    '                    'Session("agent_type") = ds.Tables(0).Rows(0)("Agent_Type").ToString()
    '                    'Session("MchntKeyITZ") = ds.Tables(0).Rows(0)("MerchantKey_ITZ").ToString().Trim()
    '                    ' ''''Session("ModeTypeITZ") = ds.Tables(0).Rows(0)("ModeType_ITZ").ToString().Trim()
    '                    'Session("_DCODE") = ds.Tables(0).Rows(0)("Decode_ITZ").ToString().Trim()
    '                    'Session("_SvcTypeITZ") = ds.Tables(0).Rows(0)("SvcType_ITZ").ToString().Trim()
    '                    'If ds.Tables(0).Rows(0)("Distr").ToString() <> "SPRING" Then
    '                    '    'div_ccpay.Visible = False
    '                    'End If
    '                    '' End If
    '                    'Marquee Message

    '                    'Try
    '                    '    dsm = det.GetMarquueemsg(servtype)
    '                    '    If dsm.Tables(0).Rows.Count > 0 Then
    '                    '        Dim msg As String = ""
    '                    '        For Each row As DataRow In dsm.Tables(0).Rows

    '                    '            msg += row("Message").ToString() & " ."
    '                    '        Next row

    '                    '        tdmarquee.InnerText = msg



    '                    '        'tdmarquee.InnerText = dsm.Tables(0).Rows(0)("Message").ToString()


    '                    '    End If
    '                    'Catch ex As Exception

    '                    'End Try





    '                    'BEGIN CHANGES FOR DISTR
    '                ElseIf usertype = "DI" Then
    '                    'divflt.Visible = False
    '                    'divhtl.Visible = False
    '                    'div_Rail.Visible = False
    '                    'div_Bus.Visible = False
    '                    'div_Utility.Visible = False
    '                    'div_BillPayment.Visible = False
    '                    'div_Series.Visible = False
    '                    ds = det.AgencyInfo(id)
    '                    If ds.Tables(0).Rows.Count > 0 Then
    '                        lblagency.Text = ds.Tables(0).Rows(0)("Agency_Name").ToString()
    '                        lblCamt.Visible = True
    '                        lblCamt.Text = " INR " & Convert.ToDouble(ds.Tables(0).Rows(0)("crd_limit").ToString())
    '                        td_AgencyID.InnerText = ds.Tables(0).Rows(0)("user_id").ToString()
    '                        lblagency.Text = Session("AgencyName")
    '                        ''Session("AGTY") = ds.Tables(0).Rows(0)("Agent_Type").ToString()
    '                    End If
    '                    'END CHANGES FOR DISTR
    '                End If

    '                If Session("User_Type") = "ACC" Or Session("User_Type") = "SALES" Then
    '                    ' div_menu.Visible = False
    '                    'hypdeal.Visible = False
    '                End If
    '                If Session("User_Type") = "EXEC" Then
    '                    'div_Rail.Visible = False
    '                    'div_Utility.Visible = False
    '                    'div_Series.Visible = False
    '                End If

    '                If Session("User_Type") = "ADMIN" Then
    '                    ' hypdeal.Visible = False
    '                End If
    '                If typeid = "TA2" Then
    '                    'div_Rail.Visible = False
    '                    'divflt.Visible = False
    '                    'divhtl.Visible = False
    '                    'div_Series.Visible = False
    '                    'div_Utility.Visible = False
    '                    'hypdeal.Visible = False
    '                End If

    '                'If Session("User_Type") = "A" Or Session("User_Type") = "EXEC" Then
    '                '    div_menu.Visible = False
    '                'End If



    '            ElseIf Session("UID") Is Nothing AndAlso Session("UserType") Is Nothing AndAlso Session("TypeID") Is Nothing Then

    '                Response.Redirect("~/Login.aspx?reason=Session TimeOut")
    '            End If
    '            ShowMenu()
    '            'RowMenu.Visible = False

    '            If (Request.UserAgent.IndexOf("AppleWebKit") > 0) Then
    '                Request.Browser.Adapters.Clear()
    '            End If


    '        End If
    '    Catch ex As Exception
    '        clsErrorLog.LogInfo(ex)
    '    End Try
    'End Sub

End Class

