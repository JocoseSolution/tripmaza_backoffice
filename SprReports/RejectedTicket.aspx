﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="RejectedTicket.aspx.vb" Inherits="SprReports_RejectedTicket" %>

<%@ Register Src="~/UserControl/LeftMenu.ascx" TagPrefix="uc1" TagName="LeftMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />

    <%-- <link href="../CSS/main2.css" rel="stylesheet" type="text/css" />--%>
    <%--<link rel="stylesheet" href="../chosen/chosen.css" />--%>
    <%-- <link href="../CSS/style.css" rel="stylesheet" type="text/css" />--%>

    <%--    <script src="../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>

    <script src="../chosen/chosen.jquery.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $(".chzn-select").chosen();
            $(".chzn-select-deselect").chosen({ allow_single_deselect: true });
        });
    </script>--%>
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow: auto;
        }

        .panel-primary > .panel-heading {
            width: 97.5%;
            color: #fff;
            margin-left: 13px;
            background-color: #f3f6ff;
            border: 1px solid #000;
        }

        .panel {
            border: 1px solid #fefefe;
        }

        h3 {
            color: #032451 !important;
        }
    </style>

    <div class="row">
        <div class="col-md-12" style="margin-top: 20px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight > Search Flight Rejected Ticket</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <input type="text" name="From" id="From" placeholder="From Date.." class="theme-search-area-section-input" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <input type="text" name="To" id="To" placeholder="To Date.." class="theme-search-area-section-input" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:TextBox ID="txt_PNR" runat="server" placeholder="Enter Pnr.." CssClass="theme-search-area-section-input"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:TextBox ID="txt_OrderId" runat="server" placeholder="Enter Orderid.." CssClass="theme-search-area-section-input"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:TextBox ID="txt_PaxName" runat="server" placeholder="Enter Pax Name.." CssClass="theme-search-area-section-input"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <<div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:DropDownList ID="ddlTrip" runat="server" CssClass="theme-search-area-section-input">
                                                <asp:ListItem Text="--Select Trip--" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Domestic" Value="D"></asp:ListItem>
                                                <asp:ListItem Text="International" Value="I"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>


                            </div>


                            <div class="row">
                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:TextBox ID="txt_TktNo" runat="server" placeholder="Enter Tkt No.." CssClass="theme-search-area-section-input"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:TextBox ID="txt_AirPNR" runat="server" placeholder="Enter Air Line.." CssClass="theme-search-area-section-input"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4" id="td_Agency" runat="server">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <input type="text" id="txtAgencyName" name="txtAgencyName" onfocus="focusObj(this);"
                                                onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID" class="theme-search-area-section-input" />
                                            <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-4" id="divPaymentMode" runat="server">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:DropDownList CssClass="theme-search-area-section-input" ID="txtPaymentmode" runat="server">
                                                <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                                <asp:ListItem Text="PG" Value="pg"></asp:ListItem>
                                                <asp:ListItem Text="Wallet" Value="wallet"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4" id="divPartnerName" runat="server">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:DropDownList CssClass="theme-search-area-section-input" ID="txtPartnerName" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>


                            </div>



                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <asp:Button ID="btn_result" runat="server" Text="Search Result" CssClass="button buttonBlue" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <asp:Button ID="btn_export" runat="server" CssClass="button buttonBlue" Text="Export" />
                                    </div>
                                </div>

                            </div>
                        </div>



                        <div class="row">
                            <div class="col-md-9">
                                <span style="color: #FF0000">* N.B: To get Today's booking without above parameter,do not fill any field, only click on search your booking.</span>
                            </div>
                        </div>

                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="row" id="divReport" runat="server" visible="true">
                                <div class="col-md-12">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow: auto; max-height: 500px;">
                                        <ContentTemplate>
                                            <asp:GridView ID="ticket_grdview" runat="server" AllowPaging="True"
                                                AutoGenerateColumns="False" CssClass="table"
                                                GridLines="None" Width="100%" PageSize="30">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Order ID">
                                                        <ItemTemplate>
                                                            <%-- <asp:Label ID="OrderID" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label>--%>
                                                            <a data-toggle="modal" data-target="#myModal" href='PnrSummaryIntl.aspx?OrderId=<%#Eval("OrderId")%> &TransID=' rel="lyteframe"
                                                                rev="width: 900px; height: 500px; overflow:hidden;" target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                                <asp:Label ID="OrderID" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label></a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Pnr">
                                                        <ItemTemplate>
                                                            <asp:Label ID="GdsPNR" runat="server" Text='<%#Eval("GdsPnr")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="AgencyId">
                                                        <ItemTemplate>
                                                            <asp:Label ID="AgentID" runat="server" Text='<%#Eval("AgencyID")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="UserId">
                                                        <ItemTemplate>
                                                            <asp:Label ID="AgentID" runat="server" Text='<%#Eval("UserId")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Executive ID">
                                                        <ItemTemplate>
                                                            <asp:Label ID="ExcutiveID" runat="server" Text='<%#Eval("ExecutiveId")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Rejected Remark">
                                                        <ItemTemplate>
                                                            <asp:Label ID="RejectRemark" runat="server" Text='<%#Eval("RejectedRemark")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="AirLine">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Airline" runat="server" Text='<%#Eval("VC")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Partner Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_CreateDate" runat="server" Text='<%#Eval("PartnerName")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:BoundField HeaderText="Sector" DataField="sector"></asp:BoundField>
                                                    <asp:BoundField HeaderText="Trip" DataField="trip"></asp:BoundField>
                                                    <asp:BoundField HeaderText="Net Fare" DataField="TotalAfterDis">
                                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="Convenience Fee">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_PGcharges" runat="server" Text='<%#Eval("PgCharges")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Status" DataField="Status"></asp:BoundField>
                                                    <asp:BoundField HeaderText="Booking Date" DataField="CreateDate"></asp:BoundField>
                                                    <asp:BoundField HeaderText="Rejected Date" DataField="RejectDate"></asp:BoundField>

                                                    <asp:TemplateField HeaderText="Payment Mode">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_PaymentMode" runat="server" Text='<%#Eval("PaymentMode")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                </Columns>
                                                <RowStyle CssClass="RowStyle" />
                                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                <PagerStyle CssClass="PagerStyle" />
                                                <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                <HeaderStyle CssClass="HeaderStyle" />
                                                <EditRowStyle CssClass="EditRowStyle" />
                                                <AlternatingRowStyle CssClass="AltRowStyle" />
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
</asp:Content>
