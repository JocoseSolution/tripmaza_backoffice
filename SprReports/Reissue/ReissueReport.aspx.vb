﻿Imports System.Collections.Generic
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data.SqlClient
Imports System.Data
Imports System.Configuration
Partial Class SprReports_Reissue_ReissueReport
    Inherits System.Web.UI.Page
    Private STDom As New SqlTransactionDom()
    Dim AgencyDDLDS As New DataSet()
    Dim objDA As New SqlTransaction
    Dim con As New SqlConnection()
    Dim con1 As New SqlConnection()
    Dim adp As SqlDataAdapter
    Public pnrds As New DataSet
    Dim ds As New DataSet()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Try

            If Session("UID") = "" Or Session("UID") Is Nothing Then
                Response.Redirect("~/Login.aspx")
            End If
            divPaymentMode.Visible = False
            'partnername.Visible = False
            If Session("User_Type") = "AGENT" Then
                td_Agency.Visible = False
                'partnername.Visible = False

            End If
            If Session("User_Type") = "EXEC" Then
                tr_ExecID.Visible = False
                tdTripNonExec1.Visible = False
                'tdTripNonExec2.Visible = False
            End If
            If Not IsPostBack Then
                BindPartner()

                Dim dtExecutive As New DataTable
                Dim dtStatus As New DataTable
                dtStatus = STDom.GetStatusExecutiveID("Reissue").Tables(0)
                dtExecutive = STDom.GetStatusExecutiveID("Reissue").Tables(1)

                ddl_ExecID.AppendDataBoundItems = True
                ddl_ExecID.Items.Clear()
                ddl_ExecID.Items.Insert(0, "--Select--")
                ddl_ExecID.DataSource = dtExecutive
                ddl_ExecID.DataTextField = "ExecutiveID"
                ddl_ExecID.DataValueField = "ExecutiveID"
                ddl_ExecID.DataBind()

                ddl_Status.AppendDataBoundItems = True
                ddl_Status.Items.Clear()
                ddl_Status.Items.Insert(0, "--Select--")
                ddl_Status.DataSource = dtStatus
                ddl_Status.DataTextField = "Status"
                ddl_Status.DataValueField = "Status"
                ddl_Status.DataBind()



                'AgencyDDLDS = objDA.GetAgencyDetailsDDL()
                'If AgencyDDLDS.Tables(0).Rows.Count > 0 Then
                '    ddl_AgencyName.AppendDataBoundItems = True
                '    ddl_AgencyName.Items.Clear()
                '    ddl_AgencyName.Items.Insert(0, "--Select Agency Name--")
                '    ddl_AgencyName.DataSource = AgencyDDLDS
                '    ddl_AgencyName.DataTextField = "Agency_Name"
                '    ddl_AgencyName.DataValueField = "user_id"
                '    ddl_AgencyName.DataBind()
                '    'BindGrid()
                'End If
            End If

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
    Public Sub CheckEmptyValue()
        Try
            pnrds.Clear()
            Dim FromDate As String
            Dim ToDate As String
            If [String].IsNullOrEmpty(Request("From")) Then
                FromDate = ""
            Else
                'FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + Strings.Left((Request("From")).Split(" ")(0), 2) + Strings.Right((Request("From")).Split(" ")(0), 4)

                FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "/" + Strings.Left((Request("From")).Split(" ")(0), 2) + "/" + Strings.Right((Request("From")).Split(" ")(0), 4)
                FromDate = FromDate + " " + "12:00:00 AM"
            End If
            If [String].IsNullOrEmpty(Request("To")) Then
                ToDate = ""
            Else
                ToDate = Mid((Request("To")).Split(" ")(0), 4, 2) & "/" & Left((Request("To")).Split(" ")(0), 2) & "/" & Right((Request("To")).Split(" ")(0), 4)
                ToDate = ToDate & " " & "11:59:59 PM"
            End If

            Dim AgentID As String = If([String].IsNullOrEmpty(Request("hidtxtAgencyName")) Or Request("hidtxtAgencyName") = "Agency Name or ID", "", Request("hidtxtAgencyName"))


            Dim UserID As String = Session("UID").ToString
            Dim UserType As String = Session("User_Type")
            Dim OrderID As String = If([String].IsNullOrEmpty(txt_OrderId.Text), "", txt_OrderId.Text.Trim)
            Dim PNR As String = If([String].IsNullOrEmpty(txt_PNR.Text), "", txt_PNR.Text.Trim)
            Dim PaxName As String = If([String].IsNullOrEmpty(txt_PaxName.Text), "", txt_PaxName.Text.Trim)
            Dim TicketNo As String = If([String].IsNullOrEmpty(txt_TktNo.Text), "", txt_TktNo.Text.Trim)
            Dim Air As String = If([String].IsNullOrEmpty(txt_AirPNR.Text), "", txt_AirPNR.Text.Trim)
            Dim Partnername As String = txtPartnerName.SelectedItem.Value
            Dim PaymentMode As String = txtPaymentmode.SelectedItem.Value
            If Partnername = "0" Then
                Partnername = ""

            End If
            Dim trip As String = ""
            'If Session("User_Type") = "EXEC" Then
            '    If [String].IsNullOrEmpty(Session("TripExec")) Then
            '        trip = ""
            '    Else
            '        trip = Session("TripExec").ToString().Trim()
            '    End If
            'Else
            '    trip = If([String].IsNullOrEmpty(ddlTripReissueDomIntl.SelectedItem.Value), "", ddlTripReissueDomIntl.SelectedItem.Value.Trim())
            'End If

            If Session("User_Type") = "EXEC" Then
                trip = ""
            End If

            If Session("User_Type") = "ADMIN" Then
                trip = If([String].IsNullOrEmpty(ddlTripReissueDomIntl.SelectedItem.Value), "", ddlTripReissueDomIntl.SelectedItem.Value.Trim())
            End If

            Dim ExecID As String
            If ddl_ExecID.SelectedIndex > 0 Then
                ExecID = ddl_ExecID.SelectedValue
            Else

                ExecID = ""
            End If

            Dim Status As String
            If ddl_Status.SelectedIndex > 0 Then
                Status = ddl_Status.SelectedValue
            Else
                Status = ""
            End If
            pnrds = objDA.GetReIssueDetail(UserID, UserType, FromDate, ToDate, OrderID, PNR, PaxName, TicketNo, Air, AgentID, ExecID, Status, trip.Trim(), Partnername, PaymentMode)
            'adp.Fill(pnrds)
            Session("pnrds") = pnrds
            grd_paxstatusinfo.DataSource = pnrds
            grd_paxstatusinfo.DataBind()
            If pnrds.Tables(0).Rows.Count > 0 Then
                divReport.Visible = True
            Else
                divReport.Visible = False
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
    Private Sub BindGrid()


        Try
            pnrds.Clear()

            Dim curr_date = Now.Date() & " " & "12:00:00 AM"
            Dim curr_date1 = Now()

            pnrds = objDA.GetReissueDetailCurrent(Session("User_Type"), Session("UID"), curr_date, curr_date1, "D")

            Session("pnrds") = pnrds
            grd_paxstatusinfo.DataSource = pnrds
            grd_paxstatusinfo.DataBind()
            If pnrds.Tables(0).Rows.Count > 0 Then
                divReport.Visible = True
            Else
                divReport.Visible = False
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try

    End Sub
    Protected Sub btn_result_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_result.Click
        CheckEmptyValue()
        txtPartnerName.SelectedValue = "0"
        txtPaymentmode.SelectedValue = "All"
    End Sub

    Protected Sub grd_paxstatusinfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grd_paxstatusinfo.PageIndexChanging
        Try
            grd_paxstatusinfo.PageIndex = e.NewPageIndex
            grd_paxstatusinfo.DataSource = Session("pnrds")
            grd_paxstatusinfo.DataBind()
            If DirectCast(grd_paxstatusinfo.DataSource, DataSet).Tables(0).Rows.Count > 0 Then
                divReport.Visible = True
            Else
                divReport.Visible = False
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
    Protected Sub btn_export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_export.Click
        Try
            pnrds.Clear()
            Dim FromDate As String
            Dim ToDate As String
            If [String].IsNullOrEmpty(Request("From")) Then
                FromDate = ""
            Else
                'FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + Strings.Left((Request("From")).Split(" ")(0), 2) + Strings.Right((Request("From")).Split(" ")(0), 4)

                FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "/" + Strings.Left((Request("From")).Split(" ")(0), 2) + "/" + Strings.Right((Request("From")).Split(" ")(0), 4)
                FromDate = FromDate + " " + "12:00:00 AM"
            End If
            If [String].IsNullOrEmpty(Request("To")) Then
                ToDate = ""
            Else
                ToDate = Mid((Request("To")).Split(" ")(0), 4, 2) & "/" & Left((Request("To")).Split(" ")(0), 2) & "/" & Right((Request("To")).Split(" ")(0), 4)
                ToDate = ToDate & " " & "11:59:59 PM"
            End If

            Dim AgentID As String = If([String].IsNullOrEmpty(Request("hidtxtAgencyName")) Or Request("hidtxtAgencyName") = "Agency Name or ID", "", Request("hidtxtAgencyName"))

            Dim UserID As String = Session("UID").ToString
            Dim UserType As String = Session("User_Type")
            Dim OrderID As String = If([String].IsNullOrEmpty(txt_OrderId.Text), "", txt_OrderId.Text.Trim)
            Dim PNR As String = If([String].IsNullOrEmpty(txt_PNR.Text), "", txt_PNR.Text.Trim)
            Dim PaxName As String = If([String].IsNullOrEmpty(txt_PaxName.Text), "", txt_PaxName.Text.Trim)
            Dim TicketNo As String = If([String].IsNullOrEmpty(txt_TktNo.Text), "", txt_TktNo.Text.Trim)
            Dim Air As String = If([String].IsNullOrEmpty(txt_AirPNR.Text), "", txt_AirPNR.Text.Trim)
            Dim Partnername As String = txtPartnerName.SelectedItem.Value
            Dim PaymentMode As String = txtPaymentmode.SelectedItem.Value
            If Partnername = "0" Then
                Partnername = ""

            End If
            Dim trip As String = ""
            'If Session("User_Type") = "EXEC" Then
            '    If [String].IsNullOrEmpty(Session("TripExec")) Then
            '        trip = ""
            '    Else
            '        trip = Session("TripExec").ToString().Trim()
            '    End If
            'Else
            '    trip = If([String].IsNullOrEmpty(ddlTripReissueDomIntl.SelectedItem.Value), "", ddlTripReissueDomIntl.SelectedItem.Value.Trim())
            'End If

            If Session("User_Type") = "EXEC" Then
                trip = ""
            End If

            If Session("User_Type") = "ADMIN" Then
                trip = If([String].IsNullOrEmpty(ddlTripReissueDomIntl.SelectedItem.Value), "", ddlTripReissueDomIntl.SelectedItem.Value.Trim())
            End If
            Dim ExecID As String
            If ddl_ExecID.SelectedIndex > 0 Then
                ExecID = ddl_ExecID.SelectedValue
            Else

                ExecID = ""
            End If

            Dim Status As String
            If ddl_Status.SelectedIndex > 0 Then
                Status = ddl_Status.SelectedValue
            Else
                Status = ""
            End If
            pnrds = objDA.GetReIssueDetail(UserID, UserType, FromDate, ToDate, OrderID, PNR, PaxName, TicketNo, Air, AgentID, ExecID, Status, trip.Trim(), Partnername, PaymentMode)
            STDom.ExportData(pnrds)
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try

    End Sub
    Public Sub BindPartner()
        Dim constr As String = ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString
        Dim con As New SqlConnection(constr)
        Dim cmd As New SqlCommand("BindPartnerNameSP_PP")

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = con
        con.Open()
        txtPartnerName.DataSource = cmd.ExecuteReader()
        txtPartnerName.DataTextField = "PartnerName"
        txtPartnerName.DataValueField = "PartnerName"


        txtPartnerName.DataBind()
        con.Close()
        txtPartnerName.Items.Insert(0, New ListItem("--Select PartnerName--", "0"))
    End Sub
End Class
