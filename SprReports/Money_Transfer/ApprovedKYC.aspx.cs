﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_Money_Transfer_ApprovedKYC : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlDataAdapter adap;
    protected SqlTransactionDom STDom = new SqlTransactionDom();
    private string AgencyId { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        else
        {
            AgencyId = Session["UID"].ToString();
        }

        if (!IsPostBack)
        {
            pnlUpdateForm.Visible = true;
            pnlUpdateMessage.Visible = false;

            string remtid = Request.QueryString["remtid"] != null ? Request.QueryString["remtid"].ToString() : null;

            DataTable dtRemitter = GetDmtTrancReportList(remtid, null, null, null, null);
            if (dtRemitter != null && dtRemitter.Rows.Count > 0)
            {
                ddlType.SelectedValue = dtRemitter.Rows[0]["IsKYC"].ToString().ToLower() == "true" ? "1" : "0";
                ddlSatus.SelectedValue = dtRemitter.Rows[0]["KYCStatus"].ToString();
                txtRemark.Text = dtRemitter.Rows[0]["Remark"].ToString();
            }
        }
    }

    private DataTable GetDmtTrancReportList(string remtid, string agencyid = null, string fromDate = null, string toDate = null, string status = null)
    {
        DataTable dtDueReport = new DataTable();

        try
        {
            string newfromdate = string.Empty; string newtodate = string.Empty;

            if (!string.IsNullOrEmpty(fromDate))
            {
                newfromdate = String.Format(fromDate.Split('-')[2], 4) + "-" + String.Format(fromDate.Split('-')[1], 2) + "-" + String.Format(fromDate.Split('-')[0], 2);
            }
            if (!string.IsNullOrEmpty(toDate))
            {
                newtodate = String.Format(fromDate.Split('-')[2], 4) + "-" + String.Format(fromDate.Split('-')[1], 2) + "-" + String.Format(fromDate.Split('-')[0], 2);
            }

            adap = new SqlDataAdapter("sp_GetInstantPayDMTDirectRemitter", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@Id", remtid);
            adap.SelectCommand.Parameters.AddWithValue("@AgentId", !string.IsNullOrEmpty(agencyid) ? agencyid.Trim() : null);
            adap.SelectCommand.Parameters.AddWithValue("@FromDate", !string.IsNullOrEmpty(newfromdate) ? newfromdate.Trim() : null);
            adap.SelectCommand.Parameters.AddWithValue("@ToDate", !string.IsNullOrEmpty(newtodate) ? newtodate.Trim() + " 23:59:59" : null);
            adap.SelectCommand.Parameters.AddWithValue("@Status", !string.IsNullOrEmpty(status) ? status.Trim() : null);
            adap.Fill(dtDueReport);

        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return dtDueReport;
    }

    protected void btn_sumit_Click(object sender, EventArgs e)
    {
        string remtid = Request.QueryString["remtid"] != null ? Request.QueryString["remtid"].ToString() : null;

        if (!string.IsNullOrEmpty(remtid))
        {
            string KYCStatus = ddlSatus.SelectedValue;
            string iskyc = ddlType.SelectedValue;

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);

            string query = "update T_InstantPayDMTDirectRemitter set IsKYC=" + iskyc + ", KYCStatus='" + KYCStatus + "',Remark='" + txtRemark.Text.Trim() + "' where Id=" + remtid;
            SqlCommand cmd = new SqlCommand(query, con);
            con.Open();
            int status = cmd.ExecuteNonQuery();
            con.Close();

            if (status > 0)
            {
                txtRemark.Text = string.Empty;
                lblMessage.Text = "Remitter kyc detail updated successfully.";
                pnlUpdateForm.Visible = false;
                pnlUpdateMessage.Visible = true;

                //ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "alert", "alert('Remitter kyc detail updated successfully. !');", true);
            }
        }
    }
}