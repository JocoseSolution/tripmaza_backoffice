﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="NewExecutive.aspx.cs" Inherits="NewExecutive" %>




<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>
    <script src="../../chosen/chosen.jquery.js" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <script type="text/javascript">
        function checkit(evt) {
            evt = (evt) ? evt : window.event
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (!(charCode > 64 && charCode < 91 || charCode > 96 && charCode < 123 || (charCode == 8 || charCode == 45))) {
                return false;
            }
            status = "";
            return true;
        }

        function checkitt(evt) {
            evt = (evt) ? evt : window.event
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (!(charCode > 47 && charCode < 58)) {
                return false;
            }
            status = "";
            return true;
        }

        function getKeyCode(e) {
            if (window.event)
                return window.event.keyCode;
            else if (e)
                return e.which;
            else
                return null;
        }
        function keyRestrict(e, validchars) {
            var key = '', keychar = '';
            key = getKeyCode(e);
            if (key == null) return true;
            keychar = String.fromCharCode(key);
            keychar = keychar.toLowerCase();
            validchars = validchars.toLowerCase();
            if (validchars.indexOf(keychar) != -1)
                return true;
            if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                return true;
            return false;
        }
    </script>




    <script type="text/javascript">
        $(document).ready(function () {


            $('#<%=Submit.ClientID%>').click(function (event) {

                var returntypp = true;
                if ($.trim($("#<%=txtemail.ClientID%>").val()) == "") {

                    $("#<%=txtemail.ClientID%>").focus();
                    $('.error').show();
                    returntypp = false;
                }
                else {
                    $('.error').hide();
                }
                if ($.trim($("#<%=txtpassword.ClientID%>").val()) == "") {

                    $("#<%=txtpassword.ClientID%>").focus();
                    $('.error1').show();
                    returntypp = false;
                }
                else {
                    $('.error1').hide();
                }
                if ($.trim($("#<%=txtname.ClientID%>").val()) == "") {

                    $("#<%=txtname.ClientID%>").focus();
                    $('.error2').show();
                    returntypp = false;
                }
                else {
                    $('.error2').hide();
                }

                if ($.trim($("#<%=txtmobile.ClientID%>").val()) == "") {

                    $("#<%=txtmobile.ClientID%>").focus();
                    $('.error3').show();
                    returntypp = false;
                }
                else {
                    $('.error3').hide();
                }

                if ($.trim($("#<%=ddlRole_type.ClientID%>").val()) == "0") {

                    $("#<%=ddlRole_type.ClientID%>").focus();
                    $('.error4').show();
                    returntypp = false;
                }
                else {
                    $('.error4').hide();
                }
                return returntypp;
            });

        });
        //function confirmUpdate(thisObj) {



        //    if ($.trim($("#ctl00_ContentPlaceHolder1_GridView1_ctl02_lbtnEdit").val()) == "") {

        //        $("#ctl00_ContentPlaceHolder1_GridView1_ctl02_lbtnEdit").focus();
        //        return false;
        //    }
        //    else {
        //        alert("hi");
        //        var upd = confirm('Are you sure to update this configuration');
        //        if (upd == true) {
        //            return true;
        //        }
        //        else {
        //            return false;
        //        }
        //    }
        //}
        function confirmDelete() {
            var upd = confirm('Are you sure to delete this configuration');
            if (upd == true) {
                return true;
            }
            else {
                return false;
            }
        }

    </script>



    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {

        padding: 4px 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid #ddd;
        width: auto !important;
        max-width: none;
        white-space: nowrap;
        overflow: hidden !important;
        text-overflow: ellipsis;

}
        .panel-primary > .panel-heading {
            width: 97.5%;
            color: #fff;
            margin-left: 13px;
            background-color: #f3f6ff;
            border: 1px solid #000;
        }

        .panel {
            border: 1px solid #fefefe;
        }

        h3 {
            color: #032451 !important;
        }

        .btnuerr {
            height: 24px;
            font-size: 11px;
            margin: 0px;
            padding: 2px 7px;
        }

        .btnsearchuerr {
            height: 24px;
            font-size: 11px;
            margin: 0px;
            padding: 2px 7px;
            background: #e46517 !important;
        }

        .btnuer {
            height: 24px;
            font-size: 11px;
            margin: 0px;
            padding: 2px 7px;
        }

        .btnsearchuer {
            height: 24px;
            font-size: 11px;
            margin: 0px;
            padding: 2px 7px;
            background: #e46517 !important;
        }

        a:focus, a:hover {
            color: #fff;
            text-decoration: underline;
        }
    </style>
    <div class="row">
        <div class="col-md-12" style="margin-top: 20px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-sm-6">
                                <h3 class="panel-title">PrivilegePanel > manage-user</h3>
                            </div>
                            <div class="col-sm-6">
                                <span class="button buttonBlue pull-right btnuer "><span class="fa fa-plus-circle"></span>Add User</span>
                                <span class="button buttonBlue pull-right btnsearchuer hidden "><span class="fa fa-search"></span>Search User</span>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12 form-group  adduser hidden" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:TextBox runat="server" placeholder="Enter Email.." CssClass="theme-search-area-section-input" ID="txtemail" MaxLength="50"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RFVMK" runat="server" ControlToValidate="txtemail" ErrorMessage="*"
                                                Display="dynamic" ValidationGroup="group1"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtemail" ValidationGroup="group1" ErrorMessage=" enter valid email "
                                                ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                            </asp:RegularExpressionValidator>

                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:TextBox placeholder="Enter Passsword.." CssClass="theme-search-area-section-input" runat="server" ID="txtpassword" MaxLength="40" TextMode="Password"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RFVMK1" runat="server" ControlToValidate="txtpassword" ErrorMessage="*"
                                                Display="dynamic" ValidationGroup="group1"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="Regex2" runat="server" ControlToValidate="txtpassword" ValidationGroup="group1" Display="dynamic" ValidationExpression="^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{5,}$" ErrorMessage="Minimum 5 characters atleast 1 Alphabet, 1 Number and 1 Special Character" ForeColor="Red" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:TextBox placeholder="Enter Name.." CssClass="theme-search-area-section-input" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwxyz');" runat="server" ID="txtname" MaxLength="20"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RFVMK2" runat="server" ControlToValidate="txtname" ErrorMessage="*"
                                                Display="dynamic" ValidationGroup="group1"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtname" ID="RegularExpressionValidator1" ValidationExpression="^[\s\S]{5,30}$" runat="server" ErrorMessage="Minimum 5 and Maximum 20 characters required."></asp:RegularExpressionValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtname" ValidationGroup="group1"
                                                ValidationExpression="[a-zA-Z ]*$" ErrorMessage="*Valid characters: Alphabets and space." />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:TextBox placeholder="Enter Mobile no.." CssClass="theme-search-area-section-input" onkeypress="return checkitt(event)" runat="server" ID="txtmobile" MaxLength="10"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtmobile" ErrorMessage="*"
                                                Display="dynamic" ValidationGroup="group1"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationGroup="group1"
                                                ControlToValidate="txtmobile" ErrorMessage="Please Fill valid 10 digit mobile no."
                                                ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:DropDownList CssClass="theme-search-area-section-input" ID="ddlRole_type" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:DropDownList CssClass="theme-search-area-section-input"  ID="DD_Branch" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <asp:Button ID="Submit" runat="server" Text="Submit" CssClass="button buttonBlue" OnClick="Submit_Click" ValidationGroup="group1" />
                                        <label for="exampleInputPassword1" id="Label1" runat="server" style="color: red;"></label>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-12 form-group searchuser" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="col-md-3">
                                <div class="theme-search-area-section theme-search-area-section-line">
                                    <div class="theme-search-area-section-inner">
                                        <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                        <asp:DropDownList CssClass="theme-search-area-section-input" ID="ddluser" runat="server">
                                            <asp:ListItem Text="Select Role" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="theme-search-area-section theme-search-area-section-line">
                                    <div class="theme-search-area-section-inner">
                                        <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                        <asp:TextBox ID="txt_Email" runat="server" placeholder="Enter Email.." CssClass="theme-search-area-section-input"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="theme-search-area-section theme-search-area-section-line">
                                    <div class="theme-search-area-section-inner">
                                        <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                        <asp:DropDownList CssClass="theme-search-area-section-input" ID="ddluserStatus" runat="server">
                                            <asp:ListItem Text="Select Status" Value=""></asp:ListItem>
                                            <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="InActive" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <br />
                                    <asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="button buttonBlue" OnClick="BtnSearch_Click" OnClientClick="return Validate();" />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" DataKeyNames="user_id"
                                OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
                                OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting" PageSize="8"
                                CssClass="table" GridLines="None" Width="100%">

                                <Columns>
                                    <asp:TemplateField HeaderText="Counter" ItemStyle-Width="150">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcounter" runat="server" Text='<%# Eval("counter") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User_id" ItemStyle-Width="150">
                                        <ItemTemplate>
                                            <asp:Label ID="lbluserid" runat="server" Text='<%# Eval("user_id") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Password" ItemStyle-Width="150">
                                        <ItemTemplate>
                                            <asp:Label ID="lblpwd" runat="server" Text='<%# Eval("password") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtExecPwd" runat="server" CssClass="form-control" Text='<%#Eval("password") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Role_Name" ItemStyle-Width="150">
                                        <ItemTemplate>
                                            <asp:Label ID="lblroleid" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="ddlRole_Name" CssClass="form-control" runat="server" Width="150px"></asp:DropDownList>
                                            <asp:Label ID="lblRoleNameHidden" runat="server" Text='<%# Eval("Role") %>' Visible="false"></asp:Label>
                                        </EditItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Role_Type" ItemStyle-Width="150">
                                        <ItemTemplate>
                                            <asp:Label ID="lblroletype" runat="server" Text='<%# Eval("role_type") %>'></asp:Label>
                                        </ItemTemplate>
                                        <%--<EditItemTemplate>
                                        <asp:DropDownList ID="ddlRole_Type" runat="server" Width="150px"></asp:DropDownList>
                                        <asp:Label ID="lblRoleTypeHidden" runat="server" Text='<%# Eval("role_type") %>' Visible="false"></asp:Label>
                                    </EditItemTemplate>--%>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name" ItemStyle-Width="150">
                                        <ItemTemplate>
                                            <asp:Label ID="lblname" runat="server" Text='<%# Eval("name") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email" ItemStyle-Width="150">
                                        <ItemTemplate>
                                            <asp:Label ID="lblemailid" runat="server" Text='<%# Eval("Email_id") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mobile_no" ItemStyle-Width="150">
                                        <ItemTemplate>
                                            <asp:Label ID="lblmobileno" runat="server" Text='<%# Eval("mobile_no") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" ItemStyle-Width="150">
                                        <ItemTemplate>
                                            <asp:Label ID="lblstatus" runat="server" Text='<%# GetStatusVal( Eval("status")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>

                                            <asp:DropDownList ID="ddlstatus" CssClass="form-control" runat="server" SelectedValue='<%# Eval("status").ToString() %>'>
                                                <asp:ListItem Value="True" Text="Active"></asp:ListItem>
                                                <asp:ListItem Value="False" Text="Inactive"></asp:ListItem>
                                            </asp:DropDownList>

                                        </EditItemTemplate>

                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Edit/Delete" ItemStyle-CssClass="nowrapgrdview">
                                        <EditItemTemplate>
                                            <asp:LinkButton ID="lbtnUpdate" runat="server" CssClass="button buttonBlue btnuerr" CommandName="Update" Text="Update"></asp:LinkButton>
                                            <asp:LinkButton ID="lbtnCancel" runat="server" CssClass="button buttonBlue btnsearchuerr" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnEdit" runat="server" CssClass="button buttonBlue btnuerr" CommandName="Edit" Text="Edit"></asp:LinkButton>/
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CssClass="button buttonBlue btnsearchuerr" CommandName="Delete" Text="Delete" CommandArgument='<%#Eval("user_id")%>' OnClientClick="return confirmDelete();"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                </Columns>

                            </asp:GridView>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script>
    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/search4.js") %>"></script>     --%>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript">
        $('.btnuer').click(function () {
            $(".adduser").removeClass('hidden');
            $(".btnuer").addClass('hidden');
            $(".searchuser").addClass('hidden');
            $(".btnsearchuer").removeClass('hidden');
        });
        $('.btnsearchuer').click(function () {
            $(".adduser").addClass('hidden');
            $(".btnsearchuer").addClass('hidden');
            $(".btnuer").removeClass('hidden');
            $(".searchuser").removeClass('hidden');
        });
    </script>
</asp:Content>

