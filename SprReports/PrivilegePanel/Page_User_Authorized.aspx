﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="Page_User_Authorized.aspx.cs" Inherits="SprReports_PrivilegePanel_Page_User_Authorized" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .panel-primary > .panel-heading {
            width: 97.5%;
            color: #fff;
            margin-left: 13px;
            background-color: #f3f6ff;
            border: 1px solid #000;
        }

        .panel {
            border: 1px solid #fefefe;
        }

        h3 {
            color: #032451 !important;
        }
    </style>
    <div class="row">
        <div class="col-md-12" style="margin-top: 20px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">PrivilegePanel > Authorization Page </h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Role Name:</label>
                                        <asp:DropDownList CssClass="form-control" ID="Role1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="Role_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Role1" ErrorMessage="*"
                                            Display="dynamic"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Page Name:</label>
                                        <asp:DropDownList CssClass="form-control" ID="Page_name" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <asp:Button ID="Button1" runat="server" Text="Submit" CssClass="button buttonBlue" OnClick="Submit_Click" />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1" id="Label1" runat="server"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <asp:UpdatePanel runat="server" ID="panelUserBypage" UpdateMode="Always" ChildrenAsTriggers="true">
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="GridView1" />
                                </Triggers>
                                <ContentTemplate>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False"
                                                PageSize="8" CssClass="table table-bordered table-striped" GridLines="None" Width="100%" OnRowCommand="GridView1_RowCommand" OnRowDeleting="GridView1_RowDeleting">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Role" ItemStyle-Width="150">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Page_Name" ItemStyle-Width="150">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPage_id" runat="server" Text='<%# Eval("Page_name") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Delete" ItemStyle-CssClass="nowrapgrdview">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="Delete" Text="Delete" CommandArgument='<%#Eval("Role")+"~"+Eval("Page_name") %>' OnClientClick="return confirmDelete();"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                            <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="panelUserBypage" OnDataBinding="Role_SelectedIndexChanged">
                                <ProgressTemplate>
                                    <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                    </div>
                                    <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                        Please Wait....<br />
                                        <br />
                                        <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                        <br />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <%-- <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-10">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">PrivilegePanel > Authorization Page </h3>
                    </div>
                    <div class="panel-body">
                        <asp:UpdatePanel runat="server" ID="panelUserBypage" UpdateMode="Always" ChildrenAsTriggers="true">
                            <Triggers>
                                <asp:PostBackTrigger ControlID="GridView1" />
                                <asp:AsyncPostBackTrigger ControlID="Role1" />
                            </Triggers>
                            <ContentTemplate>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Role Name:</label>
                                            <asp:DropDownList CssClass="form-control" ID="Role1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="Role_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Role1" ErrorMessage="*"
                                                Display="dynamic"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Page Name:</label>
                                            <asp:DropDownList CssClass="form-control" ID="Page_name" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <asp:Button ID="Button1" runat="server" Text="Submit" CssClass="button buttonBlue" OnClick="Submit_Click" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1" id="Label1" runat="server"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False"
                                            PageSize="8" CssClass="table table-bordered table-striped" GridLines="None" Width="100%" OnRowCommand="GridView1_RowCommand" OnRowDeleting="GridView1_RowDeleting">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Role" ItemStyle-Width="150">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Page_Name" ItemStyle-Width="150">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPage_id" runat="server" Text='<%# Eval("Page_name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete" ItemStyle-CssClass="nowrapgrdview">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="Delete" Text="Delete" CommandArgument='<%#Eval("Role")+"~"+Eval("Page_name") %>' OnClientClick="return confirmDelete();"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        </div>
                                    </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>                

                        <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="panelUserBypage" OnDataBinding="Role_SelectedIndexChanged">
                             <ProgressTemplate>
                                 <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                 </div>
                                 <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                     Please Wait....<br />
                                     <br />
                                     <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                     <br />
                                 </div>
                             </ProgressTemplate>
                         </asp:UpdateProgress>


                    </div>
                </div>
            </div>
        </div>
    </div>--%>



    <script src="<%=ResolveUrl("~/Js/jquery-1.7.1.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Js/jquery-ui-1.8.8.custom.min.js") %>" type="text/javascript"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>


    <script type="text/javascript">
        $(document).ready(function () {

            $('#<%=Button1.ClientID%>').click(function (event) {

                if ($.trim($("#ctl00_ContentPlaceHolder1_Role1").val()) == "0") {
                    alert("Role should be selected");
                    $("#<%=Role1.ClientID%>").focus();
                    return false;
                }

                if ($.trim($("#ctl00_ContentPlaceHolder1_Page_name").val()) == "0") {
                    alert("Page Name should be selected");
                    $("#<%=Page_name.ClientID%>").focus();
                    return false;
                }
            });
        });
        function confirmDelete() {
            var upd = confirm('Parent Page will also delete for this page!');
            if (upd == true) {
                return true;
            }
            else {
                return false;
            }
        }

    </script>


    <script type="text/javascript">
        $(document).ready(function () {
            $("#ctl00_ContentPlaceHolder1_Role1").click(function () {

                $("#ctl00_ContentPlaceHolder1_Label1").hide();


            });
            $("#ctl00_ContentPlaceHolder1_Page_name").click(function () {

                $("#ctl00_ContentPlaceHolder1_Label1").hide();
            });
        });
    </script>
</asp:Content>

