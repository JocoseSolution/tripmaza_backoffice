﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="Role.aspx.cs" Inherits="Login" %>

<%@ Register Src="~/UserControl/PageAllow1.ascx" TagPrefix="uc1" TagName="PageAllow1" %>






<%--


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

   
 <style>
      .error
      {
      color:red;
      padding-left:10px;
      display:none;
      }
 </style>
 <script>
        $(document).ready(function () {

            $('#ContentPlaceHolder1_Submit').click(function (event) {

                var data = $("#ContentPlaceHolder1_Roletxt").val();


                var length = data.length;

                if (length < 1) {

                    $('.error').show();
                    return false;
                }
                else {
                    $('.error').hide();
                }


            });
        });
</script>

    <div style="margin-top:-220px;margin-left:750px">


        <uc1:PageAllow1 runat="server" ID="PageAllow1" />

       
    </div>


   <div>
        <table style="border-style: solid; border-color: inherit; border-width: medium; width:399px; height:146px; padding-right:50px; " >
            <tr>
                <td>Role:</td>
                <td>
                    <input type="text" name="Role" id ="Roletxt" runat="server" /><span class ="error">***</span></td>
            </tr>

             <tr>
                <td>Role_Type</td>
                <td>
                    <asp:DropDownList ID="RoleType" runat="server" Height="29px" Width="139px">
                      
                         <asp:ListItem Text="Executive" Value="EXEC"></asp:ListItem>
                         <asp:ListItem Text="Agent" Value="AGENT"></asp:ListItem>
                         <asp:ListItem Text="Admin" Value="ADMIN"></asp:ListItem>
                         <asp:ListItem Text="Sales" Value="SALES"></asp:ListItem>
                         <asp:ListItem Text="Account" Value="ACC"></asp:ListItem>                
                    </asp:DropDownList>
                    </td>
            </tr>
            <tr>
                <td>
                      <asp:Button runat="server" Text="Submit" ID="Submit" OnClick="Submit_Click"/>
                </td>
            </tr>

        </table>
    </div>

    <br/> <br/>
    <asp:Label ID="Label1" runat="server" Text=""></asp:Label><br/>
   
  <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" DataKeyNames="Role_id"
 OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting"  >
     
   
          <Columns> 
               <asp:TemplateField HeaderText="Role" ItemStyle-Width="150">
        <ItemTemplate>
            <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>'></asp:Label> 
        </ItemTemplate> 
           <EditItemTemplate>
          <asp:TextBox ID="txtRole" runat="server" Text='<%# Eval("Role") %>'></asp:TextBox>
        </EditItemTemplate> 
          </asp:TemplateField>

          <asp:TemplateField HeaderText="Role_Type" ItemStyle-Width="150">
            <ItemTemplate>
            <asp:Label ID="lblRole_type" runat="server" Text='<%# Eval("Role_Type") %>'></asp:Label> 
             </ItemTemplate> 
            </asp:TemplateField>

           
       
         
   
    <asp:CommandField ButtonType="Link" ShowEditButton="true" ShowDeleteButton="true" ItemStyle-Width="150"/>
</Columns>

        <HeaderStyle BackColor="Gray" ForeColor="#ffffff"/>  
            <RowStyle BackColor="#e7ceb6"/>  
             </asp:GridView>
--%>





<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .panel-primary > .panel-heading {
            width: 97.5%;
            color: #fff;
            margin-left: 13px;
            background-color: #f3f6ff;
            border: 1px solid #000;
        }

        .panel {
            border: 1px solid #fefefe;
        }

        h3 {
            color: #032451 !important;
        }
    </style>

    <div class="row">
        <div class="col-md-12" style="margin-top: 20px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">PrivilegePanel > Role Page</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Role :</label>
                                        <asp:TextBox CssClass="form-control" runat="server" ID="Roletxt" name="Role" MaxLength="30"></asp:TextBox>
                                        <%--   <asp:RequiredFieldValidator ID="RFVMK" runat="server" ControlToValidate="Roletxt" ErrorMessage="*"
                                    Display="dynamic" ValidationGroup="group2"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Role_Type :</label>
                                        <asp:DropDownList CssClass="form-control" ID="RoleType" runat="server">
                                            <asp:ListItem Text="--select Role---" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Executive" Value="EXEC"></asp:ListItem>
                                            <asp:ListItem Text="Agent" Value="AGENT"></asp:ListItem>
                                            <asp:ListItem Text="Admin" Value="ADMIN"></asp:ListItem>
                                            <asp:ListItem Text="Sales" Value="SALES"></asp:ListItem>
                                            <asp:ListItem Text="Account" Value="ACC"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">

                                        <asp:Button ID="Submit" runat="server" Text="Submit" CssClass="button buttonBlue" OnClick="Submit_Click" ValidationGroup="group2" />
                                    </div>
                                </div>

                            </div>



                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1" id="Label1" runat="server"></label>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="col-md-12">
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>

                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Role_id"
                                            OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
                                            OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting" PageSize="8"
                                            CssClass="table" GridLines="None" Width="100%">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Role" ItemStyle-Width="150">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtRole" runat="server" MaxLength="30" Text='<%# Eval("Role") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Role_Type" ItemStyle-Width="150">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRole_type" runat="server" Text='<%# Eval("Role_Type") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--  <asp:CommandField ButtonType="Link" ShowEditButton="true" ShowDeleteButton="true"  ItemStyle-Width="150" />--%>

                                                <asp:TemplateField HeaderText="Edit/Delete" ItemStyle-CssClass="nowrapgrdview">
                                                    <EditItemTemplate>
                                                        <asp:LinkButton ID="lbtnUpdate" runat="server" CommandName="Update" Text="Update" CommandArgument='<%#Eval("Role_id")%>' OnClientClick="return confirmUpdate(this.id);"></asp:LinkButton>
                                                        <asp:LinkButton ID="lbtnCancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>/
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="Delete" Text="Delete" CommandArgument='<%#Eval("Role_id")%>' OnClientClick="return confirmDelete();"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <%--   <script type="text/javascript">

        $(document).ready(function () {

            $('#<%=Submit.ClientID%>').click(function (event) {

                  if ($.trim($("#<%=RoleType.ClientID%>").val()) == "0") {
                      alert("value should be selected");
                      $("#<%=RoleType.ClientID%>").focus();
                    return false;
                }


              });
          });
    </script>--%>


    <script type="text/javascript">
        $(document).ready(function () {

            $('#ContentPlaceHolder1_Submit').click(function (event) {

                var data = $("#ContentPlaceHolder1_Roletxt").val();


                var length = data.length;

                if (length < 1) {

                    $('.error').show();
                    return false;
                }
                else {
                    $('.error').hide();
                }


            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#ctl00_ContentPlaceHolder1_Roletxt").click(function () {

                $("#ctl00_ContentPlaceHolder1_Label1").hide();


            });
            $("#ctl00_ContentPlaceHolder1_RoleType").click(function () {

                $("#ctl00_ContentPlaceHolder1_Label1").hide();
            });
        });
    </script>


    <script type="text/javascript">

        $(document).ready(function () {

            $('#<%=Submit.ClientID%>').click(function (event) {

                if ($.trim($("#ctl00_ContentPlaceHolder1_Roletxt").val()) == "") {
                    alert("Please select Role ");
                    $("#<%=RoleType.ClientID%>").focus();
                    return false;
                }

                if ($.trim($("#<%=RoleType.ClientID%>").val()) == "0") {
                    alert("Please select Role Type");
                    $("#<%=RoleType.ClientID%>").focus();
                    return false;
                }


            });
        });


        //function confirmUpdate(thisObj) {

        //    if ($.trim($("#ctl00_ContentPlaceHolder1_GridView1_ctl06_txtRole").val()) == "") {

        //        $("#ctl00_ContentPlaceHolder1_GridView1_ctl06_txtRole").focus();
        //        return false;
        //    }
        //    else {
        //        var upd = confirm('Are you sure to update this configuration');
        //        if (upd == true) {
        //            return true;
        //        }
        //        else {
        //            return false;
        //        }
        //    }
        //}

        function confirmUpdate(aaa) {

            var txtid = aaa.replace("lbtnUpdate", "txtRole");

            if ($.trim($("#" + txtid).val()) == "") {

                $("#" + txtid).focus();
                return false;
            }
            else {
                var upd = confirm('Are you sure to update this configuration');
                if (upd == true) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }
        function confirmDelete() {
            var upd = confirm('Are you sure to delete this configuration');
            if (upd == true) {
                return true;
            }
            else {
                return false;
            }
        }

    </script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</asp:Content>

