﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="PageDetails.aspx.cs" Inherits="Page" %>






<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />


    <script src="<%=ResolveUrl("~/Js/jquery-ui-1.8.8.custom.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Js/jquery-1.7.1.min.js") %>" type="text/javascript"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            $("#ctl00_ContentPlaceHolder1_Pagetxt").click(function () {

                $("#ctl00_ContentPlaceHolder1_Label1").hide();


            });
            $("#ctl00_ContentPlaceHolder1_Pageurltxt").click(function () {

                $("#ctl00_ContentPlaceHolder1_Label1").hide();
            });
            $("#ctl00_ContentPlaceHolder1_Root_page_Name").click(function () {

                $("#ctl00_ContentPlaceHolder1_Label1").hide();
            });
            $("#ctl00$ContentPlaceHolder1$CheckBox1").click(function () {

                $("#ctl00_ContentPlaceHolder1_Label1").hide();
            });
        });
    </script>


    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .panel-primary > .panel-heading {
            width: 97.5%;
            color: #fff;
            margin-left: 13px;
            background-color: #f3f6ff;
            border: 1px solid #000;
        }

        .panel {
            border: 1px solid #fefefe;
        }

        h3 {
            color: #032451 !important;
        }
    </style>
    <div class="row">
        <div class="col-md-12" style="margin-top: 20px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">PrivilegePanel > Page Details</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="row">
                                <div class="col-md-4">

                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Page Name :</label>
                                        <asp:TextBox CssClass="form-control" runat="server" name="Page_Name" ID="Pagetxt"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RFVMK" runat="server" ControlToValidate="Pagetxt" ErrorMessage="*"
                                            Display="dynamic" ValidationGroup="group1"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Page URL :</label>
                                        <asp:TextBox CssClass="form-control" runat="server" name="Page_url" ID="Pageurltxt"></asp:TextBox>

                                    </div>
                                </div>




                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Root_Page_Name:</label>
                                        <asp:DropDownList CssClass="form-control" ID="Root_page_Name" runat="server">
                                        </asp:DropDownList>

                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Is Parent Page:</label>
                                        <asp:CheckBox ID="CheckBox1" runat="server" CssClass="form-control" />

                                    </div>

                                </div>


                                <div class="col-md-4">
                                    <br />
                                    <div class="form-group">

                                        <asp:Button ID="Submit" runat="server" Text="Submit" CssClass="button buttonBlue" OnClick="Submit_Click" ValidationGroup="group1" />

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1" id="Label1" runat="server"></label>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="row" id="divReport" style="background-color: #fff; overflow-y: scroll; overflow-x: scroll; max-height: 500px;" runat="server">
                                <div class="col-md-12">
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>

                                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" DataKeyNames="Page_id"
                                                OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting"
                                                PageSize="8" CssClass="table" GridLines="None" Width="100%">




                                                <Columns>
                                                    <asp:TemplateField HeaderText="Page_id" ItemStyle-Width="150">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPageid" runat="server" Text='<%# Eval("Page_id") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Page_Name" ItemStyle-Width="150">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPagename" runat="server" Text='<%# Eval("Page_name") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtPagename" runat="server" Text='<%# Eval("Page_name") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Page_url" ItemStyle-Width="150">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPageurl" runat="server" Text='<%# Eval("Page_url") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtPageurl" runat="server" Text='<%# Eval("Page_url") %>' TextMode="MultiLine"></asp:TextBox>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Root_Page_ID" ItemStyle-Width="150">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRootPage" runat="server" Text='<%# Eval("Root_page_id") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <%-- <EditItemTemplate>
                                                        <asp:TextBox ID="txtRootPage" runat="server" Text='<%# Eval("Root_page_id") %>'></asp:TextBox>
                                                    </EditItemTemplate>--%>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Is_Root_Page" ItemStyle-Width="150">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblIs_Root_page" runat="server" Text='<%# Eval("Is_Parent_Page") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Page_Order" ItemStyle-Width="150">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPageorder" runat="server" Text='<%# Eval("PageOrder") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtPageorder" runat="server" Text='<%# Eval("PageOrder") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <%-- <asp:CommandField ButtonType="Link" ShowEditButton="true" ShowDeleteButton="true" ItemStyle-Width="150" />--%>


                                                    <asp:TemplateField HeaderText="Edit/Delete" ItemStyle-CssClass="nowrapgrdview">
                                                        <EditItemTemplate>
                                                            <asp:LinkButton ID="lbtnUpdate" runat="server" CommandName="Update" Text="Update" CommandArgument='<%#Eval("Page_id")%>' OnClientClick="return confirmUpdate(this.id);"></asp:LinkButton>
                                                            <asp:LinkButton ID="lbtnCancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lbtnEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>/
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="Delete" Text="Delete" CommandArgument='<%#Eval("Page_id")%>' OnClientClick="return confirmDelete();"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>


                                            </asp:GridView>


                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {


            $('#<%=Submit.ClientID%>').click(function (event) {

                    var returntypp = true;
                    if ($.trim($("#<%=Pagetxt.ClientID%>").val()) == "") {

                        $("#<%=Pagetxt.ClientID%>").focus();
                        $('.error').show();
                        returntypp = false;
                    }
                    else {
                        $('.error').hide();
                    }
                    if ($.trim($("#<%=Pageurltxt.ClientID%>").val()) == "") {

                        $("#<%=Pageurltxt.ClientID%>").focus();
                        $('.error1').show();
                        returntypp = false;
                    }
                    else {
                        $('.error1').hide();
                    }
                <%-- if ($.trim($("#<%=Root_page_Name.ClientID%>").val()) == "0") {

                     $("#<%=Root_page_Name.ClientID%>").focus();
                          $('.error2').show();
                          returntypp = false;
                      }
                      else {
                          $('.error2').hide();
                      }--%>
                    return returntypp;
                });

            });



        function confirmUpdate(aaa) {

            var txtid = aaa.replace("lbtnUpdate", "txtPagename");

            if ($("#" + txtid).val() == "") {

                $("#" + txtid).focus();
                return false;
            }
            else {
                var upd = confirm('Are you sure to update this configuration');
                if (upd == true) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }
        function confirmDelete() {
            var upd = confirm('Are you sure to delete this configuration');
            if (upd == true) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>


    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</asp:Content>

