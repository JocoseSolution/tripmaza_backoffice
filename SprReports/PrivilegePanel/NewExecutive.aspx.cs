﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


public partial class NewExecutive : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        if (string.IsNullOrEmpty(Convert.ToString(Session["User_Type"])) || string.IsNullOrEmpty(Convert.ToString(Session["UID"])) || string.IsNullOrEmpty(Convert.ToString(Session["TypeID"])))
        {
            Response.Redirect("~/Login.aspx");
        }      
        if (!this.IsPostBack)
        {
            ddluser.Items.Clear();
            ListItem item = new ListItem("Select Role");
            ddluser.AppendDataBoundItems = true;
            ddluser.Items.Insert(0, item);
            ddluser.DataSource = GetRoleTypeExec();
                ddluser.DataTextField = "role_type";
                ddluser.DataValueField = "role_type";
                ddluser.DataBind();
               BindRole(); Branch();            
            Label1.InnerText = "";
        }
    }
    public void Branch()
    {
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);

        SqlCommand cmd = new SqlCommand("Sp_Branch_Opertion");

        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@Action", "select");
        cmd.Connection = con;
        con.Open();
        DD_Branch.DataSource = cmd.ExecuteReader();
        DD_Branch.DataTextField = "Branch";
        DD_Branch.DataValueField = "Branch";
        DD_Branch.DataBind();
        con.Close();
        DD_Branch.Items.Insert(0, new ListItem("--Select Branch--", "DELHI"));
    }
    public void BindRole()
    {
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);

        SqlCommand cmd = new SqlCommand("BindRoleNewExecu_PP");

        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Connection = con;
        con.Open();
        ddlRole_type.DataSource = cmd.ExecuteReader();
        ddlRole_type.DataTextField = "Role";
        ddlRole_type.DataValueField = "Role_id";
        ddlRole_type.DataBind();
        con.Close();
        ddlRole_type.Items.Insert(0, new ListItem("--Select Role--", "0"));
    }
    public string GetStatusVal(object val)
    {
        string value = "";
        bool result = Convert.ToBoolean(val);

        if (result == false)
        {
            value = "Inactive";
        }
        else
        {
            value = "Active";
        }
        return value;
    }
    protected void Submit_Click(object sender, EventArgs e)
    {

        Pro bo = new Pro();

        //int result = 0;
        string result = "";

        bo.user_id = txtemail.Text;
        bo.password = txtpassword.Text;
        bo.name = txtname.Text;
        bo.mobileno = txtmobile.Text;
        bo.email = txtemail.Text;
        bo.status = true;
        bo.Role_id = Convert.ToInt32(ddlRole_type.SelectedValue);
        //bo.Role_Type = ddlRole_type.SelectedItem.ToString();

        Dao d1 = new Dao();
        try
        {
            result = d1.insertdata(bo, DD_Branch.SelectedValue.Trim());
            if (result == "Insert")
            {
                BindGrid();
                Label1.InnerText = "Data submitted successfully.";
                txtemail.Text = "";
                txtpassword.Text = "";
                txtname.Text = "";
                txtmobile.Text = "";
            }
            else
            {
                BindGrid();
                Label1.InnerText = "Data Already Exist.";
                txtemail.Text = "";
                txtpassword.Text = "";
                txtname.Text = "";
                txtmobile.Text = "";
            }
        }

        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            bo = null;
            d1 = null;
        }

        ddlRole_type.SelectedIndex = 0;
    }
    protected void BindGridview()
    {
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);
        con.Open();

        SqlCommand cmd = new SqlCommand("SelectNewExecSp_PP");
        cmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter sda = new SqlDataAdapter();

        cmd.Connection = con;
        sda.SelectCommand = cmd;
        DataTable dt = new DataTable();

        sda.Fill(dt);
        con.Close();
        GridView1.DataSource = dt;

        GridView1.DataBind();

    }
    protected void OnRowCancelingEdit(object sender, EventArgs e)
    {
        GridView1.EditIndex = -1;
        this.BindGridview();

    }
    protected void OnRowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        this.BindGridview();


        DropDownList ddlRoleName = GridView1.Rows[e.NewEditIndex].FindControl("ddlRole_Name") as DropDownList;
        //DropDownList ddlRoleType = GridView1.Rows[e.NewEditIndex].FindControl("ddlRole_Type") as DropDownList;

        Label lblRoleName = GridView1.Rows[e.NewEditIndex].FindControl("lblRoleNameHidden") as Label;
       // Label lblRoleType = GridView1.Rows[e.NewEditIndex].FindControl("lblRoleTypeHidden") as Label;

        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);



        SqlDataAdapter adp = new SqlDataAdapter("BindRoleNewExecu_PP", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        DataTable dt = new DataTable();
        adp.Fill(dt);

        ddlRoleName.DataSource = dt;
        ddlRoleName.DataTextField = "Role";
        ddlRoleName.DataValueField = "Role_id";
        ddlRoleName.DataBind();
        //ddlRoleName.Items.FindByValue((e.Row.FindControl("lblRoleNameHidden") as Label).Text).Selected = true;
        ddlRoleName.Items.FindByText(lblRoleName.Text).Selected = true;


        //ddlRoleType.DataSource = dt;
        //ddlRoleType.DataTextField = "Role_Type";
        //ddlRoleType.DataValueField = "Role_id";
        //ddlRoleType.DataBind();
        //ddlRoleType.Items.FindByText(lblRoleType.Text).Selected = true;

        //ddlRoleType.Items.FindByValue((e.Row.FindControl("lblRoleTypeHidden") as Label).Text).Selected = true;

    }
    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string user_id = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("DeleteSp1_PP"))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@user_id", user_id);
                cmd.Connection = con;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
        this.BindGridview();

    }
    protected void OnRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridViewRow row = GridView1.Rows[e.RowIndex];
        string user_id = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
        string status = (row.FindControl("ddlstatus") as DropDownList).SelectedItem.ToString();

        TextBox txtPwd = (TextBox)(GridView1.Rows[e.RowIndex].FindControl("txtExecPwd"));
        string Pwd = Convert.ToString(txtPwd.Text);
        if (string.IsNullOrEmpty(Pwd))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter password!');", true);
            return;
        }

        string roleId = (row.FindControl("ddlRole_Name") as DropDownList).SelectedValue.ToString();

        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("NewExecuUpdateSp_PP"))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@user_id", user_id);
                cmd.Parameters.AddWithValue("@password", Pwd);
                cmd.Parameters.AddWithValue("@RoleID", roleId);
                cmd.Parameters.AddWithValue("@status", status.ToUpper() == "ACTIVE" ? 1 : 0);


                cmd.Connection = con;
                con.Open();
                int i = Convert.ToInt32(cmd.ExecuteNonQuery());
                con.Close();
                if(i > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Record updated successfully.');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Record not updated.');", true);
                }
                
            }
        }
        GridView1.EditIndex = -1;
        this.BindGridview();

    }
    protected void Cancel_Click(object sender, EventArgs e)
    {
        txtemail.Text = "";
        txtpassword.Text = "";
        txtname.Text = "";
        txtmobile.Text = "";
        ddlRole_type.SelectedIndex = 0;
    }
    public DataTable CheckUserDetails()
    {
        DataTable dt = new DataTable();
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB1"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);
        try
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("CheckUserDetails");
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sda = new SqlDataAdapter();
            cmd.Connection = con;
            sda.SelectCommand = cmd;
            sda.Fill(dt);
            con.Close();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            con.Close();
        }

        return dt;
    }
    public DataTable GetRoleTypeExec()
    {
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            SqlCommand cmd = new SqlCommand();          
            cmd.CommandText = "GetRoleType";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Connection = con;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            con.Close();
        }

        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            con.Close();
        }
        return dt;
    }
    protected void BtnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            BindGrid();
        }
        catch(Exception ex)
        {
            ex.ToString();
        }
    }
    public void BindGrid()
    {
        try
        {
            GridView1.DataSource = GetRecord();
            GridView1.DataBind();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    public DataTable GetRecord()
    {
        DataTable dt = new DataTable();
        try
        {          
            string Email = string.IsNullOrEmpty(txt_Email.Text) ? "" : txt_Email.Text.Trim();
            string RoleType = "";
            if ((ddluser.SelectedIndex > 0))
                RoleType = ddluser.SelectedValue;
            string Status = "";
            Status = ddluserStatus.SelectedValue;
            if (con.State == ConnectionState.Closed)
                con.Open();
            adap = new SqlDataAdapter("SelectNewExecSp_PP", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@RoleType", RoleType);
            adap.SelectCommand.Parameters.AddWithValue("@Email", Email);
            adap.SelectCommand.Parameters.AddWithValue("@Status", Status);
            //adap.SelectCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 100);
            //adap.SelectCommand.Parameters["@Msg"].Direction = ParameterDirection.Output;           
            adap.Fill(dt);
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            con.Close();
            adap.Dispose();
        }
        return dt;
    }
}