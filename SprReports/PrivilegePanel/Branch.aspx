﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="Branch.aspx.cs" Inherits="SprReports_PrivilegePanel_Branch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .panel-primary > .panel-heading {
            width: 97.5%;
            color: #fff;
            margin-left: 13px;
            background-color: #f3f6ff;
            border: 1px solid #000;
        }

        .panel {
            border: 1px solid #fefefe;
        }

        h3 {
            color: #032451 !important;
        }
    </style>

    <div class="row">
        <div class="col-md-12" style="margin-top: 20px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">PrivilegePanel > Branch Page</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Branch:</label>
                                        <asp:TextBox CssClass="form-control" runat="server" ID="Branchtxt" name="branch" MaxLength="30"></asp:TextBox>
                                        <%--   <asp:RequiredFieldValidator ID="RFVMK" runat="server" ControlToValidate="Roletxt" ErrorMessage="*"
                                    Display="dynamic" ValidationGroup="group2"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">

                                        <asp:Button ID="Submit" runat="server" Text="Submit" CssClass="button buttonBlue" OnClick="Submit_Click" ValidationGroup="group2" />
                                    </div>
                                </div>

                            </div>



                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1" id="Label1" runat="server"></label>

                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="col-md-12">
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>

                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Branch_id"
                                            OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
                                            OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting" PageSize="8"
                                            CssClass="table" GridLines="None" Width="100%">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Branch" ItemStyle-Width="150">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBranch" runat="server" Text='<%# Eval("Branch") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtBranch" runat="server" MaxLength="30" Text='<%# Eval("Branch") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>

                                                <%--  <asp:CommandField ButtonType="Link" ShowEditButton="true" ShowDeleteButton="true"  ItemStyle-Width="150" />--%>

                                                <asp:TemplateField HeaderText="Edit/Delete" ItemStyle-CssClass="nowrapgrdview">
                                                    <EditItemTemplate>
                                                        <asp:LinkButton ID="lbtnUpdate" runat="server" CommandName="Update" Text="Update" CommandArgument='<%#Eval("Branch_id")%>' OnClientClick="return confirmUpdate(this.id);"></asp:LinkButton>
                                                        <asp:LinkButton ID="lbtnCancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>/
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="Delete" Text="Delete" CommandArgument='<%#Eval("Branch_id")%>' OnClientClick="return confirmDelete();"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <%--   <script type="text/javascript">

        $(document).ready(function () {

            $('#<%=Submit.ClientID%>').click(function (event) {

                  if ($.trim($("#<%=RoleType.ClientID%>").val()) == "0") {
                      alert("value should be selected");
                      $("#<%=RoleType.ClientID%>").focus();
                    return false;
                }


              });
          });
    </script>--%>


    <script type="text/javascript">
        $(document).ready(function () {

            $('#ContentPlaceHolder1_Submit').click(function (event) {

                var data = $("#ContentPlaceHolder1_Branchtxt").val();


                var length = data.length;

                if (length < 1) {

                    $('.error').show();
                    return false;
                }
                else {
                    $('.error').hide();
                }


            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#ctl00_ContentPlaceHolder1_Branchtxt").click(function () {

                $("#ctl00_ContentPlaceHolder1_Label1").hide();


            });

        });
    </script>


    <script type="text/javascript">

        $(document).ready(function () {

            $('#<%=Submit.ClientID%>').click(function (event) {

                if ($.trim($("#ctl00_ContentPlaceHolder1_Branchtxt").val()) == "") {
                    alert("Please select Role ");
                    $("#<%=Branchtxt.ClientID%>").focus();
                    return false;
                }

            });
        });


        //function confirmUpdate(thisObj) {

        //    if ($.trim($("#ctl00_ContentPlaceHolder1_GridView1_ctl06_txtRole").val()) == "") {

        //        $("#ctl00_ContentPlaceHolder1_GridView1_ctl06_txtRole").focus();
        //        return false;
        //    }
        //    else {
        //        var upd = confirm('Are you sure to update this configuration');
        //        if (upd == true) {
        //            return true;
        //        }
        //        else {
        //            return false;
        //        }
        //    }
        //}

        function confirmUpdate(aaa) {

            var txtid = aaa.replace("lbtnUpdate", "txtBranch");

            if ($.trim($("#" + txtid).val()) == "") {

                $("#" + txtid).focus();
                return false;
            }
            else {
                var upd = confirm('Are you sure to update this configuration');
                if (upd == true) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }
        function confirmDelete() {
            var upd = confirm('Are you sure to delete this configuration');
            if (upd == true) {
                return true;
            }
            else {
                return false;
            }
        }

    </script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</asp:Content>

