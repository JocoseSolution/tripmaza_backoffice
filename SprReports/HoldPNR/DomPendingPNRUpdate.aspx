﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="DomPendingPNRUpdate.aspx.vb" Inherits="Reports_HoldPNR_DomPendingPNRUpdate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--  <link href="../../css/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />--%>
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow-y: scroll;
        }

        .panel-primary > .panel-heading {
            width: 97.5%;
            color: #fff;
            margin-left: 13px;
            background-color: #f3f6ff;
            border: 1px solid #000;
        }

        .panel {
            border: 1px solid #fefefe;
        }

        h3 {
            color: #032451 !important;
        }
    </style>
    <script language="javascript" type="text/javascript">

        function Validate() {
            if (document.getElementById("ctl00_ContentPlaceHolder1_txt_Reject").value == "") {

                alert('Please Fill Remark');
                document.getElementById("ctl00_ContentPlaceHolder1_txt_Reject").focus();
                return false;


            }
            if (confirm("Are you sure you want to Reject!"))
                return true;
            return false;
        }
    </script>


    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-12" style="margin-top: 20px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight > Pending PNR Process</h3>
                    </div>

                    <div class="panel-body">
                        <div id="td_Reject" runat="server" visible="false">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Reject Remark</label>
                                        <asp:TextBox ID="txt_Reject" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox><br />
                                        <br />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <asp:Button ID="btn_Comment" runat="server" Text="Comment" OnClientClick="return Validate();"
                                            CssClass="button buttonBlue" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <asp:Button ID="btn_Cancel" runat="server" Text="Cancel" CssClass="button buttonBlue" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="row" id="divReport" style="background-color: #fff; overflow-y: scroll; overflow-x: scroll; max-height: 500px;" runat="server" visible="True">
                                <div class="col-md-28">

                                    <asp:GridView ID="GridHoldPNRAccept" runat="server" AllowPaging="True" AllowSorting="True"
                                        AutoGenerateColumns="False" CssClass="table" GridLines="None" PageSize="10">
                                        <Columns>

                                            <asp:TemplateField HeaderText="CreatedDate/Time">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CreateDate" runat="server" Text='<%#Eval("CreateDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Booking">
                                                <ItemTemplate>
                                                    <a href='../Admin/Update_BookingOrder.aspx?OrderId=<%#Eval("OrderId")%> &TransID='
                                                        target="_blank"
                                                        style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">Booking&nbsp;Details
                                                    </a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="TX ID">

                                                <%--<a id="ancher" href='PnrSummaryIntl.aspx?OrderId=<%#Eval("OrderId")%>'
                                               rel="lyteframe" rev="width: 900px; height: 500px; overflow:hidden;" target="_blank"
                                                style="font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #800000;
                                                font-weight: bold;">--%>
                                                <%--  <asp:Label ID="lbl_OrderId" runat="server" Text='<%#Eval("OrderId") %>'></asp:Label>--%>
                                                <%-- (View)</a>--%>

                                                <ItemTemplate>
                                                    <a id="ancher" href='../PnrSummaryIntl.aspx?OrderId=<%#Eval("OrderId")%>' target="_blank"
                                                        style="font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #004b91; font-weight: bold;">
                                                        <asp:Label ID="lbl_OrderId" runat="server" Text='<%#Eval("OrderId") %>'></asp:Label>(View)</a>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="AgentId">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_AgentID" runat="server" Text='<%#Eval("AgentID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="AgencyName">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_AgencyName" runat="server" Text='<%#Eval("AgencyName")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="GdsPnr">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_PNR" runat="server" Text='<%#Eval("GdsPnr") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="AirlinePnr">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_AirlinePnr" runat="server" Text='<%#Eval("AirlinePnr")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sector">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Sector" runat="server" Text='<%#Eval("sector") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Pax Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_PgFName" runat="server" Text='<%#Eval("PgFName") %>'></asp:Label>
                                                    <asp:Label ID="lbl_PgLName" runat="server" Text='<%#Eval("PgLName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Carrier">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Carrier" runat="server" Text='<%#Eval("VC")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Status" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="TripType">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_TripType" runat="server" Text='<%#Eval("TripType") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Trip">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Trip" runat="server" Text='<%#Eval("Trip") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="BookingAmountGross">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_TotalBookingCost" runat="server" Text='<%#Eval("TotalBookingCost") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="BookingAmountNet">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_TotalAfterDis" runat="server" Text='<%#Eval("TotalAfterDis") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Branch">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Branch" runat="server" Text='<%#Eval("Branch")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="SalesExecID">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_SalesExecID" runat="server" Text='<%#Eval("SalesExecID")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SupplierID">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_SupplierID" runat="server" Text='<%#Eval("TicketID")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="TimeSincePending">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_TimeSincePending" runat="server" Text='<%#Eval("TimeSincePending")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="PgMobile">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_PgMobile" runat="server" Text='<%#Eval("PgMobile") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PgEmail">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_PgEmail" runat="server" Text='<%#Eval("PgEmail") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Partner Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_PartnerName" runat="server" Text='<%#Eval("PartnerName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Update">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="ITZ_Accept" runat="server" CommandArgument='<%#Eval("OrderId") %>'
                                                        Font-Bold="True" Font-Underline="False" OnClick="ITZ_Accept_Click">UpdatePNR</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Reject">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LB_Reject" runat="server" CommandName="Reject" CommandArgument='<%#Eval("OrderId") %>'
                                                        Font-Bold="True" Font-Underline="False" ForeColor="Red">Refund</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Payment Mode">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_PaymentMode" runat="server" Text='<%# If(Convert.ToString(Eval("PaymentMode")) = "PG", "PG", "Wallet")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="JourneyType">
                                                <ItemTemplate>
                                                    <asp:Label ID="Lbl_FareType" runat="server" Text='<%#Eval("FareType")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="OutBoundRefNo">
                                                <ItemTemplate>
                                                    <asp:Label ID="Lbl_OutBoundRefNo" runat="server" Text='<%#Eval("ReferenceNo")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Remarks">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_remarks" runat="server" Text='<%#Eval("Remark") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Convenience Fee">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_PGCharges" runat="server" Text='<%#Eval("PGCharges")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>

                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
