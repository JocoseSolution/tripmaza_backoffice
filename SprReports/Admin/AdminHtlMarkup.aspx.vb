﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class SprReports_Admin_AdminHtlMarkup
    Inherits System.Web.UI.Page
    Private ST As New HotelDAL.HotelDA()
    Private STDOm As New SqlTransactionDom
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("UID") Is Nothing Then
                Response.Redirect("~/Login.aspx", True)
            End If
            If Not Page.IsPostBack Then
                If Session("User_Type") = "ADMIN" Then
                    Bindgrid(ST.GetHtlMarkup(Session("UID").ToString, "", "", "", "", "", "", "", 0, "", "", "AdminMrkSearch"))
                    Dim msg As String = ""
                    ddl_GroupType.AppendDataBoundItems = True
                    ddl_GroupType.Items.Clear()
                    ddl_GroupType.Items.Insert(0, New ListItem("ALL", "ALL"))
                    ddl_GroupType.DataSource = GroupTypeMGMT("", "", "MultipleSelect", msg)
                    ddl_GroupType.DataTextField = "GroupType"
                    ddl_GroupType.DataValueField = "GroupType"
                    ddl_GroupType.DataBind()
                End If
            End If
            Dim dtmodule As New DataTable
            dtmodule = STDom.GetModuleAccessDetails(Session("UID"), MODULENAME.DISCOUNT_I.ToString()).Tables(0)
            If (dtmodule.Rows.Count > 0) Then
                For Each dr As DataRow In dtmodule.Rows
                    If (dr("MODULETYPE").ToString().ToUpper() = MODULETYPE.INSERT.ToString().ToUpper() AndAlso Convert.ToBoolean(dr("STATUS").ToString()) = True) Then
                        btn_Submit.Visible = False
                    End If
                    If (dr("MODULETYPE").ToString().ToUpper() = MODULETYPE.UPDATE.ToString().ToUpper() AndAlso Convert.ToBoolean(dr("STATUS").ToString()) = True) Then
                        GrdMarkup.Columns(7).Visible = False
                    End If
                    If (dr("MODULETYPE").ToString().ToUpper() = MODULETYPE.DELETE.ToString().ToUpper() AndAlso Convert.ToBoolean(dr("STATUS").ToString()) = True) Then
                        GrdMarkup.Columns(9).Visible = False
                    End If
                Next
            End If
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub

    Protected Sub btn_Search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Search.Click
        Try
            If Session("User_Type") = "ADMIN" Then
                BindGridMarkup()
            End If
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub

    Protected Sub GrdMarkup_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GrdMarkup.RowDeleting
        Try
            Dim Counter As Label = TryCast(DirectCast(GrdMarkup.Rows(e.RowIndex).FindControl("lblCounter"), Label), Label)
            Dim i As Integer = ST.InsHtlMarkup("", "", "", "", "", "", "", txtAmt.Text.Trim(), "", "", "DeleteAdminMrk", Convert.ToInt32(Counter.Text))
            If i > 0 Then
                Dim DS As New DataSet
                DS = Session("Grdds")
                For Each strow In DS.Tables(0).Rows
                    If strow.Item("MarkupID").ToString = Counter.Text Then
                        strow.Delete()
                        DS.AcceptChanges()
                        Exit For
                    End If
                Next
                Bindgrid(DS)
                ShowAlertMessage("Markup Deleted Successfully")
            Else
                ShowAlertMessage("Please Try Again")
            End If

        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub

    Protected Sub btn_Submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Submit.Click
        Try
            If Session("User_Type") = "ADMIN" Then
                Dim City As String = If([String].IsNullOrEmpty(Request("htlCity")) Or Request("htlCity") = "City" Or Request("City") = "ALL", "ALL", Request("htlCity"))
                Dim Country As String = If([String].IsNullOrEmpty(Request("TR_Country")) Or Request("TR_Country") = "Country" Or Request("TR_Country") = "ALL", "ALL", Request("TR_Country"))
                Dim Star As String = If(ddlStar.SelectedIndex > 0, ddlStar.SelectedValue, "ALL")
                Dim Supplier As String = If(ddlSupplier.SelectedIndex > 0, ddlSupplier.SelectedValue, "ALL")
                Dim GroupType As String = If(ddl_GroupType.SelectedIndex > 0, ddl_GroupType.SelectedValue, "ALL")

                Dim MrkupType As String = Request("mrktype")
                Dim AgentID As String = If([String].IsNullOrEmpty(Request("hidtxtAgencyName")) Or Request("hidtxtAgencyName") = "Agency Name or ID" Or Request("hidtxtAgencyName") = "ALL", "ALL", Request("hidtxtAgencyName"))
                Dim i As Integer = ST.InsHtlMarkup(Session("UID").ToString, AgentID, Country, City, "", MrkupType, Star, txtAmt.Text.Trim, Supplier, GroupType, "INSAdmin", 0)
                If i > 0 Then
                    Dim ds As New DataSet()
                    ds = Session("Grdds")
                    Dim addnewRow As DataRow = ds.Tables(0).NewRow
                    addnewRow("MarkupID") = i
                    addnewRow("AgentID") = AgentID
                    addnewRow("Country") = Country
                    addnewRow("City") = City
                    addnewRow("Star") = Star

                    addnewRow("MarkupType") = MrkupType
                    addnewRow("MarkupAmount") = txtAmt.Text.Trim()
                    ds.Tables(0).Rows.Add(addnewRow)
                    Bindgrid(ds)
                    ShowAlertMessage("Markup Inserted Successfully")
                    txtAmt.Text = ""

                    ddlStar.SelectedIndex = -1
                Else
                    ShowAlertMessage("Markup Already Exists")
                End If
            End If
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub


    Protected Sub GrdMarkup_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GrdMarkup.RowUpdating
        Try
            Dim markupAmt As TextBox = TryCast(DirectCast(GrdMarkup.Rows(e.RowIndex).FindControl("txtAmt"), TextBox), TextBox)
            Dim Counter As Label = TryCast(DirectCast(GrdMarkup.Rows(e.RowIndex).FindControl("lblCounter"), Label), Label)
            Dim i As Integer = ST.InsHtlMarkup("", "", "", "", "", "", "", markupAmt.Text.Trim(), "", "", "UpdateAdmin", Convert.ToInt32(Counter.Text))
            GrdMarkup.EditIndex = -1
            If i > 0 Then
                Dim DS As New DataSet
                DS = Session("Grdds")
                For Each strow In DS.Tables(0).Rows
                    If strow.Item("MarkupID").ToString = Counter.Text Then
                        strow.Item("markupAmount") = markupAmt.Text
                        DS.AcceptChanges()
                    End If
                Next
                Bindgrid(DS)
                ShowAlertMessage("Markup Updated Successfully")
                'Response.Redirect("~/SprReports/Admin/AdminHtlMarkup.aspx", False)
            Else
                ShowAlertMessage("This Record is Already In Database. Please Insert Another Record")
            End If
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try

    End Sub
    Public Sub BindGridMarkup()
        Try
            Dim HotelType As String = "", Star As String = ""

            If ddlStar.SelectedValue <> "--Select star--" Then
                Star = ddlStar.SelectedValue
            End If
            Dim AgentID As String = If([String].IsNullOrEmpty(Request("txtAgencyName")) Or Request("txtAgencyName") = "Agency Name or ID", "", Request("txtAgencyName"))
            Dim Country As String = If([String].IsNullOrEmpty(Request("TR_Country")) Or Request("TR_Country") = "Country" Or Request("TR_Country") = "ALL", "ALL", Request("TR_Country"))
            Dim City As String = If([String].IsNullOrEmpty(Request("htlCity")) Or Request("htlCity") = "City" Or Request("City") = "ALL", "ALL", Request("htlCity"))
            Dim Supplier As String = If(ddlSupplier.SelectedIndex > 0, ddlSupplier.SelectedValue, "ALL")
            Dim GroupType As String = If(ddl_GroupType.SelectedIndex > 0, ddl_GroupType.SelectedValue, "ALL")
            Bindgrid(ST.GetHtlMarkup(Session("UID").ToString, AgentID, Country, City, Star, HotelType, Request("mrktype"), "", 0, Supplier, GroupType, "AdminMrkSearch"))
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub
    Private Sub Bindgrid(ByVal ds As DataSet)
        Try
            Session("Grdds") = ds
            GrdMarkup.DataSource = ds
            GrdMarkup.DataBind()
            If ds.Tables(0).Rows.Count > 0 Then
                NoRecard.Visible = False
            Else
                NoRecard.Visible = True
                NoRecard.InnerText = "No record found"
            End If
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub

    Protected Sub GrdMarkup_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GrdMarkup.RowEditing
        GrdMarkup.EditIndex = e.NewEditIndex
        Bindgrid(Session("Grdds"))
    End Sub

    Protected Sub GrdMarkup_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GrdMarkup.RowCancelingEdit
        GrdMarkup.EditIndex = -1
        Bindgrid(Session("Grdds"))
    End Sub

    Protected Sub GrdMarkup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdMarkup.PageIndexChanging
        GrdMarkup.PageIndex = e.NewPageIndex
        Bindgrid(Session("Grdds"))
    End Sub
    Public Shared Sub ShowAlertMessage(ByVal [error] As String)
        Dim page As Page = TryCast(HttpContext.Current.Handler, Page)
        If page IsNot Nothing Then
            [error] = [error].Replace("'", "'")
            ScriptManager.RegisterStartupScript(page, page.[GetType](), "err_msg", "alert('" & [error] & "');", True)
        End If
    End Sub
    Public Function GroupTypeMGMT(ByVal type As String, ByVal desc As String, ByVal cmdType As String, ByRef msg As String) As DataTable
        Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
        Dim dt As New DataTable()
        Dim cmd As New SqlCommand()
        Try
            cmd.CommandText = "usp_agentTypeMGMT"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 200).Value = type
            cmd.Parameters.Add("@desc", SqlDbType.VarChar, 500).Value = desc
            cmd.Parameters.Add("@cmdType", SqlDbType.VarChar, 50).Value = cmdType
            cmd.Parameters.Add("@msg", SqlDbType.VarChar, 500)
            cmd.Parameters("@msg").Direction = ParameterDirection.Output
            con.Open()
            cmd.Connection = con
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            msg = cmd.Parameters("@msg").Value.ToString().Trim()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        Finally
            con.Close()
            cmd.Dispose()
        End Try
        Return dt
    End Function
End Class
