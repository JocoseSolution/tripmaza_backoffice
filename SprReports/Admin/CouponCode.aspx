﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="CouponCode.aspx.vb" Inherits="SprReports_Admin_CouponCode" %>

<%@ Register Src="~/UserControl/Settings.ascx" TagPrefix="uc1" TagName="Settings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />

    <script type='text/javascript'>
        $(document).ready(function () {
            var hh = this;
            this.Form = $(".STARTDATEG");
            this.To = $(".ENDDATEG");

            var fFromDate = hh.Form.val();
            var tToDate = hh.To.val();

            //var returnDate = h.hidtxtRetDate.val();
            //Date Picker Bind

            var dtPickerOptions = {
                numberOfMonths: 1, dateFormat: "dd-mm-yy", maxDate: "+1y", minDate: "-2y", showOtherMonths: true, selectOtherMonths: false
            };
            if (hh.Form.length != 0) {
                hh.Form.datepicker(dtPickerOptions).datepicker("option", { onSelect: hh.UpdateRoundTripMininumDate }).datepicker("setDate", fFromDate.substr(0, 10));
            }
            if (hh.To.length != 0) {
                hh.To.datepicker(dtPickerOptions).datepicker("option", { onSelect: hh.UpdateRoundTripMininumDate }).datepicker("setDate", tToDate.substr(0, 10));
            }
        });

    </script>

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .panel-primary > .panel-heading {
            width: 97.5%;
            color: #fff;
            margin-left: 13px;
            background-color: #f3f6ff;
            border: 1px solid #000;
        }

        .panel {
            border: 1px solid #fefefe;
        }

        h3 {
            color: #032451 !important;
        }
    </style>
    <div class="row">
        <div class="col-md-12" style="margin-top: 20px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight Setting > Coupon Code</h3>
                    </div>
                    <div class="panel-body">

                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Start Date</label>
                                        <div class="form-groups">
                                            <input type="text" name="From" id="From" placeholder="Start Date" style="width: 260px; height: 35px;" cssclass="form-control" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">End Date</label>
                                        <div class="form-groups">
                                            <input type="text" name="To" placeholder="End Date" id="To" cssclass="form-control" style="width: 260px; height: 35px;" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Markup Type</label>
                                        <asp:DropDownList ID="drpMarkupType" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0" Text="--Select Markup Type--" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="P" Text="PERCENTAGE"></asp:ListItem>
                                            <asp:ListItem Value="F" Text="FIXED"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Trip</label>
                                        <asp:DropDownList ID="ddl_TripType" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="D">Domestic</asp:ListItem>
                                            <asp:ListItem Value="I">International</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Airline</label>
                                        <%--<asp:DropDownList ID="ddl_airline" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="ALL">--ALL--</asp:ListItem>
                                        <asp:ListItem Value="AI">Air India</asp:ListItem>
                                        <asp:ListItem Value="9W">Jet Airways</asp:ListItem>
                                        <asp:ListItem Value="UK">Vistara</asp:ListItem>
                                        <asp:ListItem Value="6E">Indigo</asp:ListItem>
                                        <asp:ListItem Value="G8">Goair</asp:ListItem>
                                        <asp:ListItem Value="SG">Spicejet</asp:ListItem>
                                    </asp:DropDownList>--%>

                                        <asp:DropDownList ID="ddl_airline" CssClass="form-control" runat="server" data-placeholder="Choose a Airline...">
                                        </asp:DropDownList>
                                    </div>
                                </div>





                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">PaxType</label>
                                        <asp:DropDownList ID="DdlPaxWise" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="ALL" Text="ALL" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="" Text="PAXWISE"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Agent Id</label>
                                        <input type="text" id="txtAgencyName" class="form-control" name="txtAgencyName" onfocus="focusObjag(this);"
                                            onblur="blurObjag(this);" defvalue="ALL" autocomplete="off" value="ALL" />

                                        <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">CouponCode :</label>
                                        <asp:TextBox ID="txt_Code" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Amount :</label>
                                        <asp:TextBox ID="TXTAmount" CssClass="form-control" runat="server" onKeyPress="return keyRestrict(event,'.0123456789');"
                                            MaxLength="5" Text="0"></asp:TextBox>
                                    </div>
                                </div>

                            </div>



                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Origin</label>
                                        <asp:TextBox ID="TXTOrg" CssClass="form-control" runat="server" MaxLength="3" onfocus="focusObjag(this);"
                                            onblur="blurObjag(this);" defvalue="ALL" autocomplete="off" value="ALL"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Destination</label>
                                        <asp:TextBox ID="TXTDest" CssClass="form-control" runat="server" MaxLength="3" onfocus="focusObjag(this);"
                                            onblur="blurObjag(this);" defvalue="ALL" autocomplete="off" value="ALL"></asp:TextBox>

                                    </div>
                                </div>



                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">CouponOnApply</label>
                                        <asp:DropDownList ID="DD_FARE" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0" Text="Select" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="BASIC" Text="BASIC"></asp:ListItem>
                                            <asp:ListItem Value="YQ" Text="YQ"></asp:ListItem>
                                            <asp:ListItem Value="BASIC+YQ" Text="BASIC+YQ"></asp:ListItem>
                                            <asp:ListItem Value="TOTAL" Text="TOTAL"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>


                            <div class="row">


                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">TravelType</label>
                                        <asp:DropDownList ID="DD_TRAVEL" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0" Text="Select" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="O" Text="ONEWAYS"></asp:ListItem>
                                            <asp:ListItem Value="R" Text="BOTH"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">CouponCount :</label>
                                        <asp:TextBox ID="CouponCount" CssClass="form-control" runat="server" onKeyPress="return keyRestrict(event,'.0123456789');"
                                            MaxLength="5"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <br />
                                    <div class="form-group">
                                        <label for="exampleInputPassword1"></label>
                                        <asp:Button ID="save" runat="server" Text="Save" CssClass="button buttonBlue" OnClientClick=" return validate()"
                                            OnClick="SAVE_Click" />
                                        <asp:Button ID="btnreset" runat="server" OnClick="btnreset_Click" Text="RESET" Visible="false" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="row" style="background-color: #fff; overflow: auto;">
                                <div class="large-10 medium-12 small-12 large-push-1">

                                    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>--%>
                                    <asp:GridView ID="grdemp" runat="server" AutoGenerateColumns="False" CssClass="table" GridLines="None" Width="100%"
                                        OnRowEditing="grdemp_RowEditing" OnRowUpdating="grdemp_RowUpdating" OnRowCommand="grdemp_RowCommand" OnRowDataBound="grdemp_RowDataBound1"
                                        OnRowCancelingEdit="grdemp_RowCancelingEdit" OnRowDeleting="grdemp_RowDeleting">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbtnCounter" runat="server" Text='<%#Bind("Counter")%>' CssClass="hide"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="StartDate">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbst" runat="server" Text='<%#Bind("StartDateN")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="EndDate">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbed" runat="server" Text='<%#Bind("EndDateN")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>



                                            <asp:TemplateField HeaderText="Trip">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbtnTrip" runat="server" Text='<%#Bind("Trip")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Airline">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbtnAirline" runat="server" Text='<%#Bind("Airline")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PaxType">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbFareType" runat="server" Text='<%#Bind("CouponOnPaxWise")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CouponOn">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbCouponOn" runat="server" Text='<%#Bind("CouponOn")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>



                                            <asp:TemplateField HeaderText="CouponCode">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbtnCouponCode" runat="server" Text='<%#Bind("CouponCode")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="AgentId">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbtnAgentId" runat="server" Text='<%#Bind("AgentId")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Origin">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbtnOrg" runat="server" Text='<%#Bind("Org")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Destination">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbtdest" runat="server" Text='<%#Bind("Dest")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="MarkupType">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbMarkupType" runat="server" Text='<%#Bind("MarkupType")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>



                                            <asp:TemplateField HeaderText="TravelType">
                                                <EditItemTemplate>
                                                    <asp:Label ID="Elbl_lbTravelType" Visible="false" runat="server" Text='<%#Eval("TravelType")%>' />
                                                    <asp:DropDownList ID="GDD_TRAVEL" runat="server" CssClass="form-control">

                                                        <asp:ListItem Value="O" Text="ONEWAYS"></asp:ListItem>
                                                        <asp:ListItem Value="R" Text="BOTH"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbTravelType" runat="server" Text='<%#Eval("TravelType")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>



                                            <asp:TemplateField HeaderText="Amount">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbtnAmount" runat="server" Text='<%#Bind("Amount")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtAmount" onKeyPress="return isNumberKey(event)" runat="server"
                                                        Text='<%#Eval("Amount") %>' Width="90px" MaxLength="7"></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="CouponCount">
                                                <ItemTemplate>
                                                    <asp:Label ID="LBCouponCount" runat="server" Text='<%#Bind("CouponCount")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TXCouponCount" onKeyPress="return isNumberKey(event)" runat="server"
                                                        Text='<%#Eval("CouponCount")%>' Width="90px" MaxLength="7"></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>







                                            <asp:TemplateField HeaderText="CreatedDate">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbtncreatedDate" runat="server" Text='<%#Bind("createdDate")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="UpadtedDate">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbUpadtedDate" runat="server" Text='<%#Bind("UpdatedDate")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Edit" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Button ID="btnEdit" runat="server" CommandName="Edit" CssClass="button_edit" Text="Edit" />
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:Button ID="btnUpdate" runat="server" CommandName="Update" CssClass="button_update" Text="Update" />&nbsp;&nbsp
                                            <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" CssClass="button_cancel" Text="Cancel" />
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Delete">
                                                <ItemTemplate>
                                                    <asp:Button ID="btnDelete" runat="server" CommandName="Delete" CssClass="button_delete" Text="Delete"
                                                        OnClientClick="if(!confirm('Do you want to delete?')){ return false; };" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="RowStyle" />
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <PagerStyle CssClass="PagerStyle" />
                                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                                        <HeaderStyle CssClass="HeaderStyle" />
                                        <EditRowStyle CssClass="EditRowStyle" />
                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                    </asp:GridView>
                                    <%-- </ContentTemplate>
                                </asp:UpdatePanel>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function isNumberKey(event) {

            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode >= 48 && charCode <= 57 || charCode == 08 || charCode == 46) {
                return true;
            }
            else {
                alert("please enter 0 to 9 only ");
                return false;
            }
        }
        function isCharKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode >= 65 && charCode <= 122 || charCode == 32 || charCode == 08) {
                return true;
            }
            else {
                alert("please enter char type ");
                return false;
            }
        }
        function validate() {
            debugger;
            var Amount = document.getElementById('<%=TXTAmount.ClientID %>');

            if (document.getElementById("From").value == "") {
                alert('Please Select Start Date');
                document.getElementById("From").focus();
                return false;
            }

            if (document.getElementById("To").value == "") {
                alert('Please Select End Date');
                document.getElementById("To").focus();
                return false;
            }



            if ($('#ctl00_ContentPlaceHolder1_drpMarkupType').val() == "0") {
                alert("Select Markup Type.");
                $('#ctl00_ContentPlaceHolder1_drpMarkupType').focus()
                return false;
            }
            if ($('#ctl00_ContentPlaceHolder1_DD_FARE').val() == "0") {
                alert("Select Fare");
                $('#ctl00_ContentPlaceHolder1_DD_FARE').focus()
                return false;
            }
            if ($('#ctl00_ContentPlaceHolder1_DD_TRAVEL').val() == "0") {
                alert("Select TRAVEL TRIP");
                $('#ctl00_ContentPlaceHolder1_DD_TRAVEL').focus()
                return false;
            }




            if ($('#ctl00_ContentPlaceHolder1_txt_Code').val() == "") {
                alert("Enter Coupon Code");
                $('#ctl00_ContentPlaceHolder1_txt_Code').focus()
                return false;
            }
            if ($('#ctl00_ContentPlaceHolder1_TXTAmount').val() == "") {
                alert("Enter Coupon Amount");
                $('#ctl00_ContentPlaceHolder1_TXTAmount').focus()
                return false;
            }

            if ($('#ctl00_ContentPlaceHolder1_CouponCount').val() == "") {
                alert("Enter Coupon Count Value");
                $('#ctl00_ContentPlaceHolder1_CouponCount').focus()
                return false;
            }



            if (confirm("Are you sure??"))
                return true;
            return false;
        }
    </script>

    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

    <script type="text/javascript">
        function focusObjag(obj) { if (obj.value == "ALL") obj.value = ""; }
        function blurObjag(obj) { if (obj.value == "") obj.value = "ALL"; }

        function focusObjorigin(obj) { if (obj.value == "ALL") obj.value = ""; }
        function blurObjorigin(obj) { if (obj.value == "") obj.value = "ALL"; }

        function focusObjdest(obj) { if (obj.value == "ALL") obj.value = ""; }
        function blurObjdest(obj) { if (obj.value == "") obj.value = "ALL"; }

    </script>


</asp:Content>
