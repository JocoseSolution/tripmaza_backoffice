﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="TempCreditLimit.aspx.cs" Inherits="SprReports_Admin_TempCreditLimit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/CSS/PopupStyle.css?V=0")%>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/PopupScript.js?V=2")%>"></script>
    <script src="../../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>
    <script src="../../chosen/chosen.jquery.js" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    
    <script language="javascript" type="text/javascript">

        $(function () {
            $("#ToDate").datepicker(
                   {
                       numberOfMonths: 1,

                       autoSize: true, dateFormat: 'dd-mm-yy', closeText: 'X', duration: 'slow', gotoCurrent: true, changeMonth: false,
                       changeYear: false, hideIfNoPrevNext: false, maxDate: '1y', minDate: 0, navigationAsDateFormat: true, defaultDate: +0, showAnim: 'toggle', showOtherMonths: true,
                       selectOtherMonths: true, showoff: "button", buttonImageOnly: true
                   }
              )

        });
        function getKeyCode(e) {
            if (window.event)
                return window.event.keyCode;
            else if (e)
                return e.which;
            else
                return null;
        }
        function keyRestrict(e, validchars) {
            var key = '', keychar = '';
            key = getKeyCode(e);
            if (key == null) return true;
            keychar = String.fromCharCode(key);
            keychar = keychar.toLowerCase();
            validchars = validchars.toLowerCase();
            if (validchars.indexOf(keychar) != -1)
                return true;
            if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                return true;
            return false;
        }

        function ClearRec() {
            $("#ToDate").val("");
        }

        function Check() {
            debugger;
            if ($("#ctl00_ContentPlaceHolder1_hdnUserId").val() == "") {
                alert("Please again serach and get agent details");
                $("#txtAgencyName").focus();
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_hdnAgencyId").val() == "") {
                alert("Please again serach and get agent details");
                $("#txtAgencyName").focus();
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_TxtAgentCredit").val() == "") {
                alert("Enter agent credit limit.");
                $("#ctl00_ContentPlaceHolder1_TxtAgentCredit").focus();
                return false;
            }

            if ($("#ToDate").val() == "") {
                alert("Enter agent temp credit limit expiry date.");
                $("#ToDate").focus();
                return false;
            }

            //var CreditLimit = $('#ctl00_ContentPlaceHolder1_TxtAgentCredit').val();
            //var DueAmount = $('#ctl00_ContentPlaceHolder1_TxtDueAmount').val()

            ////if (parseFloat(CreditLimit) > parseFloat((DueAmount))) {
            //if (parseFloat(DueAmount) > parseFloat((CreditLimit))) {
            //    alert('Please set credit limit  more than due amount');
            //    $("#ctl00_ContentPlaceHolder1_TxtAgentCredit").focus();
            //    return false;
            //}

            if ($("#ctl00_ContentPlaceHolder1_TxtRemark").val() == "") {
                alert("Enter remark.");
                $("#ctl00_ContentPlaceHolder1_TxtRemark").focus();
                return false;
            }
                document.getElementById('backgroundPopup').style.display = 'block';
                document.getElementById('toPopupReport').style.display = 'block';
         

                return true;
        }


        function Validate() {
            if ($("#txtAgencyName").val() == "") {
                alert("Please select agent id");
                $("#txtAgencyName").focus();
                return false;
            }
            
        }
    </script>

                           <div id="toPopupReport" class="tbltbl large-12 medium-12 small-12">
        <div id="popup_content">
            <!--your content start-->
            <%--<table border="0" cellpadding="10" cellspacing="5" style="font-family: arial, Helvetica, sans-serif; width: 100%; font-size: 12px; font-weight: normal; font-style: normal; color: #000000">
                <tr>
                    <td>
                        <span id="PNR"></span>
                        <input id="txtPNRNO" name="txtPNRNO" type="hidden" />
                    </td>
                    
                </tr>
                
                <tr>
                    <td colspan="2">
                        <b>
                            <span id="RemarksTypetext"></span>Remark 
                        </b>
                        <input id="RemarksType" name="RemarksType" type="hidden" />

                    </td>
                </tr>
                
                
                
            </table>--%>

            <div class="row">
                <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="Enter OTP" />
                </div>
                 <div class="col-md-3">
                    <input type="button" class="btn btn-danger" id="btnverify" value="Verify OTP" />
                </div>
            </div>

        </div>
        <!--your content end-->
    </div>
           <div id="backgroundPopup">
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Upload > Set Credit Limit</h3>
                    </div>
                    <div class="panel-body">
     
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">User Id/Agency Id :</label>
                                    <input type="text" id="txtAgencyName" name="txtAgencyName" onfocus="focusObj(this);"
                                        onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID"
                                        class="form-control" />
                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <br />
                                    <%--<asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="button buttonBlue" OnClick="BtnSearch_Click" />--%>

                                    <%--<asp:Button ID="BtnDebit" runat="server" Text="DEBIT" OnClick="BtnDebit_Click" CssClass="button buttonBlue"  OnClientClick="return Check();" />--%>
                                    <asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="button buttonBlue" OnClientClick="return Validate();" OnClick="BtnSearch_Click" />

                                    
                                </div>
                            </div>


                        </div>
                        <div class="clear"></div>
                        <div class="row" id="DivAgencyDetals" runat="server" visible="false">

                            <div class="col-md-12 mtable">
                                <table class="table table-bordered">
                                    <tr>
                                        <td>Total Balance</td>
                                        <td id="tdTotalBAl" runat="server"></td>
                                        <td>User Id/Agency Id</td>
                                        <td id="tdAgentId" runat="server"></td>
                                        <td>Mobile</td>
                                        <td id="tdMobile" runat="server"></td>
                                    </tr>
                                    <tr>
                                        <td>Total Credit Limit</td>
                                        <td id="tdAgentCredit" runat="server"></td>
                                        <td>Name</td>
                                        <td id="tdAgnetName" runat="server"></td>
                                        <td>Email</td>
                                        <td id="tdEmail" runat="server"></td>

                                    </tr>

                                    <tr>
                                        <td>Due Amount</td>
                                        <td id="tdDueAmount" runat="server"></td>
                                        <td>Agency Name</td>
                                        <td id="tdAgencyName" runat="server"></td>
                                        <td>Address</td>
                                        <td id="tdAddress" runat="server"></td>
                                    </tr>


                                </table>
                            </div>

                        </div>



                        <div class="row" id="DivDetails" runat="server" visible="false">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Agency Details:</label>
                                    <br />
                                    <asp:Label ID="lblAgentDetails" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                                    <br />
                                    <asp:Label ID="lblDistrId" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                                    <asp:HiddenField ID="hdnUserId" runat="server" />
                                    <asp:HiddenField ID="hdnAgencyId" runat="server" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Aval Balance:</label>
                                    <asp:TextBox ID="TxtAvalBal" CssClass="form-control" runat="server" oncopy="return false" onpaste="return false" oncut="return false" ReadOnly="true" onKeyPress="return keyRestrict(event,'.0123456789');" MaxLength="7"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Total Credit Limit</label>
                                    <asp:TextBox ID="txtTotalCreditLimit" CssClass="form-control" runat="server" oncopy="return false" onpaste="return false" oncut="return false" ReadOnly="true" onKeyPress="return keyRestrict(event,'.0123456789');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Total Due Amount</label>
                                    <asp:TextBox ID="TxtDueAmount" CssClass="form-control" runat="server" oncopy="return false" onpaste="return false" oncut="return false" ReadOnly="true" onKeyPress="return keyRestrict(event,'.0123456789');" MaxLength="7"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="row" id="BalDetails" runat="server" visible="false">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Fixed Credit Limit:</label>
                                    <asp:TextBox ID="txtFixedCreditLimit" CssClass="form-control" runat="server" oncopy="return false" onpaste="return false" oncut="return false" onKeyPress="return keyRestrict(event,'.0123456789');" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Temp Credit Limit:</label>
                                    <asp:TextBox ID="TxtAgentCredit" CssClass="form-control" runat="server" oncopy="return false" onpaste="return false" oncut="return false" onKeyPress="return keyRestrict(event,'.0123456789');" MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Expiry Date  <span onclick="ClearRec();" class="hide" style="cursor: pointer; color: Red; font-weight: bold; text-decoration: blink;">&nbsp; &nbsp; &nbsp;  Clear Expiry Date</span></label>
                                    <span runat="server" id="spanExpiryDate">
                                        <input type="text" name="ToDate" id="ToDate" readonly="readonly" class="form-control" /></span>



                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Remark:</label>
                                    <asp:TextBox ID="TxtRemark" CssClass="form-control" runat="server" onpaste="return false" onKeyPress="return keyRestrict(event,' .0123456789abcdefghijklmnopqrstuvwxyz');"></asp:TextBox>
                                </div>
                            </div>


                        </div>
                        <div class="clear"></div>
                        <div class="row" id="DivUpdateButton" runat="server" visible="true">

                            <div class="col-md-3">
                                <div class="form-group">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                </div>
                            </div>
                            <div class="col-md-3" id="divsubmit" style="display:none">
                                <div class="form-group">
                                    <br />
                                    <asp:Button ID="BtnSubmit" runat="server" Text="Set Credit Limit" OnClick="BtnSubmit_Click" CssClass="button buttonBlue" />
                                    &nbsp;&nbsp;
                                </div>
                            </div>
                                  <div class="col-md-3">
                                <div class="form-group">
                                    <br />
                                    <input type="button" id="BtnSendotp" name="Send OTP" value="Send OTP" class="button buttonBlue" OnClick="return Check();" />
                                    &nbsp;&nbsp;
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="DivMsg" runat="server" style="color: red;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hidActionType" runat="server" Value="select" />
    </div>

    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script>
    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/search4.js") %>"></script>     --%>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>





</asp:Content>

