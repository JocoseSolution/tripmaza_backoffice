﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false" CodeFile="QCTicketReport.aspx.vb" Inherits="SprReports_Admin_QCTicketReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/Hotel/css/B2Bhotelengine.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/gridview-readonly-script.js")%>"></script>
    <script type="text/javascript">
        function fareRuleToolTip(id) {
            $.ajax({
                type: "POST",
                url: "QCTicketReport.aspx/GetFairRule",
                data: '{paxid: "' + id + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    debugger;
                    if (msg.d != "") {
                        $("#ddd").html(msg.d);
                        $("#outerdiv").show();

                    }
                    else {
                        alert("Fare Rule Not Available")
                    }
                },
                Error: function (x, e) {
                    alert("error")
                }
            });
        }
    </script>

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow: auto;
        }

        .tooltip1 {
            position: relative;
        }

        .tooltiptext {
            visibility: hidden;
            width: 120px;
            background-color: black;
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 5px 0;
            /* Position the tooltip */
            position: absolute;
            z-index: 1;
        }

        .tooltip1:hover .tooltiptext {
            visibility: visible;
        }

        .popupnew2 {
            position: absolute;
            top: 10px;
            left: 7%;
            width: 900PX;
            height: 500px !important;
            z-index: 1;
            box-shadow: 0px 5px 5px #f3f3f3;
            border: 2px solid #004b91;
            background-color: #fff;
            background-color: #ffffff !important;
            padding: 10px 20px;
            overflow-x: hidden;
        }

        .vew321 {
            background-color: #fff;
            width: 75%;
            float: right;
            padding: 5px 10px;
            text-align: justify;
            height: 300px;
            overflow-x: auto !important;
            overflow-y: auto !important;
            z-index: 1;
            position: fixed;
            top: 100px;
            left: 20%;
            border: 5px solid #d1d1d1;
        }

        .panel-primary > .panel-heading {
            width: 97.5%;
            color: #fff;
            margin-left: 13px;
            background-color: #f3f6ff;
            border: 1px solid #000;
        }

        .panel {
            border: 1px solid #fefefe;
        }

        h3 {
            color: #032451 !important;
        }
    </style>

    <div class="row">
        <div class="col-md-12" style="margin-top: 20px;">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight > Flight Search Booking Details</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="row">
                                <div class="col-md-4" id="divFromDate" runat="server">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-calendar"></i>
                                            <input type="text" name="From" id="From" class="theme-search-area-section-input" placeholder="From Date" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4" id="divToDate" runat="server">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-calendar"></i>
                                            <input type="text" name="To" id="To" class="theme-search-area-section-input" placeholder="To Date" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4" id="div1" runat="server">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-calendar"></i>
                                            <input type="text" name="OTFromDate" id="OTFromDate" class="theme-search-area-section-input" placeholder="Travel from date" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4" id="div2" runat="server">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-calendar"></i>
                                            <input type="text" name="OTToDate" id="OTToDate" class="theme-search-area-section-input" placeholder=" Travel to date" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:TextBox ID="txt_faretype" runat="server" CssClass="theme-search-area-section-input" placeholder="Enter Fare type"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                  <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:TextBox ID="txt_supplior" runat="server" CssClass="theme-search-area-section-input" placeholder="Enter Supplior Id"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>                               
                            </div>
                            <div class="row">
                                 <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:TextBox ID="txt_PNR" runat="server" CssClass="theme-search-area-section-input" placeholder="Enter pnr"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line" id="td_Agency" runat="server">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <input type="text" id="txtAgencyName" name="txtAgencyName" class="theme-search-area-section-input" onfocus="focusObj(this);"
                                                onblur="blurObj(this);" defvalue="Agency Name or ID" placeholder="user Id" autocomplete="off" value="Agency Name or ID" />
                                            <input type="hidden" id="hidtxtAgencyName" class="form-control" name="hidtxtAgencyName" value="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>

                                            <asp:TextBox ID="txt_PaxName" runat="server" CssClass="theme-search-area-section-input" placeholder="Enter pax name"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>                                                           
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:TextBox ID="txt_OrderId" runat="server" CssClass="theme-search-area-section-input" placeholder="Enter orderid"></asp:TextBox>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4" runat="server" visible="false">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:TextBox ID="txt_TransactionId" runat="server" CssClass="theme-search-area-section-input" placeholder="Enter transactionId"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:DropDownList CssClass="theme-search-area-section-input" ID="dd_status" runat="server">
                                                <asp:ListItem Text="ALL" Value="ALL">ALL</asp:ListItem>
                                                <asp:ListItem Text="Request" Value="Request"></asp:ListItem>
                                                <asp:ListItem Text="Pending" Value="Pending"></asp:ListItem>
                                                <asp:ListItem Text="Hold" Value="Confirm"></asp:ListItem>
                                                <asp:ListItem Text="HoldByAgent" Value="ConfirmByAgent"></asp:ListItem>
                                                <asp:ListItem Text="PreHoldByAgent" Value="PreConfirmByAgent"></asp:ListItem>
                                                <asp:ListItem Text="Rejected" Value="Rejected"></asp:ListItem>
                                                <asp:ListItem Text="Ticketed" Value="Ticketed"></asp:ListItem>
                                                <asp:ListItem Text="Failed" Value="Failed"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4" id="divPaymentMode" runat="server">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:DropDownList CssClass="theme-search-area-section-input" ID="txtPaymentmode" runat="server">
                                                <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                                <asp:ListItem Text="PG" Value="pg"></asp:ListItem>
                                                <asp:ListItem Text="Wallet" Value="wallet"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:TextBox ID="txt_TktNo" runat="server" CssClass="theme-search-area-section-input" placeholder="Enter ticket no"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>    
                            </div>
                            <div class="row">
                                <div class="col-md-4" id="divPartnerName" runat="server">
                                    <div class="theme-search-area-section theme-search-area-section-line" id="Partnernameid" runat="server">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:DropDownList CssClass="theme-search-area-section-input" ID="txtPartnerName" placeholder="Enter partner name" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <br />
                                    <div class="">
                                        <asp:Button ID="btn_result" runat="server" Text="Search Result" CssClass="button buttonBlue" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <br />
                                    <div class="">
                                        <asp:Button ID="btn_export" runat="server" CssClass="button buttonBlue" Text="Export" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="display: none;">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">
                                        Total Ticket Sale :
                                    </label>
                                    <asp:Label ID="lbl_Total" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" style="margin-top: 20px;">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">
                                        Total Records :
                                    </label>
                                    <asp:Label ID="lbl_counttkt" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow: auto; max-height: 500px;">
                                        <ContentTemplate>
                                            <asp:GridView ID="ticket_grdview" runat="server" AllowPaging="True" AllowSorting="True"
                                                AutoGenerateColumns="False" CssClass="table"
                                                GridLines="None" PageSize="30">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Booking Date/Time">
                                                        <ItemTemplate>

                                                            <a href='BookingTime.aspx?OrderId=<%#Eval("OrderId")%> '
                                                                rel="lyteframe" rev="width: 1200px; height: 500px; overflow:hidden;" target="_blank"
                                                                style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91"><b>
                                                                    <asp:Label ID="lblCreateDate_" runat="server" Text='<%#Eval("CreateDate")%>'></asp:Label></b>
                                                            </a>


                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Status" DataField="Status"></asp:BoundField>
                                                    <asp:TemplateField HeaderText="Booking Id">
                                                        <ItemTemplate>
                                                            <a href='../OrderDeatils.aspx?OrderId=<%#Eval("OrderId")%> &TransID=' style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                                <asp:Label ID="TID" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label>
                                                            </a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="PNR">
                                                        <ItemTemplate>
                                                            <asp:Label ID="GdsPNR" runat="server" Text='<%#Eval("GdsPnr")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Gross Amount" DataField="TotalBookingCost">
                                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="Passenger Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="FN" runat="server" Text='<%#Eval("Fname")%>'></asp:Label>
                                                            <asp:Label ID="LN" runat="server" Text='<%#Eval("Lname")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="JourneyType">
                                                        <ItemTemplate>
                                                            <asp:Label ID="JourneyType" runat="server" Text='<%#Eval("JourneyType")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="OB_TX ID">
                                                        <ItemTemplate>
                                                            <asp:Label ID="ReferenceNo" runat="server" Text='<%#Eval("ReferenceNo")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="AgencyName">
                                                        <ItemTemplate>
                                                            <asp:Label ID="AgencyName" runat="server" Text='<%#Eval("AgencyName")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="AgencyID">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAgencyID" runat="server" Text='<%#Eval("AgencyId")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="AirlinePnr">
                                                        <ItemTemplate>
                                                            <asp:Label ID="AirlinePnr" runat="server" Text='<%#Eval("AirlinePnr")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="TicketNumber" DataField="TicketNumber"></asp:BoundField>
                                                    <asp:BoundField HeaderText="Sector" DataField="sector"></asp:BoundField>
                                                    <asp:TemplateField HeaderText="TravelDate">
                                                        <ItemTemplate>
                                                            <asp:Label ID="JourneyDate" runat="server" Text='<%#Eval("JourneyDate")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Carrier">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Carrier" runat="server" Text='<%#Eval("VC")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Sector" DataField="Trip">
                                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField HeaderText="Trip Type" DataField="TripType">
                                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField HeaderText="BookingAmountNet" DataField="TotalAfterDis">
                                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                    </asp:BoundField>

                                                    <asp:BoundField HeaderText="Search ID" DataField="SearchId">
                                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField HeaderText="Booking ID" DataField="PNRId">
                                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField HeaderText="Ticketing ID" DataField="TicketId">
                                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="Payment Mode">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPaymentMode" runat="server" Text='<%#Eval("PaymentMode")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="PG TRAX ID">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPgTransaId" runat="server" Text='<%#Eval("PGTransactionid")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Branch">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Branch" runat="server" Text='<%#Eval("Branch")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="FareType">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFareType" runat="server" Text='<%#Eval("RESULTTYPE")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle CssClass="RowStyle" />
                                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                <PagerStyle CssClass="PagerStyle" />
                                                <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                <HeaderStyle CssClass="HeaderStyle" />
                                                <EditRowStyle CssClass="EditRowStyle" />
                                                <AlternatingRowStyle CssClass="AltRowStyle" />
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="text-decoration-line: initial; display: none" id="outerdiv" class="vew321">
        <div onclick="closethis();" title="click to close" style="background: url(../../Images/closebox.png); cursor: pointer; float: right; z-index: 9999; min-height: 30px; min-width: 30px;"></div>
        <div id="ddd"></div>
    </div>

    <script type="text/javascript">

        $(function () {
            $("#OTFromDate").datepicker(
                {
                    numberOfMonths: 1,
                    autoSize: true, dateFormat: 'dd-mm-yy', closeText: 'X', duration: 'slow', gotoCurrent: true, changeMonth: false,
                    changeYear: false, hideIfNoPrevNext: false, maxDate: '1y', navigationAsDateFormat: true, defaultDate: +0, showAnim: 'toggle', showOtherMonths: true,
                    selectOtherMonths: true, showoff: "button", buttonImageOnly: true, onSelect: UpdateOTToDate
                }
            )

        });
        $(function () {
            $("#OTToDate").datepicker(
                {
                    numberOfMonths: 1,
                    autoSize: true, dateFormat: 'dd-mm-yy', closeText: 'X', duration: 'slow', gotoCurrent: true, changeMonth: false,
                    changeYear: false, hideIfNoPrevNext: false, maxDate: '1y',navigationAsDateFormat: true, defaultDate: +0, showAnim: 'toggle', showOtherMonths: true,
                    selectOtherMonths: true, showoff: "button", buttonImageOnly: true
                }
            )

        });
        function UpdateOTToDate(dateText, inst) {
            $("#OTToDate").datepicker("option", { minDate: dateText });
        }

        function validate() {
            if (confirm("Are you sure you want to change status to hold!")) {
                return true;
            } else {
                return false;
            }
        }
        function closethis() {
            $("#outerdiv").hide();
        }

        $(function () {
            InitializeToolTip();
        });
    </script>
    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
</asp:Content>

