﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="EmulateAgentIdFalse.aspx.cs" Inherits="SprReports_Admin_EmulateAgentIdFalse" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>
    <script src="../../chosen/chosen.jquery.js" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .panel-primary > .panel-heading {
            width: 97.5%;
            color: #fff;
            margin-left: 13px;
            background-color: #f3f6ff;
            border: 1px solid #000;
        }

        .panel {
            border: 1px solid #fefefe;
        }

        h3 {
            color: #032451 !important;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function getKeyCode(e) {
            if (window.event)
                return window.event.keyCode;
            else if (e)
                return e.which;
            else
                return null;
        }
        function keyRestrict(e, validchars) {
            var key = '', keychar = '';
            key = getKeyCode(e);
            if (key == null) return true;
            keychar = String.fromCharCode(key);
            keychar = keychar.toLowerCase();
            validchars = validchars.toLowerCase();
            if (validchars.indexOf(keychar) != -1)
                return true;
            if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                return true;
            return false;
        }

        function Validate() {
            if ($("#txtAgencyName").val() == "") {
                alert("Please select agent id");
                $("#txtAgencyName").focus();
                return false;
            }
        }

        function ValidateSubmit() {
            if ($("#ctl00_ContentPlaceHolder1_TxtRemark").val() == "") {
                alert("Please enter remark.");
                $("#ctl00_ContentPlaceHolder1_TxtRemark").focus();
                return false;
            }
        }
    </script>
    <div class="row">
        <div class="col-md-12" style="margin-top: 20px;">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Emulate Agent Id Without OTP </h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">User Id/Agency Id :</label>
                                        <input type="text" id="txtAgencyName" name="txtAgencyName" onfocus="focusObj(this);"
                                            onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID"
                                            class="form-control" />
                                        <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                    </div>
                                </div>


                                <div class="col-md-3">
                                    <div class="form-group">
                                        <br />

                                        <asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="button buttonBlue" OnClientClick="return Validate();" OnClick="BtnSearch_Click" />
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="row" id="DivDetails" runat="server" visible="false">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Agency Details:</label><br />
                                        <asp:Label ID="lblAgentDetails" runat="server" Font-Bold="true"></asp:Label>
                                        <br />
                                        Agency Id:
                                        <asp:Label ID="lblAgencyID" runat="server" Font-Bold="true"></asp:Label>
                                        <%--<br />
                                     <asp:Label ID="LblUserId" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                                    <br />--%>

                                        <asp:Label ID="lblDistrId" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Aval Balance:</label>
                                        <asp:Label ID="LblAvlBal" runat="server" Font-Bold="true"></asp:Label>
                                        <br />
                                        <span>Due Amount:&nbsp;</span>
                                        <asp:Label ID="lblDueAmount" runat="server" Font-Bold="true"></asp:Label>
                                        <br />
                                        <span>Credit Limit:&nbsp; &nbsp; </span>
                                        <asp:Label ID="lblCreditLimit" runat="server" Font-Bold="true"></asp:Label>
                                        <br />
                                        <%--<asp:TextBox ID="TxtAvalBal" CssClass="form-control" runat="server" oncopy="return false" onpaste="return false" oncut="return false" ReadOnly="true" onKeyPress="return keyRestrict(event,'.0123456789');" MaxLength="7"></asp:TextBox>--%>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                        <asp:Label ID="lblName" runat="server" Font-Bold="true"></asp:Label>
                                        <br />
                                        <span>Mobile No:</span>
                                        <asp:Label ID="lblMobileNo" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                                        <br />
                                        <span>Email Id:&nbsp;&nbsp;&nbsp;</span>
                                        <asp:Label ID="lblEmailId" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                                        <br />
                                        <br />
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="row" id="BalDetails" runat="server" visible="false">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Remark:</label>
                                        <asp:TextBox ID="TxtRemark" CssClass="form-control" runat="server" onpaste="return false" onKeyPress="return keyRestrict(event,' .0123456789abcdefghijklmnopqrstuvwxyz');"></asp:TextBox>
                                        <%-- <label for="exampleInputPassword1">Credit Limit:</label>
                                    <asp:TextBox ID="TxtAgentCredit" CssClass="form-control" runat="server" oncopy="return false" onpaste="return false" oncut="return false" onKeyPress="return keyRestrict(event,'.0123456789');" MaxLength="7"></asp:TextBox>--%>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <br />
                                        <%--<asp:Button ID="BtnSubmit" runat="server" Text="Login" OnClick="BtnSubmit_Click" CssClass="button buttonBlue"  OnClientClick="return Check();" /> &nbsp;&nbsp;--%>
                                        <asp:Button ID="BtnSubmit" runat="server" Text="Login" OnClick="BtnSubmit_Click" CssClass="button buttonBlue" OnClientClick="return ValidateSubmit();" />
                                        &nbsp;&nbsp;
                                    </div>
                                </div>

                            </div>
                            <div class="clear"></div>
                            <div class="row">

                                <div class="col-md-12">
                                    <div id="DivMsg" runat="server" style="color: red;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hidActionType" runat="server" Value="select" />
        <asp:HiddenField ID="hdnUserId" runat="server" />
        <asp:HiddenField ID="hdnAgencyId" runat="server" />
        <asp:HiddenField ID="hdnMobile" runat="server" />
        <asp:HiddenField ID="hdnEmailID" runat="server" />
    </div>

    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
</asp:Content>
