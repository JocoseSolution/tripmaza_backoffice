﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="PGStatusByOorderId.aspx.cs" Inherits="SprReports_Accounts_PGStatusByOorderId" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .orderid {
            width: 78px;
            border-right: 1px solid #000;
            padding-right: 10px;
        }

        .Request {
            width: 463px !important;
            word-break: break-all;
            padding-right: 10px !important;
            border-right: thin solid #000;
            padding-left: 10px;
        }

        .Response {
            width: 763px !important;
            word-break: break-all;
            padding-right: 10px !important;
            padding-left: 10px;
        }

        .AnyMessage {
            padding-left: 10px;
            width: 100px;
        }

        .FBold {
            font-weight: bold;
            font-size: 14px;
        }

        .panel-primary > .panel-heading {
            width: 97.5%;
            color: #fff;
            margin-left: 13px;
            background-color: #f3f6ff;
            border: 1px solid #000;
        }

        .panel {
            border: 1px solid #fefefe;
        }

        h3 {
            color: #032451 !important;
        }
    </style>

    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-12" style="margin-top: 20px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Check PG Payment Status By Order Id</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">OrderId</label>
                                        <asp:TextBox ID="txt_OrderId" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1"></label>
                                        <br />
                                        <asp:Button ID="btn_result" runat="server" Text="Get Details" CssClass="button buttonBlue" OnClick="btn_result_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%-- <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <br />

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <br />

                                </div>
                            </div>
                        </div>

                        <div class="row">
                        </div>--%>
                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="col-md-12">
                                <div id="divReport" runat="server" visible="true" style="background-color: #fff; overflow-y: scroll;" class="large-12 medium-12 small-12">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <%--PG Reponse TR START--%>
                                        <tr id="trPGResponse" runat="server">
                                            <td>
                                                <h3>Current PG Response   </h3>
                                            </td>
                                        </tr>

                                        <tr id="trPGResponse1" runat="server">
                                            <td>
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td class="orderid FBold">Status</td>
                                                        <td class="Response FBold">Response</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="orderid" id="tdApiStatus" runat="server"></td>
                                                        <td class="Response" id="tdApiResponse" runat="server"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <%-- PG Reponse TR END--%>

                                        <%-- Agent Detail START--%>

                                        <tr id="trAgentDetails" runat="server">
                                            <td>
                                                <h3>Agent Details</h3>
                                            </td>
                                        </tr>

                                        <tr id="trAgentDetails1" runat="server">
                                            <td>
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td>Total Balance</td>
                                                        <td id="tdTotalBAl" runat="server"></td>
                                                        <td>User Id/Agency Id</td>
                                                        <td id="tdAgentId" runat="server"></td>
                                                        <td>Mobile</td>
                                                        <td id="tdMobile" runat="server"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Agent Credit Limit</td>
                                                        <td id="tdAgentCredit" runat="server"></td>
                                                        <td>Name</td>
                                                        <td id="tdAgnetName" runat="server"></td>
                                                        <td>Email</td>
                                                        <td id="tdEmail" runat="server"></td>

                                                    </tr>

                                                    <tr>
                                                        <td>Due Amount</td>
                                                        <td id="tdDueAmount" runat="server"></td>
                                                        <td>Agency Name</td>
                                                        <td id="tdAgencyName" runat="server"></td>
                                                        <td>Address</td>
                                                        <td id="tdAddress" runat="server"></td>
                                                    </tr>


                                                </table>
                                            </td>
                                        </tr>
                                        <%-- Agent Detail END--%>


                                        <tr id="trPaymentDetail" runat="server">
                                            <td>
                                                <h3>PG Payment Details</h3>
                                            </td>
                                        </tr>

                                        <tr id="trPaymentDetail1" runat="server">
                                            <td>
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td>Order Id</td>
                                                        <td id="tdOrderId" runat="server"></td>
                                                        <td>Using Service</td>
                                                        <td id="tdServiceType" runat="server"></td>
                                                        <td>User Id/Agency Name</td>
                                                        <td id="tdUserIdAndAgencyName" runat="server"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Payment Status</td>
                                                        <td id="tdPGSatus" runat="server"></td>
                                                        <td>Api Staus</td>
                                                        <td id="tdApiStatusData" runat="server"></td>
                                                        <td>Unmapped Status</td>
                                                        <td id="tdCheckStatus" runat="server"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Orignal Amount</td>
                                                        <td id="tdOrignalAmount" runat="server"></td>
                                                        <td>Trans.Charges</td>
                                                        <td id="tdTransCharge" runat="server"></td>
                                                        <td>Total Amount</td>
                                                        <td id="tdTotalAmount" runat="server"></td>
                                                    </tr>

                                                    <tr>
                                                        <td>Bank Ref No.</td>
                                                        <td id="tdBankRefNo" runat="server"></td>
                                                        <td>Payment Mode</td>
                                                        <td id="tdPaymentMode" runat="server"></td>
                                                        <td>Error Msg</td>
                                                        <td id="tdErrorMsg" runat="server"></td>
                                                    </tr>
                                                    <tr>

                                                        <td colspan="3"><%--<a href='IntInvoiceDetails.aspx?OrderId=<%#Eval("OrderId")%>&amp;invno=<%#Eval("OrderId")%>&amp;tktno=<%#Eval("OrderId")%>&amp;AgentID=<%#Eval("AgentId")%>'
                                                                        style="color: #004b91; font-size: 11px; font-weight: bold" target="_blank">
                                                                        <asp:Label ID="OrderId" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label>
                                                                       (Check Response )</a>--%></td>
                                                        <td>
                                                            <asp:Button ID="BtnChangeStatus" runat="server" Text="CHANGE STATUS" CssClass="button buttonBlue" OnClick="BtnChangeStatus_Click" /></td>
                                                        <td></td>
                                                        <td>
                                                            <asp:Button ID="BtnAddAmountInwallet" runat="server" Text="ADD AMOUNT & CHANGE STATUS" CssClass="button buttonBlue" OnClick="BtnAddAmountInwallet_Click" /></td>
                                                    </tr>

                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="trMessage" runat="server">

                                            <td style="color: red;">Note:&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;<br />
                                                1.Login Payment Gateway websites and Check payment status and payment received or not received in BANK A/C.<br />
                                                2.Check  Received  Total Amount.<br />
                                                3.Check Convenience Fee, Original Amount and Total Amount<br />
                                                Convenience Fee + Original Amount=TotalAmount<br />
                                                4.Upload only  Original Amount,because Convenience Fee is payment gateway transaction charge<br />
                                                (Payment Mode Debit Card,Credit Card ,Net Banking etc.)<br />


                                            </td>
                                        </tr>

                                        <tr id="trLedger" runat="server">
                                            <td>
                                                <h3>Ledger Details </h3>
                                            </td>
                                        </tr>

                                        <tr id="trLedger1" runat="server">
                                            <td>





                                                <div class="row">
                                                    <div class="col-md-12" style="overflow: auto;">

                                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false"
                                                            CssClass="table" GridLines="None" Width="100%">
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="OrderId">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblId" runat="server" Text='<%#Eval("InvoiceNo") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="UserId">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAgentID" runat="server" Text='<%#Eval("AgentID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="AgencyName">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAgencyName" runat="server" Text='<%#Eval("AgencyName") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="DR.">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDebit" runat="server" Text='<%#Eval("Debit") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="CR.">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCredit" runat="server" Text='<%#Eval("Credit") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="BookingType">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblBookingType" runat="server" Text='<%#Eval("BookingType") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Remark">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblRemark" runat="server" Text='<%#Eval("Remark") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="PaymentMode">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblPaymentMode" runat="server" Text='<%#Eval("PaymentMode") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <%--AgentID,AgencyName,InvoiceNo,Debit,Credit,CreatedDate,BookingType,Remark,PaymentMode--%>
                                                                <asp:TemplateField HeaderText="CreatedDate">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCreatedDate" runat="server" Text='<%#Eval("CreatedDate") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="RowStyle" />
                                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                            <PagerStyle CssClass="PagerStyle" />
                                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                            <HeaderStyle CssClass="HeaderStyle" />
                                                            <EditRowStyle CssClass="EditRowStyle" />
                                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                                            <EmptyDataTemplate>No records found</EmptyDataTemplate>
                                                        </asp:GridView>

                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr id="trRequest" runat="server">
                                            <td>
                                                <h3>Request/Response </h3>
                                            </td>
                                        </tr>

                                        <tr id="trRequest1" runat="server">
                                            <td>

                                                <%-- <table style="width: 100%">
                                                <tr>
                                                    <td class="orderid">Order Id</td>
                                                    <td class="Request">Request</td>
                                                    <td class="Request">Response</td>
                                                    <td class="AnyMessage">Any Message</td>
                                                </tr>
                                                <tr>
                                                    <td class="orderid"></td>
                                                    <td class="Request"></td>
                                                    <td class="Request"></td>
                                                    <td class="AnyMessage"></td>
                                                </tr>


                                            </table>--%>



                                                <div class="row">
                                                    <div class="col-md-12" style="overflow: auto;">

                                                        <asp:GridView ID="Grid1" runat="server" AutoGenerateColumns="false"
                                                            CssClass="table" GridLines="None" Width="100%">
                                                            <Columns>
                                                                <%-- OrderId,ApiResponse,ChcekStatusBy,AddToWallet,CreatedDate--%>
                                                                <asp:TemplateField HeaderText="ID">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblID" runat="server" Text='<%#Eval("Id") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="OrderId">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblId" runat="server" Text='<%#Eval("OrderId") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="APIResponse">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblApiResponse" runat="server" Text='<%#Eval("ApiResponse") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="UpdateStatusBy">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblChcekStatusBy" runat="server" Text='<%#Eval("ChcekStatusBy") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="CreatedDate">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCreatedDate" runat="server" Text='<%#Eval("CreatedDate") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="RowStyle" />
                                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                            <PagerStyle CssClass="PagerStyle" />
                                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                            <HeaderStyle CssClass="HeaderStyle" />
                                                            <EditRowStyle CssClass="EditRowStyle" />
                                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                                            <EmptyDataTemplate>No records found</EmptyDataTemplate>
                                                        </asp:GridView>

                                                    </div>
                                                </div>
                                            </td>
                                        </tr>


                                    </table>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnTotalCredit" runat="server" />
    <asp:HiddenField ID="hdnOrderNo" runat="server" />
    <asp:HiddenField ID="HdnUserId" runat="server" />
    <asp:HiddenField ID="hdnApiStatus" runat="server" />
    <asp:HiddenField ID="hdnUnmappedStatus" runat="server" />
    <asp:HiddenField ID="HdnOldApiStatus" runat="server" />
    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>


</asp:Content>

