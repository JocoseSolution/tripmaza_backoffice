﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false" CodeFile="CurrencyExchangeRate.aspx.cs" Inherits="SprReports_Accounts_CurrencyExchangeRate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/Styles/main.css") %>" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .panel-primary > .panel-heading {
            width: 97.5%;
            color: #fff;
            margin-left: 13px;
            background-color: #f3f6ff;
            border: 1px solid #000;
        }

        .panel {
            border: 1px solid #fefefe;
        }

        h3 {
            color: #032451 !important;
        }
    </style>
    <div class="row">
        <div class="col-md-12" style="margin-top: 70px;">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-title">Currency Exchange Rate in INR</div>
                    </div>
                    <div class="panel-body">

                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="col-md-12">
                                <div class="col-md-2">Country Name</div>
                                <div class="col-md-2">
                                    <input type="text" name="CountryName" id="CountryName" onkeypress="return isCharKey(this)" />
                                    <input type="hidden" id="CountryCode" class="CLASS_HID_GE" name="CountryCode" />
                                </div>
                                <div class="col-md-2">
                                    <input id="btn_search" type="submit" value="Show ALL" class="button buttonBlue" />
                                </div>
                            </div>

                            <div class="clear1"></div>
                            <div class="clear1"></div>
                            <div style="width: 100%; overflow-x: scroll; max-height: 490px;">
                                <div id="gridContent" class="w100 lft"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="loader"></div>

    <script type="text/javascript">
        $(document).ready(function () {
            Bindgrid('', '', '', 'ReportCurrency');

            $('.edit-mode').hide();
            $("#CountryName").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: UrlBase + "SprReports/CurrencyConverter.asmx/CurrancyDetails",
                        contentType: 'application/json; charset=utf-8', type: 'POST', dataType: 'json',
                        data: "{ 'CountryName': '" + request.term + "', 'CountryCode': '', 'CurrancyCode': '', 'ReqType': 'CurrencyAutoComplete'}",
                        dataFilter: function (data) { return data; },
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.CountryName + ", " + item.CountryCode + " (" + item.CurrancyCode + ")",
                                    label1: item.CountryCode,
                                    value: item.CountryName + ", " + item.CountryCode + " (" + item.CurrancyCode + ")"
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                minLength: 3, //The minimum number of characters a user has to type before the Autocomplete activates.
                select: function (event, ui) {// Select Event is Triggered when an item is selected from the menu; ui.item refers to the selected item.  
                    $("#CountryCode").val(ui.item.label1);
                    Bindgrid('', ui.item.label1, '', 'ReportCurrency');
                },
                change: function (event, ui) {
                    if (!ui.item) {
                        $("#CountryCode").val('');
                    }
                }
            });
        });

        function SaveCurrancy(pid) {

            var Name = $("#txt" + pid).val();
            $.ajax({
                url: UrlBase + "SprReports/CurrencyConverter.asmx/CurrencyExchangeRateUpdate",
                contentType: 'application/json; charset=utf-8', type: 'POST', dataType: 'json',
                data: "{ 'Id': " + pid + ", 'amt': " + Name + ", 'ReqType': 'UpdateCurrency'}",
                success: function (result) {
                    if (result.d == "Updated") {
                        alert("Currency Updated successfully");
                        window.location.href = UrlBase + "SprReports/Accounts/CurrencyExchangeRate.aspx";
                        //$("#" + pid).hide();
                        //$("#txt" + pid).hide();
                        //$("#cancel" + pid).hide();
                        //$("#EDIT" + pid).show();
                        //$("#cur" + pid).show();
                        //$("#cur" + pid).val($("#txt" + pid).val()); alert($("#txt" + pid).val());
                    }
                }
            });

        }
        function Deletediscdetails(pid) {
            if (confirm("Do you want to delete Currency: " + pid)) {
                $.ajax({
                    url: UrlBase + "SprReports/CurrencyConverter.asmx/CurrencyExchangeRateUpdate",
                    contentType: 'application/json; charset=utf-8', type: 'POST', dataType: 'json',
                    data: "{ 'Id': " + pid + ", 'amt': 0, 'ReqType': 'DeleteCurrency'}",
                    success: function (result) {
                        if (result.d == "Deleted") {
                            alert("Currency deleted successfully");
                            window.location.href = UrlBase + "SprReports/Accounts/CurrencyExchangeRate.aspx";
                        }
                        else
                            alert("Could not delete");
                    }
                });
            }
        }
        function EditCurrancy(pid) {
            $("#" + pid).show();
            $("#txt" + pid).show();
            $("#cancel" + pid).show();
            $("#EDIT" + pid).hide();
            $("#cur" + pid).hide();
        }
        function CancelCurrancy(pid) {
            $("#" + pid).hide();
            $("#txt" + pid).hide();
            $("#cancel" + pid).hide();
            $("#EDIT" + pid).show();
            $("#cur" + pid).show();

        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode >= 48 && charCode <= 57 || charCode == 08 || charCode == 46) {
                return true;
            }
            else {

                return false;
            }
        }
        function isCharKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode >= 65 && charCode <= 90 || charCode >= 97 && charCode <= 122 || charCode == 32 || charCode == 08) {
                return true;
            }
            else {
                return false;
            }
        }
        function Bindgrid(CountryName, CountryCode, CurrancyCode, ReqType) {
            var strcurrancy = "";
            $.ajax({
                url: UrlBase + "SprReports/CurrencyConverter.asmx/CurrancyDetails",
                contentType: 'application/json; charset=utf-8', type: 'POST', dataType: 'json',
                data: "{ 'CountryName': '" + CountryName + "', 'CountryCode': '" + CountryCode + "', 'CurrancyCode': '" + CurrancyCode + "', 'ReqType': '" + ReqType + "'}",
                success: function (result) {
                    if (result.d != null)
                        strcurrancy += "<div class='w100 bld'><div class='w25 lft'>Country Name</div><div class='w25 lft'>Currency Name</div><div class='w25 lft'>Update Date</div><div class='w25 lft'><div class='w70 lft'>Exchange Rate</div><div class='w30 lft'>Edit Delete</div></div></div>";
                    strcurrancy += "<div class='clear'></div>";
                    for (var i = 0; i < result.d.length; i++) {
                        var currancylist = result.d[i];
                        strcurrancy += "<div class='w100'><div class='w25 lft'>" + currancylist.CountryName + " (" + currancylist.CountryCode + ")</div>";
                        strcurrancy += "<div class='w25 lft'>" + currancylist.CurrancyName + " (" + currancylist.CurrancyCode + ")</div><div class='w25 lft'>" + currancylist.UpdateDate + "</div>";
                        strcurrancy += "<div class='w25 lft'><div class='w70 lft'> <span class='display-mode' id='cur" + currancylist.IDS + "'>" + currancylist.ExchangeRate + "</span> <input type='text' id='txt" + currancylist.IDS + "' value=" + currancylist.ExchangeRate + " class='edit-mode' style='width:83px;' /></div>";
                        //strcurrancy +="<a class='edit-user display-mode' ><img src='/Images/edit-icon.png' title='Edit' /></a>   ";
                        // strcurrancy +=" <a class='save-user edit-mode' id="+ currancylist.IDS +"><img src='/Images/ok.png' title='Save' /></a> ";
                        strcurrancy += "<div class='w30 lft'><a class='edit-user display-mode' id='EDIT" + currancylist.IDS + "' ><img src='/Images/edit-icon.png' title='Edit' onclick='EditCurrancy(" + currancylist.IDS + ")' /></a>   ";
                        strcurrancy += " <a class='save-user edit-mode' id='" + currancylist.IDS + "'><img src='/Images/ok.png' title='Save' onclick='SaveCurrancy(" + currancylist.IDS + ")' /></a> ";
                        strcurrancy += "<a class='cancel-user edit-mode' id='cancel" + currancylist.IDS + "'><img src='/Images/cancel-icon.png' title='Cancel'  onclick='CancelCurrancy(" + currancylist.IDS + ")' /></a> ";
                        strcurrancy += "<a class='topbutton Delete' title='Delete' onclick='Deletediscdetails(" + currancylist.IDS + ")' ><img src='/Images/delete-icon.png' title='Delete'/> </a>";
                        strcurrancy += "</div></div>";
                    }
                    $('#gridContent div').empty('');
                    $("#gridContent").html(strcurrancy); $('.edit-mode').hide();
                },
                error: function (xhr, status) { alert(xhr.responseText); }
            });

        }
    </script>
</asp:Content>

