﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="CashInFlow.aspx.vb" Inherits="Reports_Accounts_CashInFlow" %>

<%@ Register Src="~/UserControl/AccountsControl.ascx" TagPrefix="uc1" TagName="Account" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<link href="../../css/main2.css" rel="stylesheet" type="text/css" />--%>

    <script src="../../JS/JScript.js" type="text/javascript"></script>

    <script src="../../JS/lytebox.js" type="text/javascript"></script>

    <link href="../../CSS/lytebox.css" rel="stylesheet" type="text/css" />

    <%--<link href="../../CSS/style.css" rel="stylesheet" type="text/css" />--%>
    <link href="../../css/basic.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
    <style type="text/css">
        .txtCalander {
            width: 100px;
            background-image: url(../../images/cal.gif);
            background-repeat: no-repeat;
            background-position: right;
            cursor: pointer;
            border: 1px #D6D6D6 solid;
            margin-bottom: 10px;
        }

        .form-control {
            margin-bottom: 20px;
        }

        .panel-primary > .panel-heading {
            width: 97.5%;
            color: #fff;
            margin-left: 13px;
            background-color: #f3f6ff;
            border: 1px solid #000;
        }

        .panel {
            border: 1px solid #fefefe;
        }

        h3 {
            color: #032451 !important;
        }
    </style>

    <script type='text/javascript'>
        function validate() {

            if (document.getElementById("ctl00_ContentPlaceHolder1_txt_TC").value == "") {
                alert('Please Fill TC');
                document.getElementById("ctl00_ContentPlaceHolder1_txt_TC").focus();
                return false;

            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_txt_DBN").value == "") {
                alert('Please Fill DBN');
                document.getElementById("ctl00_ContentPlaceHolder1_txt_DBN").focus();
                return false;

            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_txt_Rmk").value == "") {
                alert('Please Fill Rmk');
                document.getElementById("ctl00_ContentPlaceHolder1_txt_Rmk").focus();
                return false;

            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_txt_Amt").value == "") {
                alert('Please Fill Amt');
                document.getElementById("ctl00_ContentPlaceHolder1_txt_Amt").focus();
                return false;

            }


        }
    </script>

    <div class="mtop80"></div>
    <div class="large-12 medium-12 small-12">
        <%--<div class="large-2 medium-2 small-12 columns">
            <uc1:Account runat="server" ID="Settings" />
        </div>--%>

        <div class="large-9 medium-9 small-12 columns end">

            <div class="row">
                <div class="col-md-2"></div>

                <div class="col-md-12" style="margin-top: 20px;">
                    <div class="page-wrapperss">

                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Account > Cash Inflow </h3>
                            </div>
                            <div class="panel-body" style="overflow: scroll;">
                                <div class="large-12 medium-12 small-12 heading">
                                    <%--<div class="large-12 medium-12 small-12 heading1">CashInFlow Report</div>
                <div class="clear1"></div>--%>
                                    <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td valign="top" width="50%">
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td width="90" valign="top">From :
                                                            </td>
                                                            <td width="290" valign="top">
                                                                <input type="text" name="From" id="From" class="txtCalander form-control" readonly="readonly"
                                                                    style="width: 290px" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="90" valign="top">To :
                                                            </td>
                                                            <td width="290" valign="top">
                                                                <input type="text" name="To" id="To" class="txtCalander form-control" readonly="readonly" style="width: 290px" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="90" valign="top">Upload Type:
                                                            </td>
                                                            <td width="290" valign="top">
                                                                <asp:DropDownList ID="ddl_UploadType" runat="server" Width="290px" CssClass="form-control">
                                                                    <asp:ListItem Selected="True" Value="Select Type">Select Type</asp:ListItem>
                                                                    <asp:ListItem Value="CA">Cash</asp:ListItem>
                                                                    <asp:ListItem Value="CR">Credit</asp:ListItem>
                                                                    <asp:ListItem Value="CC">Card</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" id="tr_SearchType" runat="server" visible="false">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td width="90" valign="top">Search Type
                                                                        </td>
                                                                        <td style="width: 290px">
                                                                            <asp:RadioButton ID="RB_Agent" runat="server" Checked="true" GroupName="Trip" onclick="Show(this)"
                                                                                Text="Agent" />
                                                                            &nbsp;&nbsp;&nbsp;
                                                            <asp:RadioButton ID="RB_Distr" runat="server" GroupName="Trip" onclick="Hide(this)"
                                                                Text="Own" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="top" width="50%">
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td valign="top" width="90">Cash Flow Type:
                                                            </td>
                                                            <td width="290" valign="top" style="margin-bottom: 20px;">
                                                                <asp:DropDownList ID="ddlInflowtype" runat="server" AutoPostBack="True" Width="290px" CssClass="form-control">
                                                                    <asp:ListItem Value="InFlow">InFlow</asp:ListItem>
                                                                    <asp:ListItem Value="OutFlow">OutFlow</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr id="tr_UploadType" runat="server">
                                                            <td width="90" valign="top">Agency Type:
                                                            </td>
                                                            <td width="290" valign="top">

                                                                <asp:RadioButtonList ID="RBL_Type" runat="server" AutoPostBack="True" RepeatDirection="Horizontal"
                                                                    Width="290px" CellPadding="4" CellSpacing="4">
                                                                    <asp:ListItem Value="CA" Selected="True">&nbsp;Cash</asp:ListItem>
                                                                    <asp:ListItem Value="CR">&nbsp;Credit</asp:ListItem>
                                                                </asp:RadioButtonList>

                                                            </td>
                                                        </tr>
                                                        <tr id="tr_UploadCategory" runat="server">
                                                            <td width="90" valign="top">Agency Category:
                                                            </td>
                                                            <td width="290" valign="top">
                                                                <asp:DropDownList ID="ddl_Category" runat="server" Width="290px" CssClass="form-control">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <table width="100%" id="td_ag" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td id="tr_AgencyName" runat="server" width="90">Agency Name:
                                                                        </td>
                                                                        <td width="290" valign="top" id="tr_Agency" runat="server">
                                                                            <input type="text" id="txtAgencyName" name="txtAgencyName" style="width: 290px" onfocus="focusObj(this);"
                                                                                onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID" class="form-control" />
                                                                            <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" style="padding-top: 8px">
                                                                <asp:Button ID="btn_search" runat="server" Text="Search" CssClass="button buttonBlue" Width="100px" />
                                                                &nbsp;<asp:Button ID="btn_Export" runat="server" Text="Export" OnClientClick="return Validation()"
                                                                    CssClass="button buttonBlue" Width="100px" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                                        <table width="100%">
                                            <tr>
                                                <td align="left">
                                                    <asp:Label CssClass="pnrdtls" ID="lbl_amount" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:UpdatePanel ID="UP" runat="server">
                                                        <ContentTemplate>
                                                            <table width="90%">
                                                                <tr>
                                                                    <td id="td_SalesRmk" runat="server" visible="false" align="center">
                                                                        <div id="basic-modal-content3" style="padding: 10px; overflow: auto; width: 800px; border: thin solid #161946;">
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td class="h2" style="font-size: 14px; padding-bottom: 5px; font-family: arial, Helvetica, sans-serif; color: #161946;"
                                                                                        align="center" colspan="6">Update Deposite Details
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left" width="130px" height="30px">TransactionID/Cheque No
                                                                                    </td>
                                                                                    <td align="left" width="140px">
                                                                                        <asp:TextBox ID="txt_TC" runat="server" Width="135px"></asp:TextBox>
                                                                                    </td>
                                                                                    <td align="left" width="120px">&nbsp;&nbsp;&nbsp; Deposite Bank Name
                                                                                    </td>
                                                                                    <td align="left" width="155px">
                                                                                        <asp:TextBox ID="txt_DBN" runat="server" Width="150px"></asp:TextBox>
                                                                                    </td>
                                                                                    <td width="80px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Amount
                                                                                    </td>
                                                                                    <td align="left">
                                                                                        <asp:TextBox ID="txt_Amt" runat="server" Width="90px"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left" style="padding-top: 5px">Remark
                                                                                    </td>
                                                                                    <td align="left" colspan="5">
                                                                                        <asp:TextBox ID="txt_Rmk" runat="server" Height="50px" TextMode="MultiLine" Width="500px"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right" style="padding-right: 5px; padding-top: 5px;" valign="top">
                                                                                        <asp:CheckBox ID="chk_status" runat="server" />
                                                                                    </td>
                                                                                    <td align="left" style="padding-top: 5px" colspan="3">Rest of the amount should be debited from portal balance
                                                        <br />
                                                                                        &nbsp;<asp:Label ID="lbl_msg" runat="server" ForeColor="#FF3300" Font-Bold="True"></asp:Label>
                                                                                    </td>
                                                                                    <td colspan="2" align="left">
                                                                                        <asp:Button ID="btn_Submit" runat="server" Text="Update" OnClientClick="return validate();"
                                                                                            CssClass="button" />
                                                                                        &nbsp;<asp:Button ID="btn_cancel" runat="server" Text="Cancel" CssClass="button" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        <asp:GridView ID="grd_CashInflow" runat="server" AutoGenerateColumns="false" DataKeyNames="AccID"
                                                                            CssClass="table" AllowPaging="True" PageSize="30" Width="100%">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="SL&nbsp;No.">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lbl_counter" runat="server" Text='<%#Eval("Counter") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Invoice&nbsp;No.">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lbl_Invoice" runat="server" Text='<%#Eval("InvoiceNo") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="AgencyID">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lbl_agentid" runat="server" Text='<%#Eval("AgencyId")%>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="User&nbsp;ID">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lbl_agentid" runat="server" Text='<%#Eval("AgentID") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Agency&nbsp;Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lbl_agencyname" runat="server" Text='<%#Eval("AgencyName") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Amount">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lbl_amt" runat="server" Text='<%#Eval("Amount") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Upload&nbsp;Type">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lbl_type" runat="server" Text='<%#Eval("UploadType") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <EditItemTemplate>
                                                                                        <asp:TextBox ID="txt_updatetype" runat="server" Text='<%#Eval("UploadType") %>'></asp:TextBox>
                                                                                    </EditItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Remark&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lbl_remark" runat="server" Text='<%#Eval("Remark") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="YtrRcptNo">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lbl_YtrRcptNo" runat="server" Text='<%#Eval("YtrRcptNo") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <EditItemTemplate>
                                                                                        <asp:TextBox ID="txt_YtrRcptNo" runat="server" Text='<%#Eval("YtrRcptNo") %>'></asp:TextBox>
                                                                                    </EditItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Updated&nbsp;Remark">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lbl_updatedremark" runat="server" Text='<%#Eval("UpdatedRemark") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <EditItemTemplate>
                                                                                        <asp:TextBox ID="txt_updateremark" runat="server" Text='<%#Eval("UpdatedRemark") %>'></asp:TextBox>
                                                                                    </EditItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Created&nbsp;Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lbl_Cdate" runat="server" Text='<%#Eval("CreatedDate") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Updated&nbsp;Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lbl_updateddate" runat="server" Text='<%#Eval("UpdatedDate") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="View&nbsp;Adjustment">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnk_adjdtl" runat="server">
                                                    <a href='UploadAdjustment.aspx?Counter=<%#Eval("Counter")%>&Type=View'
                                                        rel="lyteframe" rev="width: 900px; height: 300px; overflow:hidden;" target="_blank"
                                                        style="font-family: Arial, Helvetica, sans-serif; font-size: 12px;  ;
                                                        color: #161946"> Details
                                                        </a></asp:LinkButton>
                                                                                        <%--   <asp:Button ID="btn_edit" runat="server" Text="Edit" Font-Bold="true" CommandName="Edit" />--%>
                                                                                    </ItemTemplate>
                                                                                    <%--<EditItemTemplate>
                                                        <asp:Button ID="btn_update" runat="server" Text="Update" CommandName="Update" Font-Bold="true" />
                                                        <asp:Button ID="btn_cancel" runat="server" Text="Cancel" CommandName="Cancel" Font-Bold="true" />
                                                    </EditItemTemplate>--%>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Edit/Update">
                                                                                    <ItemTemplate>
                                                                                        <asp:Button ID="btn_edit" runat="server" Text="Edit" Font-Bold="true" CommandName="Edit" />
                                                                                    </ItemTemplate>
                                                                                    <EditItemTemplate>
                                                                                        <asp:Button ID="btn_update" runat="server" Text="Update" CommandName="Update" Font-Bold="true" />
                                                                                        <asp:Button ID="btn_cancel" runat="server" Text="Cancel" CommandName="Cancel" Font-Bold="true" />
                                                                                    </EditItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Adjustment">
                                                                                    <ItemTemplate>
                                                                                        <%--<asp:LinkButton ID="lnk_adj" runat="server">
                                                    <a href='UploadAdjustment.aspx?Counter=<%#Eval("Counter")%>&Type=Insert'
                                                        rel="lyteframe" rev="width: 900px; height: 300px; overflow:hidden;" target="_blank"
                                                        style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; 
                                                        color: #161946">  AdjustAmount
                                                        </a></asp:LinkButton>--%>

                                                                                        <%--   <asp:Button ID="btn_edit" runat="server" Text="Edit" Font-Bold="true" CommandName="Edit" />--%>
                                                                                    </ItemTemplate>
                                                                                    <%--<EditItemTemplate>
                                                        <asp:Button ID="btn_update" runat="server" Text="Update" CommandName="Update" Font-Bold="true" />
                                                        <asp:Button ID="btn_cancel" runat="server" Text="Cancel" CommandName="Cancel" Font-Bold="true" />
                                                    </EditItemTemplate>--%>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Type">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lbl_Agent_Type" runat="server" Text='<%#Eval("Agent_Type") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="SalesExecID">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lbl_SalesExecID" runat="server" Text='<%#Eval("SalesExecID") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Edit Details" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnk_EditPayment" runat="server" CommandName="EditDetail" CommandArgument='<%#Eval("Counter") %>'
                                                                                            Font-Bold="True" ForeColor="#161946" Font-Size="10px">Update&nbsp;Payment&nbsp;Details</asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="View">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnk_View" runat="server" Visible="false" CommandName="View" CommandArgument='<%#Eval("Counter") %>'
                                                                                            Font-Bold="True" ForeColor="#161946" Font-Size="10px">View&nbsp;Payment&nbsp;Details</asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lbl_SalesUpDate" runat="server" Text='<%#Eval("UpdatedDateSales") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <RowStyle CssClass="RowStyle" />
                                                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                                            <PagerStyle CssClass="PagerStyle" />
                                                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                                            <HeaderStyle CssClass="HeaderStyle" />
                                                                            <EditRowStyle CssClass="EditRowStyle" />
                                                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                                                        </asp:GridView>
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top" align="left">
                                                                        <div id="basic-modal-content" style="padding: 20px; overflow: auto; width: 100%;">
                                                                            <table width="100%" border="0" cellpadding="10" cellspacing="10">
                                                                                <tr>
                                                                                    <td colspan="4" style="font-family: arial, Helvetica, sans-serif; font-size: 14px;" align="left">
                                                                                        <p class="bld">(Executive Updated Upload Details)</p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left" width="180px" style="">TransactionID/ChequeNo :
                                                                                    </td>
                                                                                    <td id="td_TC" runat="server" width="170px"></td>
                                                                                    <td align="left" width="130px" style="">Deposite Bank Name :
                                                                                    </td>
                                                                                    <td id="td_DBN" runat="server"></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left" style="">Amount :
                                                                                    </td>
                                                                                    <td id="td_Amt" runat="server"></td>
                                                                                    <td align="left" style="">Updated Date :
                                                                                    </td>
                                                                                    <td id="td_UD" runat="server"></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left" style="">Debit Portal Balance :
                                                                                    </td>
                                                                                    <td id="td_DPB" runat="server" class="style1"></td>
                                                                                    <td align="left" style="">Remark :
                                                                                    </td>
                                                                                    <td id="td_Rmk" runat="server" align="left" class="style1"></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <%--<asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UP">
                                <ProgressTemplate>
                                    <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden;
                                        padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5;
                                        z-index: 1000;">
                                    </div>
                                    <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center;
                                        z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px;
                                        font-weight: bold; color: #000000">
                                        Please Wait....<br />
                                        <br />
                                        <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                        <br />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>--%>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="clear"></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

    <script src="../../Utility/JS/jquery.simplemodal.js" type="text/javascript"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Distributor.js") %>"></script>

    <script type='text/javascript'>

        function openDialog() {

            $(function () {
                //                alert('hi');
                $('#basic-modal-content').modal();
                return false;

            });




        }

    </script>
</asp:Content>
