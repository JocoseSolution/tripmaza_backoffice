﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_Accounts_ProcessedRefund : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    DataSet DSHold = new DataSet();
    DataSet DSCancel = new DataSet();
    private SqlTransaction ST = new SqlTransaction();
    private SqlTransactionDom STDom = new SqlTransactionDom();
    DataSet DS = new DataSet();
    string UserId = "", UserType = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == null || Session["UID"].ToString() == "")
        {
            Response.Redirect("~/Login.aspx");
        }
        UserId = Session["UID"].ToString();
        UserType = Session["User_Type"].ToString();
        if (IsPostBack != true)
        {
          
        }
    }


    protected void btn_result_Click(object sender, System.EventArgs e)
    {

        //ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "window.opener.location=window.opener.location.href;", true);
        try
        {


            lbl_Total.Text = "";
            lbl_counttkt.Text = "";
            Label1.Text = "";
            String msg = "";
            lbltxt1.Text = "";
            string str_OrderId = "";

            lnkupdate.Visible = false;
            GridRefunddtl.DataBind();
           
            str_OrderId = txt_OrderId.Text.Trim().ToString();
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                sqlcmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "USP_CheckBeforeRefund";
                sqlcmd.Parameters.AddWithValue("@OrderID", txt_OrderId.Text.ToString());
                //SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
                //da.Fill(DS);
                 msg = sqlcmd.ExecuteScalar().ToString();
                con.Close();

                if (msg == "yes" || msg == "")
                    {
                      
                        CheckEmptyValue();
                       
                    }

                    else
                    {
                        lbltxt1.Text = msg;
                       // lnkupdate.Visible = false;

                    }
               
                
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message + "Stack trace" + ex.StackTrace);
        }


           
    }

    public void CheckEmptyValue()    
    {
        try
        {
            UserId = Session["UID"].ToString();
            UserType = Session["User_Type"].ToString();
            string str_PNR = "", str_OrderId = "" ;
            str_PNR = txt_Pnrno.Text.Trim().ToString();
            str_OrderId = txt_OrderId.Text.Trim().ToString();        
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                sqlcmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "USP_SEARCHREFUNDSTATUS";
                sqlcmd.Parameters.AddWithValue("@pnrno", txt_Pnrno.Text.ToString());
                sqlcmd.Parameters.AddWithValue("@OrderID", txt_OrderId.Text.ToString());
                SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
                da.Fill(DS);                
                con.Close();
                   if(DS.Tables[0].Rows.Count > 0)
                   {
                       if (DS.Tables[0].Rows[0][0].ToString() == "no record found")
                       {                        
                           
                           ClientScript.RegisterStartupScript(typeof(Page), "MessagePopUp", "<script   language='JavaScript'>alert('No Refund Has Been Processed Earlier.'); window.location.href = 'ProcessedRefund.aspx';</script>");
                           
                       }
                       else
                       {
                           lbl_Total.Text = DS.Tables[0].Rows[0]["Total"].ToString();
                           lbl_counttkt.Text = DS.Tables[0].Rows[0]["Refundedamt"].ToString();
                           Label1.Text = DS.Tables[0].Rows[0]["CanBeRefunded"].ToString();                         
                           lblmaxpg.Text = Convert.ToString(DS.Tables[0].Rows[0]["maxpgcharge"]);
                           String Paymentmode = DS.Tables[0].Rows[0]["PgMode"].ToString();
                           if (Paymentmode == "PG ")
                           {
                               adb.Visible = true;
                           }
                           else
                           {
                               adb.Visible = false;
                           }


                         
                           GridRefunddtl.DataSource = DS;
                           GridRefunddtl.DataBind();
                          //ClearInputs(Page.Controls);
                            if (Convert.ToInt32(DS.Tables[0].Rows[0]["CanBeRefunded"]) > 0)
                           { lnkupdate.Visible = true; }
                           else  { lnkupdate.Visible = false; }                 
                       }
                   }
            }

          

        }
         catch (Exception ex)
        {
            Response.Write(ex.Message + "Stack trace" + ex.StackTrace);
        }
    }
    protected void lnkupdate_Click(object sender, System.EventArgs e)
    {
        try
        {
            string str_orderid = txt_OrderId.Text.ToString();
            string maxAmt = Label1.Text.ToString();
            string MaxPgchr = string.IsNullOrEmpty(lblmaxpg.Text) == true || lblmaxpg.Text.ToLower() == "maxpgcharge" ? "0.0" : lblmaxpg.Text;

             Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "window.open('RefundProcessedUpdate.aspx?OrderId=" + str_orderid + "&MaxAmt=" + maxAmt + "&MaxPg=" + MaxPgchr + "','Print','scrollbars=yes,width=938,height=380,top=20,left=150');", true);        
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    void ClearInputs(ControlCollection ctrls)
    {
        try
        {
            foreach (Control ctrl in ctrls)
            {
                if (ctrl is TextBox)
                ((TextBox)ctrl).Text = string.Empty;
                ClearInputs(ctrl.Controls);
            }
        }
        catch (Exception ex)
        {
        }
    }
}
