﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false" CodeFile="Upload.aspx.vb" Inherits="Reports_Accounts_Upload" %>

<%--<%@ Register Src="~/UserControl/RequestControl.ascx" TagPrefix="uc1" TagName="RequestControl" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
    <link href="../../CSS/lytebox.css" rel="stylesheet" type="text/css" />
    <%--<link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    
    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />--%>

    <div class="mtop80"></div>

    <%--<div class="large-2 medium-2 small-12 columns">
        <uc1:RequestControl runat="server" ID="RequestControl" />
            </div>--%>
    <style type="text/css">
        .panel-primary > .panel-heading {
            width: 97.5%;
            color: #fff;
            margin-left: 13px;
            background-color: #f3f6ff;
            border: 1px solid #000;
        }

        .panel {
            border: 1px solid #fefefe;
        }

        h3 {
            color: #032451 !important;
        }
    </style>
    <div class="row">
        <div class="col-md-2"></div>

        <div class="col-md-12" style="margin-top: 20px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Upload > Agency Credit And Debit </h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="large-9 medium-9 small-12 columns end">
                                <div class="large-12 medium-12 small-12 heading">
                                    <div class="clear1"></div>




                                    <table border="0" cellpadding="0" cellspacing="0" style="float: left;" id="tblUploadType" runat="server">
                                        <tr>
                                            <td style="padding: 10px 5px 10px 5px; font-weight: bold; font-family: arial, Helvetica, sans-serif; font-size: 13px;"
                                                width="125px" align="left">Upload Type :
                                            </td>
                                            <td align="left">
                                                <fieldset style="width: 140px;">
                                                    <asp:RadioButtonList ID="RBL_Type" runat="server" AutoPostBack="True" RepeatDirection="Horizontal"
                                                        CellPadding="2" CellSpacing="2" Font-Size="12px" Font-Names="Arial" Width="140px">
                                                    </asp:RadioButtonList>
                                                </fieldset>
                                            </td>
                                        </tr>
                                        <tr id="tr_Cat" runat="server">
                                            <td style="padding: 10px 5px 10px 5px; font-weight: bold; font-family: arial, Helvetica, sans-serif; font-size: 13px;"
                                                align="left">Upload Category :
                                            </td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddl_Category" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>


                                </div>
                                <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                                    <table cellpadding="0" cellspacing="0" style="width: 90%;">

                                        <tr id="tr_search" runat="server" visible="false">

                                            <td valign="top" style="width: 100%;">

                                                <table border="0" cellpadding="0" cellspacing="10" align="center" class="tbltbl" style="width: 100%;">
                                                    <tr>
                                                        <td>Search By Agency
                                                        </td>
                                                        <td style="padding-left: 15px">
                                                            <%--<asp:TextBox ID="txtSearch" runat="server" AutoPostBack="True" Width="160px"></asp:TextBox>--%>
                                                            <input type="text" id="txtAgencyName" name="txtAgencyName" style="width: 290px" onfocus="focusObj(this);"
                                                                onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID" class="form-control" />
                                                            &nbsp;&nbsp;&nbsp;
                                                            <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                                            <asp:Button ID="btn_search" runat="server" CssClass="button buttonBlue" Text="Search" Width="100px" />
                                                        </td>

                                                    </tr>



                                                </table>

                                            </td>

                                        </tr>

                                    </table>

                                    <div class="clear"></div>
                                </div>
                            </div>

                            <div class="clear1"></div>
                            <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                                <table class="gridwidth1 large-12 medium-12 small-12" width="100%">
                                    <tr>
                                        <td style="padding-top: 10px">
                                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="counter"
                                                AllowPaging="True" AllowSorting="True" OnPageIndexChanging="GridView1_PageIndexChanging"
                                                PageSize="25" CssClass="table" Width="100%" Style="background-color: #fff; overflow: auto; max-height: 500px;">
                                                <Columns>
                                                    <asp:BoundField DataField="counter" HeaderText="Sr.No" ReadOnly="True" />


                                                    <asp:TemplateField HeaderText="DEBIT NOTE">
                                                        <ItemTemplate>

                                                            <a target="_blank" href="UploadCredit.aspx?AgentID=<%#Eval("user_id")%>&Action=DEBIT" rel="lyteframe" rev="width: 900px; height: 280px; overflow:hidden;" target="_blank"
                                                                style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #161946">Debit</a>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="CREDIT NOTE">
                                                        <ItemTemplate>
                                                            <a target="_blank" href="UploadCredit.aspx?AgentID=<%#Eval("user_id")%>&Action=CREDIT" rel="lyteframe" rev="width: 900px; height: 280px; overflow:hidden;" target="_blank"
                                                                style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #161946">Credit</a>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Agency Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFirstName" runat="server" Text='<%#Eval("Agency_Name")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="User_Id">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_uid" runat="server" Text='<%#Eval("user_id")%>' Font-Bold="True" ForeColor="#161946"></asp:Label>
                                                            <%-- <a target="_blank" href="UploadCredit.aspx?AgentID=<%#Eval("user_id")%>" rel="lyteframe" rev="width: 900px; height: 280px; overflow:hidden;" target="_blank"
                                                style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;
                                                color: #161946">
                                        <asp:Label ID="lbl_uid" runat="server" Text='<%#Eval("user_id")%>' Font-Bold="True" ForeColor="#161946"></asp:Label></a>
                                                            --%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%--Crd_Limit-AgentLimit as Balance,--%>
                                                    <asp:BoundField DataField="AgencyId" HeaderText="AgencyId" />
                                                    <asp:BoundField DataField="Balance" HeaderText="Balance" />
                                                    <asp:BoundField DataField="DueAmount" HeaderText="Due_Amount" />
                                                    <asp:BoundField DataField="AgentLimit" HeaderText="CreditLimit" />
                                                    <asp:BoundField DataField="Mobile" HeaderText="Mobile" />
                                                    <asp:BoundField DataField="Email" HeaderText="Email" />
                                                    <asp:BoundField DataField="SalesExecID" HeaderText="Sales Agent" />
                                                    <asp:BoundField DataField="Crd_Trns_Date" HeaderText="Transaction Date" />
                                                </Columns>
                                                <RowStyle CssClass="RowStyle" />
                                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                <PagerStyle CssClass="PagerStyle" />
                                                <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                <HeaderStyle CssClass="HeaderStyle" />
                                                <EditRowStyle CssClass="EditRowStyle" />
                                                <AlternatingRowStyle CssClass="AltRowStyle" />
                                            </asp:GridView>
                                        </td>
                                    </tr>

                                </table>
                            </div>
                            <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center">
                                            <div id="register_info">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div class="clear"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>



</asp:Content>

