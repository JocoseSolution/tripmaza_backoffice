﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="Ledger.aspx.vb" Inherits="Reports_Accounts_Ledger" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    <link href="../../css/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />--%>
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .lft {
            float: left;
        }

        .rgt {
            float: right;
        }

        .panel-primary > .panel-heading {
            width: 97.5%;
            color: #fff;
            margin-left: 13px;
            background-color: #f3f6ff;
            border: 1px solid #000;
        }

        .panel {
            border: 1px solid #fefefe;
        }

        h3 {
            color: #032451 !important;
        }
    </style>

    <div class="row">
        <div class="col-md-12" style="margin-top: 20px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Account > Ledger Details</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">From Date</label>
                                        <input type="text" name="From" id="From" readonly="readonly" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">To Date</label>
                                        <input type="text" name="To" id="To" readonly="readonly" class="form-control" />
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group" id="tr_Agency" runat="server">
                                        <label for="exampleInputEmail1">Agency</label>
                                        <span id="tr_AgencyName" runat="server">
                                            <input type="text" id="txtAgencyName" name="txtAgencyName" onfocus="focusObj(this);"
                                                onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID" class="form-control" />
                                            <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" /></span>
                                    </div>
                                </div>




                            </div>



                            <div class="row">
                                <div class="col-md-4" style="display: none;">
                                    <div class="form-group" id="tr_Cat" runat="server">
                                        <label for="exampleInputEmail1">Upload Category </label>
                                        <asp:DropDownList ID="ddl_Category" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group" id="tr_UploadType" runat="server">
                                        <label for="exampleInputEmail1" id="lblUpType" runat="server">Upload Type</label>
                                        <asp:RadioButtonList ID="RBL_Type" runat="server" AutoPostBack="True" RepeatDirection="Horizontal"
                                            CssClass="form-control">
                                        </asp:RadioButtonList>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group" id="tr_SearchType" runat="server" visible="false">
                                        <label for="exampleInputEmail1">Search Type</label>
                                        <asp:RadioButton CssClass="form-control" ID="RB_Agent" runat="server" Checked="true" GroupName="Trip" onclick="Show(this)"
                                            Text="Agent" />
                                        <asp:RadioButton ID="RB_Distr" CssClass="form-control" runat="server" GroupName="Trip" onclick="Hide(this)"
                                            Text="Own" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group" id="tr_BookingType" runat="server">
                                    <div class="col-md-4">
                                        <label for="exampleInputEmail1" id="lblBookType" runat="server">Booking Type</label>
                                        <asp:DropDownList ID="ddl_BookingType" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>


                                </div>

                                <div class="col-md-4 ">
                                    <label for="exampleInputEmail1">Trans Type :</label>
                                    <asp:DropDownList CssClass="form-control" ID="ddlTransType" runat="server">
                                    </asp:DropDownList>
                                </div>


                                <div class="col-md-4 " style="display: none;">
                                    <label for="exampleInputEmail1">PaymentMode :</label>
                                    <asp:DropDownList CssClass="form-control" ID="txtPaymentmode" runat="server">
                                        <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                        <asp:ListItem Text="PG" Value="pg"></asp:ListItem>
                                        <asp:ListItem Text="Wallet" Value="wallet"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="row ">
                                <div class="col-md-4 pull-right">

                                    <div class="w40 lft">
                                        <asp:Button ID="btn_search" runat="server" Text="Search" CssClass="button buttonBlue" />

                                    </div>
                                    <div class="w20 lft">&nbsp;</div>
                                    <div class="w40 lft">
                                        <asp:Button ID="btn_export" runat="server" Text="Export" CssClass="button buttonBlue" />
                                    </div>
                                </div>

                                <div class="col-md-3">
                                </div>

                            </div>

                        </div>
                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="row" style="background-color: #fff; overflow-y: scroll;" runat="server" visible="true">
                                <div class="col-md-12">

                                    <asp:UpdatePanel ID="up" runat="server">
                                        <ContentTemplate>
                                            <asp:GridView ID="Grid_Ledger" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                CssClass="table" PageSize="30">

                                                <Columns>
                                                    <asp:BoundField DataField="AgencyId" HeaderText="AgencyId" />
                                                    <asp:BoundField DataField="AgentID" HeaderText="User_ID" />
                                                    <asp:BoundField DataField="AgencyName" HeaderText="Agency_Name" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger" />
                                                    <asp:BoundField HeaderText="Order No" DataField="InvoiceNo"></asp:BoundField>
                                                    <asp:BoundField HeaderText="Pnr" DataField="PnrNo" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                                    <asp:BoundField HeaderText="Aircode" DataField="Aircode" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                                    <asp:BoundField HeaderText="TicketNo" DataField="TicketNo" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                                    <%--<asp:BoundField HeaderText="Easy ID" DataField="YatraAccountID" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>--%>
                                                    <asp:BoundField HeaderText="DR." DataField="Debit" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                                    <asp:BoundField HeaderText="CR." DataField="Credit" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                                    <asp:BoundField HeaderText="Balance" DataField="Aval_Balance"></asp:BoundField>
                                                    <asp:BoundField HeaderText="Booking Type" DataField="BookingType" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                                    <asp:BoundField HeaderText="Created Date" DataField="CreatedDate" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                                    <asp:BoundField HeaderText="Remark" DataField="Remark" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>

                                                    <asp:BoundField HeaderText="DueAmount" DataField="DueAmount" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                                    <asp:BoundField HeaderText="CreditLimit" DataField="CreditLimit" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>

                                                    <%-- <asp:BoundField HeaderText="Payment Mode" DataField="PaymentMode" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>--%>
                                                </Columns>
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Distributor.js") %>"></script>
</asp:Content>
