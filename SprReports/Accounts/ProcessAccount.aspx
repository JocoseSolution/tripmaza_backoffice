﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="ProcessAccount.aspx.vb" Inherits="Reports_Accounts_ProcessAccount" %>

<%--<%@ Register Src="~/UserControl/RequestControl.ascx" TagPrefix="uc1" TagName="RequestControl" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style type="text/css">
        .panel-primary > .panel-heading {
            width: 97.5%;
            color: #fff;
            margin-left: 13px;
            background-color: #f3f6ff;
            border: 1px solid #000;
        }

        .panel {
            border: 1px solid #fefefe;
        }

        h3 {
            color: #032451 !important;
        }
    </style>
    <div class="row">
        <%--<div class="col-md-2"></div>--%>

        <div class="col-md-12" style="margin-top: 20px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Upload > Accepted Upload Request </h3>
                    </div>
                    <div class="panel-body">
                        <div class="large-9 medium-9 small-12 columns end">
                            <div class="large-12 medium-12 small-12 heading">
                                <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                                    <%--  <div class="large-12 medium-12 small-12 heading1">Search Record By Type And Category</div>--%>
                                    <div class="clear1"></div>
                                    <table border="0" cellpadding="0" cellspacing="0" style="float: left; width: 45%;" id="tableFilter" runat="server">
                                        <tr>
                                            <td style="padding: 10px 5px 10px 5px; font-weight: bold; font-family: arial, Helvetica, sans-serif; font-size: 13px;"
                                                width="125px" align="left">Upload Type :
                                            </td>
                                            <td align="left">
                                                <fieldset style="border: thin solid #004b91; width: 140px;">
                                                    <asp:RadioButtonList ID="RBL_Type" runat="server" AutoPostBack="True" RepeatDirection="Horizontal"
                                                        CellPadding="2" CellSpacing="2" Font-Size="12px" Font-Names="Arial" Width="140px">
                                                    </asp:RadioButtonList>
                                                </fieldset>
                                            </td>
                                        </tr>
                                        <tr id="tr_Cat" runat="server">
                                            <td style="padding: 10px 5px 10px 5px; font-weight: bold; font-family: arial, Helvetica, sans-serif; font-size: 13px;"
                                                align="left">Upload Category :
                                            </td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddl_Category" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                                    <table cellpadding="0" cellspacing="0" style="float: right; width: 55%">
                                        <tr>
                                            <td width="190px" valign="top">Search By Agency Name
                                            </td>
                                            <td width="290px" style="padding-bottom: 3px" valign="bottom">
                                                <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="True" Width="290px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td id="td_Reject" runat="server" visible="false" valign="top" colspan="2">

                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td width="190px" valign="top">Submit Comment</td>

                                                        <td align="center" style="padding-top: 10px">
                                                            <asp:TextBox ID="txt_Reject" runat="server" TextMode="MultiLine" Height="60px" Width="350px"
                                                                BackColor="#FFFFCC"></asp:TextBox><br />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="float: right">
                                                            <asp:Button ID="btn_Submit" runat="server" Text="Submit" />&nbsp;&nbsp;
                                                                                            <asp:Button ID="btn_Cancel" runat="server" Text="Cancle" />
                                                        </td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>

                                    </table>
                                </div>
                                <div class="clear"></div>

                            </div>
                            <div class="clear1"></div>
                            <div class="clear1"></div>
                            <div class="row">
                                <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                                    <table class="gridwidth1 large-12 medium-12 small-12" style="background-color: #ffffff">

                                        <tr>

                                            <td>
                                                <asp:GridView ID="grd_accdeposit" runat="server" AutoGenerateColumns="false" OnRowCommand="grd_accdeposit_RowCommand"
                                                    OnRowDataBound="grd_accdeposit_RowDataBound" CssClass="table">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="&nbsp;&nbsp;ID&nbsp;&nbsp">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblID" runat="server" Text='<%# Eval("Counter") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="200px" />
                                                        </asp:TemplateField>
                                                        <%--   <asp:BoundField HeaderText="&nbsp;&nbsp;ID&nbsp;&nbsp;" DataField="Counter" />--%>
                                                        <asp:TemplateField HeaderText="Agency Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_agencyid" runat="server" Text='<%# Eval("AgencyName").ToString() %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%-- <asp:BoundField HeaderText="AgencyID" DataField="AgencyID" />--%>
                                                        <asp:TemplateField HeaderText="User_Id">
                                                            <ItemTemplate>

                                                                <div>
                                                                    <%--<a href="UploadCredit.aspx?AgentID=<%#Eval("AgencyID")%>&ID=<%#Eval("Counter")%>&Amount=<%#Eval("Amount")%>#lightbox">
                                                                            <asp:Label ID="lbl_uid" runat="server" Text='<%#Eval("AgencyID")%>' Font-Bold="True"
                                                                                ForeColor="#004b91"></asp:Label></a>--%>
                                                                    <a href='UploadCredit.aspx?AgentID=<%#Eval("AgencyID")%>&ID=<%#Eval("Counter")%>&Amount=<%#Eval("Amount")%>' rel="lyteframe"
                                                                        rev="width: 900px; height: 300px; overflow:hidden;" target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                                        <asp:Label ID="lbl_uid" runat="server" Text='<%#Eval("AgencyID")%>'></asp:Label></a>
                                                                    <%--                                                                                                                    <a href='../../OtpValidate.aspx?AgentID=<%#Eval("AgencyID")%>&ID=<%#Eval("Counter")%>&Amount=<%#Eval("Amount")%>' rel="lyteframe"
                                                                rev="width: 900px; height: 300px; overflow:hidden;" target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                                <asp:Label ID="OrderID" runat="server" Text='<%#Eval("AgencyID")%>'></asp:Label></a>--%>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField HeaderText="AgencyID" DataField="RegAgencyID" />
                                                        <asp:BoundField HeaderText="Amount" DataField="Amount" />
                                                        <asp:BoundField HeaderText="ModeOfPayment" DataField="ModeOfPayment" />
                                                        <asp:BoundField HeaderText="Bank Name" DataField="BankName" />
                                                        <asp:BoundField HeaderText="ChequeNo" DataField="ChequeNo" />
                                                        <asp:BoundField HeaderText="ChequeDate" DataField="ChequeDate" />
                                                        <asp:BoundField HeaderText="TransactionID" DataField="TransactionID" />
                                                        <asp:BoundField HeaderText="ReferenceNo" DataField="BankAreaCode" />
                                                        <asp:BoundField HeaderText="Deposit City" DataField="DepositCity" />
                                                        <asp:BoundField HeaderText="Remark" DataField="Remark" />
                                                        <asp:BoundField HeaderText="Status" DataField="Status" />
                                                        <asp:BoundField HeaderText="Date" DataField="Date" />
                                                        <asp:TemplateField HeaderText="Reject">
                                                            <ItemTemplate>
                                                                <%--<asp:LinkButton ID="lnkaccept" runat="server" Text="Accept" CommandName="accept"
                                                    CommandArgument='<%#Eval("AgencyID") %>'></asp:LinkButton>/--%>
                                                                <asp:LinkButton ID="lnkcancel" runat="server" CommandName="reject" CommandArgument='<%#Eval("Counter") %>'
                                                                    ForeColor="Red" Font-Bold="True">Reject</asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:BoundField HeaderText="Date" DataField="Date" />
    <asp:BoundField HeaderText="Date" DataField="Date" />
    <asp:BoundField HeaderText="Date" DataField="Date" />
    <asp:BoundField HeaderText="Date" DataField="Date" />--%>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>

                                        </tr>

                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>



</asp:Content>
