<%@ Page Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="UploadAmountDetails.aspx.vb" Inherits="Reports_Agent_UploadAmountDetails"
    Title="Upload Amount Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .luna-navs li a {
            color: #fff;
            padding: 15px 15px !important;
            cursor: pointer;
            font-size: 14px !important;
        }

        .panel-primary > .panel-heading {
            width: 97.5%;
            color: #fff;
            margin-left: 13px;
            background-color: #f3f6ff;
            border: 1px solid #000;
        }

        .panel {
            border: 1px solid #fefefe;
        }

        h3 {
            color: #032451 !important;
        }
    </style>

   <%-- <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />--%>
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
    <div class="row">
        <div class="col-md-2"></div>

        <div class="col-md-12" style="margin-top: 20px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Upload Amount Report </h3>
                    </div>
                    <div class="panel-body" style="overflow: scroll;">
                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <table class="w90 auto boxshadow">
                                <tr>
                                    <td class="clear1" colspan="4"></td>
                                </tr>
                                <tr>
                                    <td align="left" class="fltdtls" width="100" style="height: 30px">From Date:
                                    </td>
                                    <td align="left" style="height: 30px">
                                        <input type="text" name="From" id="From" class="txtCalander form-control" readonly="readonly"
                                            style="width: 300px" />
                                        <br />
                                    </td>
                                    <td align="left" class="fltdtls" style="height: 30px" width="100">To Date:
                                    </td>
                                    <td align="left" style="height: 30px">
                                        <input type="text" name="To" id="To" class="txtCalander form-control" readonly="readonly" style="width: 300px" />
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="fltdtls" width="100" style="height: 30px">Payment Mode :
                                    </td>
                                    <td align="left" style="height: 30px">

                                        <asp:DropDownList ID="ddl_PType" runat="server" Width="300px" CssClass="form-control">
                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                            <asp:ListItem Text="Cash" Value="Cash"></asp:ListItem>
                                            <asp:ListItem Text="Cash Deposite In Bank" Value="Cash Deposite In Bank"></asp:ListItem>
                                            <asp:ListItem Text="NetBanking" Value="NetBanking"></asp:ListItem>
                                            <asp:ListItem Text="RTGS" Value="RTGS"></asp:ListItem>
                                        </asp:DropDownList>
                                        <br />
                                    </td>
                                    <td class="fltdtls">Status</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddl_status" runat="server" Width="300px" CssClass="form-control">
                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                            <asp:ListItem Text="Pending" Value="Pending"></asp:ListItem>
                                            <asp:ListItem Text="InProcess" Value="InProcess"></asp:ListItem>
                                            <asp:ListItem Text="Confirm" Value="Confirm"></asp:ListItem>
                                            <asp:ListItem Text="Rejected" Value="Rejected"></asp:ListItem>

                                        </asp:DropDownList>
                                        <br />
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="4" id="td_Agency" runat="server">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td style="width: 50%;">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="td_ag">
                                                        <tr>
                                                            <td align="left" class="fltdtls" width="100" style="height: 30px">Agency:
                                                            </td>
                                                            <td align="left" style="height: 30px" colspan="1">

                                                                <input type="text" id="txtAgencyName" name="txtAgencyName" style="width: 300px" onfocus="focusObj(this);"
                                                                    onblur="blurObj(this);" defvalue="Agency Name or ID"
                                                                    value="Agency Name or ID" class="form-control" />
                                                                <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />

                                                            </td>
                                                        </tr>

                                                    </table>
                                                </td>

                                                <td class="fltdtls" width="100">Upload Type</td>
                                                <td>
                                                    <asp:DropDownList ID="DropDownListADJ" runat="server" Width="290px" CssClass="form-control">
                                                        <asp:ListItem Text="--Select--" Value="Select" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="Fresh Upload" Value="FU"></asp:ListItem>
                                                        <asp:ListItem Text="Adjustment" Value="AD"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <%--<td height="30px" style="padding-top: 10px; padding-bottom: 10px;">
                                &nbsp;
                            </td>--%>
                                    <td id="tr_SearchType" runat="server" visible="false" colspan="4">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="fltdtls"
                                                    align="left">Search Type</td>
                                                <td style="width: 200px">
                                                    <asp:RadioButton ID="RB_Agent" runat="server" Checked="true" GroupName="Trip" onclick="Show(this)"
                                                        Text="Agent" />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                            <asp:RadioButton ID="RB_Distr" runat="server" GroupName="Trip" onclick="Hide(this)"
                                                                                Text="Own" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="4" align="right">
                                        <asp:Button ID="btn_showdetails" runat="server" CssClass="button buttonBlue rgt" Text="Search" Width="200px" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <table class="w70 auto">
                                <tr>
                                    <td style="padding-top: 10px" align="center">
                                        <asp:GridView ID="grd_deposit" CssClass="table" runat="server" AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:BoundField HeaderText="Transaction&nbsp;Date" DataField="Date" />
                                                <asp:BoundField HeaderText="Agency&nbsp;Name" DataField="AgencyName" />
                                                <asp:BoundField HeaderText="AgencyId" DataField="AgencyID" />
                                                <asp:BoundField HeaderText="Amount" DataField="Amount" />
                                                <asp:BoundField HeaderText="ModeOfPayment" DataField="ModeOfPayment" />
                                                <asp:BoundField HeaderText="Bank&nbsp;Name" DataField="BankName" />
                                                <asp:BoundField HeaderText="ChequeNo" DataField="ChequeNo" />
                                                <asp:BoundField HeaderText="Cheque&nbsp;Date" DataField="ChequeDate" />
                                                <asp:BoundField HeaderText="TransactionId" DataField="TransactionID" />
                                                <asp:BoundField HeaderText="BankAreaCode" DataField="BankAreaCode" />
                                                <asp:BoundField HeaderText="Deposit&nbsp;City" DataField="DepositCity" />
                                                <asp:BoundField HeaderText="Deposite&nbsp;Office" DataField="DepositeOffice" />
                                                <asp:BoundField HeaderText="Concern&nbsp;Person" DataField="ConcernPerson" />
                                                <asp:BoundField HeaderText="Reciept&nbsp;No" DataField="RecieptNo" />
                                                <asp:BoundField HeaderText="Remark" DataField="Remark" />
                                                <asp:BoundField HeaderText="Status" DataField="Status" />
                                                <asp:BoundField HeaderText="Remark&nbsp;By&nbsp;Account" DataField="RemarkByAccounts" />
                                                <asp:BoundField HeaderText="Account&nbsp;Id" DataField="AccountID" />
                                                <%--<asp:BoundField HeaderText="Date" DataField="Date" />
    <asp:BoundField HeaderText="Date" DataField="Date" />--%>
                                            </Columns>

                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Distributor.js") %>"></script>

</asp:Content>
