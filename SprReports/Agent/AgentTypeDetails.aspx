﻿<%--<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AgentTypeDetails.aspx.vb" Inherits="SprReports_Agent_AgentTypeDetails" %>--%>

<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false" CodeFile="AgentTypeDetails.aspx.vb" Inherits="SprReports_Agent_AgentTypeDetails" %>

<%@ Register Src="~/UserControl/Settings.ascx" TagPrefix="uc1" TagName="Settings" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div class="mtop80"></div>
    <div class="large-3 medium-3 small-12 columns">
       
            
    </div>

   <div class="large-9 medium-9 small-12 columns">
       
        <div class="large-12 medium-12 small-12 heading">
             <div class="large-12 medium-12 small-12 heading1">Group Type Details
            </div>
            <div class="clear1"></div>
        <asp:Label ID="LableMsg" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label>
        <div class="large-12 medium-12 small-12">
           <div class="large-6 medium-6 small-12 columns">
                <div class="large-2 medium-3 small-3 columns">Agent Type</div>
                <div class="large-6 medium-6 small-9  columns">
                    <asp:TextBox ID="TextBoxGroupType" runat="server"></asp:TextBox></div>
               <div class="clear"></div>
                <div class="large-12 medium-12 small-12  columns">
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorType" runat="server" ErrorMessage="Group Type is Required." ControlToValidate="TextBoxGroupType" ValidationGroup="ins"></asp:RequiredFieldValidator> </div>
                </div>
             <div class="large-6 medium-6 small-12 columns">
                <div class="large-2 medium-3 small-3  columns">Description</div>
                <div class="large-6 medium-6 small-9  columns">
                    <asp:TextBox ID="TextBoxDesc" runat="server" TextMode="MultiLine"></asp:TextBox>
                </div>
            <div class="clear"></div>
                <div class="large-12 medium-12 small-12 columns">
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDesc" runat="server" ErrorMessage="Description is Required."  ControlToValidate="TextBoxDesc"  ValidationGroup="ins"></asp:RequiredFieldValidator></div>
                </div>
                  <div class="clear"></div>
           
                <div class="large-2 medium-3 small-6 large-push-10 medium-push-9 small-push6">
                    <asp:Button runat="server" ID="ButtonSubmit" Text="Add Group Type"  ValidationGroup="ins" />
                </div>
                


        </div>

            <div class="clear1"></div>
</div>
       <div class="clear1"></div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="counter"
            OnRowCancelingEdit="GridView1_RowCancelingEdit" 
            OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" PageSize="8"
            CssClass="GridViewStyle" Width="100%">
            <Columns>
                <asp:CommandField  ShowEditButton="True" />
                <asp:TemplateField HeaderText="Group Type">

                    <ItemTemplate>
                        <asp:Label ID="LableGroupType" runat="server" Text='<%# Eval("GroupType")%>'></asp:Label>
                    </ItemTemplate>
                                       
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Description">
                    <ItemTemplate>
                        <asp:Label ID="LableDesc" runat="server" Text='<%# Eval("Text")%>'></asp:Label>
                    </ItemTemplate>
                     <EditItemTemplate>
                        <asp:TextBox ID="TextBoxDesc" runat="server" Text='<%# Eval("Text")%>'></asp:TextBox>
                    </EditItemTemplate>
                      
                   
                </asp:TemplateField>

               <asp:CommandField ShowDeleteButton="True" />
               
            </Columns>
            <RowStyle CssClass="RowStyle" />
            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
            <PagerStyle CssClass="PagerStyle" />
            <SelectedRowStyle CssClass="SelectedRowStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <EditRowStyle CssClass="EditRowStyle" />
            <AlternatingRowStyle CssClass="AltRowStyle" />
        </asp:GridView>

    </div>

</asp:Content--%>




<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .panel-primary > .panel-heading {
            width: 97.5%;
            color: #fff;
            margin-left: 13px;
            background-color: #f3f6ff;
            border: 1px solid #000;
        }

        .panel {
            border: 1px solid #fefefe;
        }

        h3 {
            color: #032451 !important;
        }
    </style>
    <div class="row">
        <div class="col-md-12" style="margin-top: 20px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Register > Group Type Details</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">

                            <div class="row">
                                <div class="col-md-4">

                                    <div class="form-group">

                                        <label for="exampleInputPassword1">Agent Type</label>
                                        <asp:TextBox CssClass="form-control" runat="server" ID="TextBoxGroupType" oncopy="return false" onpaste="return false" MaxLength="20" onkeypress="return keyRestrict(event,'abcdefghijklmnopqrstuvwxyz0123456789');"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorType" runat="server" ErrorMessage="Group Type is Required." ControlToValidate="TextBoxGroupType" ValidationGroup="ins"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Description </label>
                                        <asp:TextBox CssClass="form-control" runat="server" ID="TextBoxDesc" TextMode="MultiLine"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorDesc" runat="server" ErrorMessage="Description is Required." ControlToValidate="TextBoxDesc" ValidationGroup="ins"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="TextBoxDesc" ID="RegularExpressionValidator4" ValidationExpression="^[\s\S]{1,30}$" runat="server" ValidationGroup="ins" ErrorMessage="Maximum 50 characters Use"></asp:RegularExpressionValidator>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">

                                        <asp:Button CssClass="button buttonBlue" ID="ButtonSubmit" runat="server" Text="Add GroupType" ValidationGroup="ins" />
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <asp:Label ID="LableMsg" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="col-md-12">
                                <asp:UpdatePanel runat="server">

                                    <ContentTemplate>
                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="counter"
                                            OnRowCancelingEdit="GridView1_RowCancelingEdit"
                                            OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" PageSize="8"
                                            CssClass="table" GridLines="None" Width="100%">
                                            <Columns>
                                                <asp:CommandField ShowEditButton="True" ValidationGroup="VG" />
                                                <asp:TemplateField HeaderText="Group Type">

                                                    <ItemTemplate>
                                                        <asp:Label ID="LableGroupType" runat="server" Text='<%# Eval("GroupType")%>'></asp:Label>
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LableDesc" runat="server" Text='<%# Eval("Text")%>' Style="text-wrap: inherit;"></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBoxDesc" TextMode="MultiLine" runat="server" Columns="100" Rows="2" Text='<%# Eval("Text")%>'></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorType" runat="server" ErrorMessage="Description is Required." ControlToValidate="TextBoxDesc" ValidationGroup="VG"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator runat="server" ID="valInput" ControlToValidate="TextBoxDesc" ValidationExpression="^[\s\S]{0,500}$" ErrorMessage="Please enter a maximum of 500 characters"></asp:RegularExpressionValidator>
                                                    </EditItemTemplate>


                                                </asp:TemplateField>

                                                <asp:CommandField ShowDeleteButton="True" />

                                            </Columns>
                                        </asp:GridView>


                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#ctl00_ContentPlaceHolder1_TextBoxGroupType").click(function () {

                $("#ctl00_ContentPlaceHolder1_LableMsg").hide();


            });
            $("#ctl00_ContentPlaceHolder1_TextBoxDesc").click(function () {

                $("#ctl00_ContentPlaceHolder1_LableMsg").hide();
            });
        });


        function getKeyCode(e) {
            if (window.event)
                return window.event.keyCode;
            else if (e)
                return e.which;
            else
                return null;
        }
        function keyRestrict(e, validchars) {
            var key = '', keychar = '';
            key = getKeyCode(e);
            if (key == null) return true;
            keychar = String.fromCharCode(key);
            keychar = keychar.toLowerCase();
            validchars = validchars.toLowerCase();
            if (validchars.indexOf(keychar) != -1)
                return true;
            if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                return true;
            return false;
        }

    </script>
</asp:Content>
