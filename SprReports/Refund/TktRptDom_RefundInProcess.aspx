﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="TktRptDom_RefundInProcess.aspx.vb" Inherits="Reports_Refund_TktRptDom_RefundInProcess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">

        function Validate() {
            if (document.getElementById("ctl00_ContentPlaceHolder1_InProccess_grdview_ctl02_txtRemark").value == "") {

                alert('Remark can not be blank,Please Fill Remark');
                document.getElementById("ctl00_ContentPlaceHolder1_InProccess_grdview_ctl02_txtRemark").focus();
                return false;
            }
            if (confirm("Are you sure you want to Reject!"))
                return true;
            return false;
        }
    </script>
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow-y: scroll;
        }

        .panel-primary > .panel-heading {
            width: 97.5%;
            color: #fff;
            margin-left: 13px;
            background-color: #f3f6ff;
            border: 1px solid #000;
        }

        .panel {
            border: 1px solid #fefefe;
        }

        h3 {
            color: #032451 !important;
        }
    </style>
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-12" style="margin-top: 20px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight > Refund Request to Update </h3>
                    </div>

                    <div class="panel-body">
                        <div class="col-sm-12" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:TextBox ID="txt_PNR" runat="server" placeholder="Enter Pnr.." CssClass="theme-search-area-section-input"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>




                                <div class="col-md-3">
                                    <br />
                                    <div class="form-group">
                                        <asp:Button ID="btn_result" runat="server" CssClass="button buttonBlue" Text="Search Result" />
                                    </div>
                                </div>


                            </div>
                        </div>

                        <div class="col-sm-12" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="row" id="divReport" style="background-color: #fff; overflow-y: scroll; overflow-x: scroll; max-height: 500px;" runat="server" visible="True">
                                <div class="col-md-28">
                                    <asp:GridView ID="InProccess_grdview" runat="server" AllowPaging="True" AllowSorting="True" OnPageIndexChanging="GridView1_PageIndexChanging"
                                        AutoGenerateColumns="False" CssClass="table" GridLines="None" PageSize="10">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Agent ID">
                                                <ItemTemplate>
                                                    <asp:Label ID="AgentID" runat="server" Text='<%#Eval("UserID")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Agency Name" DataField="Agency_Name"></asp:BoundField>
                                            <asp:TemplateField HeaderText="Order Id">
                                                <ItemTemplate>
                                                    <%-- <asp:Label ID="OrderID" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label>--%>
                                                    <a id="ancher" href='../PnrSummaryIntl.aspx?OrderId=<%#Eval("OrderId")%>' target="_blank"
                                                        style="font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #004b91; font-weight: bold;">
                                                        <asp:Label ID="OrderID" runat="server" Text='<%#Eval("OrderId") %>'></asp:Label>(View)</a>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Executive ID">
                                                <ItemTemplate>
                                                    <asp:Label ID="ExId" runat="server" Text='<%#Eval("ExecutiveID")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Pax ID">
                                                <ItemTemplate>
                                                    <asp:Label ID="TID" runat="server" Text='<%#Eval("Counter")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Pax Type">
                                                <ItemTemplate>
                                                    <asp:Label ID="PaxType" runat="server" Text='<%#Eval("pax_type")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Pax Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="PaxName" runat="server" Text='<%#(Eval("Title").ToString()+" " + Eval("pax_fname").ToString()+" " + Eval("pax_lname").ToString()).ToUpper()%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PNR">
                                                <ItemTemplate>
                                                    <asp:Label ID="GdsPNR" runat="server" Text='<%#Eval("pnr_locator")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Ticket No">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkupdate" runat="server" Text='<%#Eval("Tkt_No") %>' ForeColor="Red"
                                                        Font-Bold="true" Font-Size="11px" CommandName="lnkupdate" CommandArgument='<%#Eval("Counter")%>'
                                                        OnClick="lnkupdate_Click"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Airline" DataField="VC"></asp:BoundField>
                                            <asp:BoundField HeaderText="SECTOR" DataField="Sector"></asp:BoundField>
                                            <asp:TemplateField HeaderText="Total Fare">
                                                <ItemTemplate>
                                                    <a id="ancher1" href='TktRptIntl_RefundFair.aspx?Counter=<%#Eval("Counter") %>' onclick="wopen('', 'popup',400, 300); return false;"
                                                        rel="lyteframe" rev="width: 300px; height: 210px; overflow:hidden;" style="font-family: arial, Helvetica, sans-serif; font-size: 12px; color: Red; font-weight: bold;">
                                                        <asp:Label ID="lbltotalfare" runat="server" Text='<%#Eval("TotalFare") %>'></asp:Label></a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Net Fare" DataField="TotalFareAfterDiscount">
                                                <ItemStyle HorizontalAlign="center"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Journey Date" DataField="Journey_Date"></asp:BoundField>
                                            <asp:BoundField HeaderText="Partner Name" DataField="PartnerName"></asp:BoundField>
                                            <asp:BoundField HeaderText="Refund Status" DataField="Status"></asp:BoundField>
                                            <asp:BoundField HeaderText="Request Date" DataField="SubmitDate"></asp:BoundField>
                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnlreject" runat="server" Text="Reject" ForeColor="Red" CommandName="reject"
                                                        CommandArgument='<%#Eval("Counter") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Reject Remark">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtRemark" runat="server" Height="47px" TextMode="MultiLine" Width="200px"
                                                        Visible="false"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ControlStyle-Width="100px">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkSubmit" runat="server" OnClick="btnCanFee_Click" OnClientClick="return Validate();" Visible="false"
                                                        CommandArgument='<%# Eval("Counter") %>' CommandName="submit"><img src="../../Images/Submit.png" alt="Ok" /></asp:LinkButton><br />
                                                    <asp:LinkButton ID="lnkHides" runat="server" OnClick="lnkHides_Click" Visible="false"
                                                        CommandName="lnkHides" CommandArgument='<%# Eval("Counter") %>'><img src="../../Images/Cancel.png" alt="Cancel" /></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Customer Remarks" ControlStyle-Width="200px">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRegardingCancel" runat="server" Text='<%#Eval("RegardingCancel")%>'></asp:Label></a>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Payment Mode" ControlStyle-Width="200px">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPayment_Mode" runat="server" Text='<%#Eval("Payment_Mode")%>'></asp:Label></a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="PgCharges" HeaderText="Convenience Fee" />
                                            <asp:BoundField DataField="CancelStatus" HeaderText="Cancel Status" />
                                        </Columns>

                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</asp:Content>
