﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CancellationReport.aspx.vb" Inherits="SprReports_Refund_CancellationReport" MasterPageFile="~/MasterAfterLogin.master" %>

<%@ Register Src="~/UserControl/LeftMenu.ascx" TagPrefix="uc1" TagName="LeftMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />


    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow: auto;
        }

        .panel-primary > .panel-heading {
            width: 97.5%;
            color: #fff;
            margin-left: 13px;
            background-color: #f3f6ff;
            border: 1px solid #000;
        }

        .panel {
            border: 1px solid #fefefe;
        }

        h3 {
            color: #032451 !important;
        }
    </style>

    <div class="row">
        <div class="col-md-12" style="margin-top: 20px;">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight > Flight Refund Reports</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12 form-group" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <input type="text" name="From" id="From" placeholder="From Date.." class="theme-search-area-section-input" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <input type="text" name="To" id="To" placeholder="To Date.." class="theme-search-area-section-input" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:TextBox ID="txt_PNR" runat="server" placeholder="Enter Pnr.." CssClass="theme-search-area-section-input"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:TextBox ID="txt_OrderId" runat="server" placeholder="Enter Orderid.." CssClass="theme-search-area-section-input"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:TextBox ID="txt_PaxName" runat="server" placeholder="Enter Pax Name.." CssClass="theme-search-area-section-input"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:TextBox ID="txt_TktNo" runat="server" placeholder="Enter Tkt No.." CssClass="theme-search-area-section-input"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div id="td_Agency" runat="server">
                                    <div id="tr_ExecID" runat="server" class="form-group">
                                        <div class="col-md-4">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner">
                                                    <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                                    <asp:DropDownList ID="ddl_ExecID" runat="server" CssClass="theme-search-area-section-input">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner">
                                                    <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                                    <input type="text" id="txtAgencyName" name="txtAgencyName" onfocus="focusObj(this);"
                                                        onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID" class="theme-search-area-section-input" />
                                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner">
                                                    <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                                    <asp:DropDownList ID="ddl_Status" runat="server" class="theme-search-area-section-input">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>




                            <div class="row">
                                <div class="col-md-4">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:TextBox ID="txt_AirPNR" runat="server" placeholder="Enter Air Line.." CssClass="theme-search-area-section-input"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4" id="divPartnerName" runat="server">
                                    <div class="theme-search-area-section theme-search-area-section-line">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                            <asp:DropDownList CssClass="theme-search-area-section-input" ID="txtPartnerName" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="theme-search-area-section theme-search-area-section-line" id="tdTripNonExec1" runat="server">
                                            <div class="theme-search-area-section-inner">
                                                <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                                <asp:DropDownList ID="ddlTripRefunDomIntl" CssClass="theme-search-area-section-input" runat="server">
                                                    <asp:ListItem Value="">-----Select-----</asp:ListItem>
                                                    <asp:ListItem Value="D">Domestic</asp:ListItem>
                                                    <asp:ListItem Value="I">International</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="theme-search-area-section theme-search-area-section-line" id="Div1" runat="server">
                                            <div class="theme-search-area-section-inner">
                                                <i class="theme-search-area-section-icon icofont-airplane-alt"></i>
                                                <asp:DropDownList ID="ddldatefilter" CssClass="theme-search-area-section-input" runat="server">
                                                    <asp:ListItem Value="U">Updated Date</asp:ListItem>
                                                    <asp:ListItem Value="R">Requested Date</asp:ListItem>
                                                    <asp:ListItem Value="A">Accepted Date</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        </div>
                                        <%--   <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Accept Date</label>
                                    <input type="text" name="acceptdate" id="acceptdate" class="form-control" readonly="readonly" />
                                </div>
                                </div>--%>
                                    </div>





                                    <div class="row">
                                        <%--  <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Update Date</label>
                                    <input type="text" name="updatedate" id="updatedate" class="form-control" readonly="readonly" />
                                </div>
                                </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Requested Date</label>
                                    <input type="text" name="requestdate" id="requestdate" class="form-control" readonly="readonly" />
                                </div>
                                </div>--%>
                                        <div class="col-md-4" id="divPaymentMode" runat="server">
                                            <label for="exampleInputEmail1">PaymentMode :</label>
                                            <asp:DropDownList CssClass="form-control" ID="txtPaymentmode" runat="server">
                                                <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                                <asp:ListItem Text="PG" Value="pg"></asp:ListItem>
                                                <asp:ListItem Text="Wallet" Value="wallet"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>




                                    </div>


                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <asp:Button ID="btn_result" runat="server" CssClass="button buttonBlue" Text="Search Result" />
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <asp:Button ID="btn_export" runat="server" CssClass="button buttonBlue" Text="Export" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <div style="color: #FF0000">
                                                    * N.B: To get Today's booking without above parameter,do not fill any field, only
                                click on search your booking.
                                                </div>
                                            </div>
                                        </div>
                                    </div>




                                </div>
                                <div class="col-sm-12" style="background: #fbfbfb; padding: 10px; box-shadow: 0px 0px 4px #ccc;">
                                    <div class="row" id="divReport" runat="server" visible="false">
                                        <div class="col-md-12">
                                            <asp:UpdatePanel ID="UP" runat="server" style="background-color: #fff; overflow: auto; max-height: 500px;">
                                                <ContentTemplate>
                                                    <asp:GridView ID="grd_report" runat="server" BackColor="White" AutoGenerateColumns="False" Width="100%" CssClass="table" GridLines="None"
                                                        AllowPaging="true" PageSize="30">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Order Id">
                                                                <ItemTemplate>

                                                                    <a id="ancher" href='../PnrSummaryIntl.aspx?OrderId=<%#Eval("OrderId")%>' target="_blank"
                                                                        style="font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #004b91; font-weight: bold;" title="click to view">
                                                                        <asp:Label ID="lbl_OrderId" runat="server" Text='<%#Eval("OrderId") %>'></asp:Label></a>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Credit Node">
                                                                <ItemTemplate>
                                                                    <a target="_blank" href="../Accounts/CreditNodeDomDetails.aspx?RefundID=<%#Eval("RefundID")%>">
                                                                        <asp:Label ID="lblRefundID" runat="server" Text='<%#Eval("RefundID") %>' ForeColor="#004b91"
                                                                            Font-Bold="True" Font-Underline="True"></asp:Label></a>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="AgencyId">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAgencyId" runat="server" Text='<%#Eval("AgencyId") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="UserID">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbluserid" runat="server" Text='<%#Eval("UserID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Agency Name">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblagencyname" runat="server" Text='<%#Eval("Agency_Name") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="P Type">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblpaxtype" runat="server" Text='<%#Eval("pax_type") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="P Name">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblpaxfname" runat="server" Text='<%#Eval("pax_fname") %>'></asp:Label>&nbsp;<asp:Label ID="lbllastname" runat="server" Text='<%#Eval("pax_lname") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%--<asp:TemplateField HeaderText="Pax LastName">
                                        <ItemTemplate>
                                            <asp:Label ID="lbllastname" runat="server" Text='<%#Eval("pax_lname") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                                            <asp:TemplateField HeaderText="Pnr">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblpnr" runat="server" Text='<%#Eval("pnr_locator") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Ticket Number">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltktno" runat="server" Text='<%#Eval("Tkt_No") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Airline">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblVC" runat="server" Text='<%#Eval("VC") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Sector">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbldestination" runat="server" Text='<%#Eval("Sector") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Departure Date">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbldeptdate" runat="server" Text='<%#Eval("departure_date") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="T Fare">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltotalfare" runat="server" Text='<%#Eval("TotalFare") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Fare After Discount">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltotalfareafterdiscount" runat="server" Text='<%#Eval("TotalFareAfterDiscount") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Cancellation Charge">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblcharge" runat="server" Text='<%#Eval("CancellationCharge") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Sevice Charge">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblsrvcharge" runat="server" Text='<%#Eval("ServiceCharge") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Refunded Fare">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblrefund" runat="server" Text='<%#Eval("RefundFare")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:BoundField DataField="PGCharges" HeaderText="Convenience Fee" />

                                                            <asp:TemplateField HeaderText="Comment">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblcancel" runat="server" Text='<%#Eval("RegardingCancel") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Exec ID">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblexecutive" runat="server" Text='<%#Eval("ExecutiveID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:BoundField DataField="Status" HeaderText="Refund Status" />

                                                            <asp:BoundField DataField="PgMode" HeaderText="Payment Mode" />
                                                            <asp:BoundField DataField="CancelStatus" HeaderText="Cancel Status" />
                                                            <asp:BoundField DataField="PartnerName" HeaderText="Partner Name" />
                                                            <asp:BoundField DataField="SubmitDate" HeaderText="Requested Date" />



                                                            <asp:TemplateField HeaderText="Accepted Date">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbldateA" runat="server" Text='<%#Eval("AcceptDate") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Updated Date">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbldateU" runat="server" Text='<%#Eval("UpdateDate") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Exec Rejected Remark">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRejectComment" runat="server" Text='<%#Eval("RejectComment") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Exec Updated Remark">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblUpComment" runat="server" Text='<%#Eval("UpdateRemark") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                        <RowStyle CssClass="RowStyle" />
                                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                        <PagerStyle CssClass="PagerStyle" />
                                                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                        <HeaderStyle CssClass="HeaderStyle" />
                                                        <EditRowStyle CssClass="EditRowStyle" />
                                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UP">
                                                <ProgressTemplate>
                                                    <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                                    </div>
                                                    <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                                        Please Wait....<br />
                                                        <br />
                                                        <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                                        <br />
                                                    </div>
                                                </ProgressTemplate>
                                            </asp:UpdateProgress>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <script type="text/javascript">
            var UrlBase = '<%=ResolveUrl("~/") %>';
        </script>

        <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

        <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

        <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
</asp:Content>
